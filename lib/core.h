// core.h
// Copyright (C) 2018-2021 JoeCo, All Rights Reserved.
#ifndef CORE_H
#define CORE_H

#include <stdint.h>
#include <stdarg.h>
#include <string.h>

#ifdef __cplusplus // C in C++
extern "C" {       //
#endif             //
///////////////////////////////////////////////////////////////////////////////

#define CORE_VERS "0.14"

///////////////////////////////////////////////////////////////////////////////
// TYPES

#if UINTPTR_MAX == UINT64_MAX
#define BUILD_64BIT 1
#else // !BUILD_64BIT
#define BUILD_64BIT 0
#endif // !BUILD_64BIT

// base types
typedef signed char int8;     // 8 bit signed int
typedef unsigned char uns8;   // 8 bit unsigned int
typedef uns8 bit;             // 1 or 0; 8 bit unsigned
typedef char chr;             // character for internal strings; 8 bit unsigned
typedef chr const cchr;       // const cchr
typedef chr const chrc;       // const cchr
typedef signed short int16;   // 16 bit signed int
typedef unsigned short uns16; // 16 bit unsigned int
typedef signed int int32;     // 32 bit signed int
typedef int32 off32;          // all offsets are from their adr base
typedef unsigned int uns32;   // 32 bit unsigned bit
typedef unsigned int uns;     // unsigned natural int
typedef float flt32;          // 32 bit float
typedef double flt64;         // 64 bit float
typedef void* ptr;            // pointer
typedef long long int64;      // 64 bit signed int
typedef unsigned long long uns64;  // 64 bit unsigned int
#if BUILD_64BIT
typedef int64 intptr; // pointer as int
#else                 // !BUILD_64BIT
typedef int32 intptr; // pointer as int
#endif                // !BUILD_64BIT

///////////////////////////////////////////////////////////////////////////////
// DEFINES

#define COMPILE_ASSERT(mustBeTrue)  ((void)sizeof(char[1 - 2*!(mustBeTrue)]))
#define NUM(_a_) (sizeof(_a_) / sizeof(_a_[0])) // number of entries
#define PTR_ADD(_p_, _i_) (void*)(((uns8*)(_p_)) + (_i_))
#define PTR_SUB(_p_, _i_) (void*)(((uns8*)(_p_)) - (_i_))
#define PTR_DIFF(_p1_, _p2_) ((int)((uns8*)(_p1_) - (uns8*)(_p2_)))

#define BRK() CorePrintf("* BRK() " __FILE__ ":%d\n",__LINE__)

#define ret(_r_) \
  do {           \
    r = (_r_);   \
    goto FINAL;   \
  } while(0)
#define bret(_r_) \
  do {            \
    BRK();        \
    r = (_r_);    \
    goto FINAL;    \
  } while(0)

///////////////////////////////////////////////////////////////////////////////

#define C2(_a_,_b_) (uns16)((_a_)|((_b_)<<8))
#define C4(_a_,_b_,_c_,_d_) (uns32)((_a_)|((_b_)<<8)|((_c_)<<16)|((_d_)<<24))

///////////////////////////////////////////////////////////////////////////////
// $("init CoreInit();") $("final CoreFinal();")

///////////////////////////////////////////////////////////////////////////////
// CORE INIT & FINAL

extern int CoreInit(void);
extern void CoreFinal(void);

///////////////////////////////////////////////////////////////////////////////
// CORE MEMORY

extern int CoreMemAlloc(void *utMemp,uns size); // alloc
extern int CoreMemAlloz(void *outMemp,uns size); // alloc and zero
extern void CoreMemFree(void *mem); // free
extern int CoreMemCount(); // return current number of allocations

// zero memory
#define MEMZ(_x_) memset((void*)&(_x_), 0, sizeof(_x_))

///////////////////////////////////////////////////////////////////////////////
// SZ STRINGS

// character tests

#define C_IS_EOL(c) ((c=='\n')||(c=='\r'))
#define C_IS_WHITE(c) ((c==' ')||(c=='\t')||(c=='\n')||(c=='\r'))
#define C_IS_ID(c) (((c>='A')&&(c<='Z')) \
  || ((c>='a')&&(c<='z')) || ((c>='0')&&(c<='9')) || (c=='_'))
#define C_IS_DIGIT(c) ((c>='0')&&(c<='9'))
#define C_SZ_SKIP (1)
#define C_IS_NUMMOD(c) \
  (((c>='0')&&(c<='9'))||(c=='.')||(c=='-')||(c=='e')||(c=='E'))

// size and length

extern int szlen(cchr *s1,cchr *s1x); // number of characters
extern int szsize(cchr *s1,cchr *s1x); // buffer byte size, incl 0-term
extern int szeos(cchr *s1,cchr *s1x); // test for end-of-string

// search, skip, scan

extern cchr* szskipchr(cchr *s1,cchr *s1x,cchr *skip,cchr *skipx);
extern cchr* sztillchr(cchr *s1,cchr *s1x,cchr *till,cchr *tillx);
extern cchr* szskipwhite(cchr *s1,cchr *s1x); // skip all whitespace
extern cchr* sztillwhite(cchr *s1,cchr *s1x); // until any whitespace
extern cchr* sztrimwhite(cchr *s1,cchr *s1x); // trim all whitespace off end
// skip (and find) argument, can be whitespace sep. OR "ARG" OR 'ARG'
extern cchr* szskiparg(cchr *str,cchr*strx,cchr **outArgp,cchr **outArgxp);
extern cchr *szlastslash(cchr *s1,cchr *s1x); // last forw or back slash
extern cchr* szskipeol(cchr *s1,cchr *s1x); // skip all end-of-line
extern cchr* sztilleol(cchr *s1,cchr *s1x); // until any end-of-line
extern cchr* szskipid(cchr *s1,cchr *s1x); // skip all id
extern cchr* sztillid(cchr *s1,cchr *s1x); // until any id
extern cchr* szchr(cchr *str,cchr *strx,chr match); // find first match
extern cchr* szrchr(cchr *str,cchr *strx,chr match); // find first match from end
// first occurrence of substring inside a string
extern cchr* szsub(cchr *sub,cchr *subx,cchr *str,cchr *strx);
extern cchr* szisub(cchr *sub,cchr *subx,cchr *str,cchr *strx);
// check if a string starts with
extern int szstart(cchr *start,cchr *startx,cchr *str,cchr *strx);
// count occurrences, return number of occurrences
extern int szcountchrs(cchr *str,cchr*strx,cchr*count,cchr*countx);
// count lines in string, returns 1 + number of end-of-lines
extern int szcountlines(cchr *str,cchr *strx); // return 1+num-of-eols
extern int szcmp(cchr *s1,cchr *s1x,cchr *s2,cchr *s2x);
extern int szicmp(cchr *s1,cchr *s1x,cchr *s2,cchr *s2x);

// returns index of mapping / -1 if not found, last entry MUST be null
extern int szmap(cchr *table[],cchr *s1,cchr *s1x);
// returns index of mapping / -1 if not found, last entry MUST be null
extern int szimap(cchr *table[],cchr *s1,cchr *s1x);

// in-place replacement
extern int szchrreplace(chr *str,chr*strx,chr from,chr to);

// buffer building
extern int szcpy(chr *dst,int dstSize,cchr *s1,cchr *s1x);
extern int szcat(chr *dst,int n,cchr *s1,cchr *s1x,cchr *s2,cchr *s2x);
extern int szfmt(chr *dst,int dstSize,cchr *fmt,...);
extern int szfmt_v(chr *dst,int dstSize,cchr *fmt,va_list args);
extern cchr* sztobin(cchr *s1,cchr *s1x,uns8 base,chr dsttype,void *dstp);

// szfmt() - minimal sprintf-like function
 // returns number of characters added to the buffer
 // always adds a terminating zero at the end, truncates if needed
 // formatting string codes recognized:
 // %s (cchr *str) -- zero-terminated string
 // %S (cchr *str,cchr *strx) -- pointer-terminated string
 // %c (chr cval) -- chr value as character (passed as int)
 // %d (int ival) -- int value
 // %ld (int32 lval) -- int32 value
 // %lld (int64 qval) -- int64 value
 // %q (int64 qval) -- 64-bit value
 // %f (flt32 fval) -- 32-bit float (passed as 64-bit-float)
 // %g (flt64 gval) -- 64-bit float (passed as 64-bit-float)
 // %p (ptr pval) -- pointer value
 // %rX (int count) -- repeat X count times

// todo: make utf8

///////////////////////////////////////////////////////////////////////////////
// CORE TIME

extern int CoreTimeNow(uns64 *outSecp); // get current time
extern int CoreTimeZoneOffsetSeconds(int32 *outSecp); // get current tz

// format time into a string
extern int CoreTimeStr(chr *dst,int dstSize,cchr *fmt,uns64 t);
 // format time, fmt recognizes:
 //  Y year%100
 //  y year
 //  m month
 //  d day
 //  H hour
 //  M min
 //  S sec

 extern int CoreTimeToYMDHHMMSS(uns64 t,uns16 *yOut,uns8 *mOut,uns8 *dOut,
  uns8 *hhOut,uns8 *mmOut,uns8 *ssOut);
extern int CoreTimeFromYJHHMMSS(uns64 *secp,uns16 y,uns16 jdays,uns8 hh,uns8 mm,uns8 ss);
extern int CoreTimeFromYMDHHMMSS(uns64 *secp,uns16 y,uns8 m,uns8 d,uns8 hh,uns8 mm,uns8 ss);

///////////////////////////////////////////////////////////////////////////////
// CORE SCRIPT FUNCTIONS

enum {
  CORERUNSCRIPT_ECHONONE,
  CORERUNSCRIPT_ECHOSTDOUT,
  CORERUNSCRIPT_ECHOSTDERR,
  CORERUNSCRIPT_ECHOSTDOUTERR
};

// run a script - note that shell scripts must >>=8 the result
extern int CoreRunScript(chr **outBufp,cchr *scr,cchr *scrx,int flags);
 // on success, caller must use CoreMemFree() to free the buffer
extern int CoreRunWithLog(chr **bufp,cchr *scr,cchr *scrx,
 cchr *logPath,int flagEcho);
 // on success, caller must use CoreMemFree() to free the buffer

// launch a script, doesn't return
extern int CoreLaunchScript(cchr *scr,cchr *scrx,chr*const* envarr);
 // envarr must be zero terminated

////////////////////////////////////////////////////////////////////////////////
// CORE LISTS

typedef uns32 CoreFncOp;
typedef va_list CoreArgs;
typedef int (*CoreFnc)(void*, CoreFncOp, CoreArgs);
typedef uns16 CoreType; // ascii-readable id
 // '_', -- reserved for types defined by system
 // '_','f' -- file

#define COREFNC_INIT C4('i','n','i','t')
#define COREFNC_ZAP  C4('z','a','p',0)

typedef struct _corelink {
  struct _corelink *p;  // prev
  struct _corelink *n;  // next
  CoreFnc fnc; // obj-function
  CoreType t; // CORETYPE_
  uns8 f; // flags reserved for system use
  uns8 user; // available for app use
} CoreLink;

#define CL(_a_) ((CoreLink*)(_a_))

extern void CoreLinkMakeHead(CoreLink* head);
extern void CoreLinkMakeNode(CoreLink* node, CoreType t,CoreFnc fnc);
extern void CoreLinkBefore(CoreLink* h, CoreLink* n);
extern void CoreLinkAfter(CoreLink* h, CoreLink* n);
extern void CoreLinkUnlink(CoreLink* n);
extern int CoreLinkFnc(CoreLink* lnk,uns32 op,...);
extern int CoreListTypeFnc(CoreLink* list,uns32 coreType,uns32 op,...);
extern int CoreLinkNew(CoreLink **out,CoreType t,CoreFnc fnc,uns32 totSize);

////////////////////////////////////////////////////////////////////////////////

// CoreNode - simplified

typedef struct _corenode {
  struct _corenode *p; // prev
  struct _corenode *n; // next
  uns16 t;             // type
  uns16 f;             // flags
} CoreNode;

#define CN(_n_) ((CoreNode*)(_n_))

extern void CoreNodeMakeHead(CoreNode* h);
extern void CoreNodeMakeNode(CoreNode* h, uns8 t);
extern void CoreNodeBefore(CoreNode* h, CoreNode* n);
extern void CoreNodeAfter(CoreNode* h, CoreNode* n);
extern void CoreNodeUnlink(CoreNode* n);

///////////////////////////////////////////////////////////////////////////////
// CORE FILE

#define M_COREFILESIZETIME_ATIME 0x0001
#define M_COREFILESIZETIME_MTIME 0x0002
#define M_COREFILESIZETIME_CTIME 0x0004
extern int CoreFileSizeTime(uns64 *sizep,uns64 *secp,cchr *path,cchr *pathx,int flags);
 // get file time

#define M_COREREADFILE_QUIET 0x01
#define M_COREREADFILE_EOLGUARANTEE 0x10
extern int CoreReadFile(chr **bufp,cchr *fpath,cchr *fpathx,int flags);
 // open and read file into a buffer, guarantees zero termination
 // on success, caller must use CoreMemFree() to free the buffer

// directory record
typedef struct {
  void *alloc;
  void *sys;
  chr *path;
  chr *entryx;
  intptr_t finder;
  uns16 flags;
  uns16 basePathSize;
  uns16 allocSize;
} CoreDirRec;

#define COREDIRENTRY_IS_FILE 1
#define COREDIRENTRY_IS_DIR 2
#define M_COREDIRSTART_ALLDIRS 0x01
extern int CoreDirStart(CoreDirRec *dr,cchr *path,cchr *pathx,int flags);
extern int CoreDirEntry(CoreDirRec *dr,cchr **outFile,cchr **outFilex);
extern int CoreDirCat(CoreDirRec *dr,cchr **outFile,cchr **outFilex,
  cchr *cat1,cchr *cat1x,cchr *cat2,cchr *cat2x);
extern void CoreDirFree(CoreDirRec *dr);

///////////////////////////////////////////////////////////////////////////////
// CORE FILE RECORD

#define CORETYPE_FILEREC C2('_','f')

// core file record
typedef struct {
  CoreLink lnk; // must be first
  chr *name; // path or name
  uns32 nameSize; // size of name buffer
} CoreFileRec;

extern int CoreFileRecFnc(CoreFileRec *o,CoreFncOp op,CoreArgs args);
extern int CoreFileRecNew(CoreFileRec **out,uns32 nameSize);

#define M_CORELISTFILES_RECURSE 0x01
extern int CoreListFiles(CoreLink *list,cchr *path,cchr *pathx,int flags);

///////////////////////////////////////////////////////////////////////////////
// CORE MISC

// returns number of characters printed
extern int CorePrintf(cchr *fmt,...); // uses szfmt() to format string
extern void CorePrintFlush();

// get the last line from a file (line=-1, numLines=1)
extern int CoreGetLinesFromFile(chr **outAlloc,
  chr **outLastLine,chr **outLastLinex,
  int line,int numLines,cchr *fpath,cchr *fpathx);

// find the Nth part of a string separated by <sep>, handles quotes
extern int CoreStringField(cchr **outStr,cchr **outStrx,cchr *str,cchr *strx,
  int n,int sep);

// argument scanner
extern int CoreArg(int argc,chrc **argv,cchr *sp,...);
  // in: argc, argv from main()
  // sp: string-parameter-definitions: <op><type><match>
  // <op> must be 'a' for argument for now
  // <type> denotes type of argument passed
  //  'i' (int *ival) -- pass a pointer to an int which is filled in
  //  'I' (int *present) -- pointer to int, set to non-zero if arg found
  //  's' (cchr **strp) -- pointer to cchr *, filled with pointer to arg found
  // <match> the rest of the string, until whitespace, must match
  // examples:
  //  "ai-n" (int *ival) -- ival is filled in with arg after '-n'
  //  "aI--name" (int *ival) -- ival is filled with 1 if '--name' was passed
  //  "as--name" (cchr **strp) -- strp is set to address of arg after '--name'

extern int CoreFileDependency(cchr *file0,...);
  // list of file-names must be 0 terminated
  // return 0 if file0 exists and date is >= other files

///////////////////////////////////////////////////////////////////////////////
// CORE RETURN CODES

enum {
  CORERET_OK=0,
  CORERET_ERR_PARAM=-9999,
  CORERET_ERR_OPERATION,
};

///////////////////////////////////////////////////////////////////////////////
#ifdef __cplusplus // C in C++
}                  //
#endif             //

#endif // ndef CORE_H
