![](misc/readme.png)

# Introduction
 InstantC makes building, debugging, and running C programs easier by handling the build and boiler-plate infrastructure.

## Installing on Linux, Cygwin
Install by cloning the git repo and building:
- ```$ git clone --depth 1 https://gitlab.com/jlinhoff/instantc.git``` 
- ```$ cd instantc```
- ```$ make install```

## Test The Install

When you run instantc from the command line, you should see the following:
```
$ instantc
instantc [options]
------------------
Options that start with a + are handled by InstantC:
 +build -- build, don't run
 +debug -- debug with gdb
 +help -- show help
 +launch -- launch
 +rebuild -- rebuild and run
 +v -- verbose
 +verbose -- verbose
--------------------------------
A C/C++ scripting compiler tool. Version: 0.46
See 'man instantc' for documentation.
Contact: instantcompiler@gmail.com
Copyright (C) 2019-2020 Joe Linhoff - see LICENSE file
```

Copyright (C) 2020 Joe Linhoff - All Rights Reserved

# Hello Instant C and C++

I created InstantC as a way to make C as easy to use as any other scripting languages. InstantC is a front end to the compiler on your system; it re-organizes your code, adds boilerplate, compiles, and runs your program.

## Hello InstantC!

Here is a simple example. Create a file, ```hello``` with the single line

```
printf("Hello InstantC!\n");
```

and then run it ```$ instantc hello``` .

Another way to startup your InstantC program in a Unix-like environment is to add the shebang line to the start of the file:

```
#!/usr/bin/env instantc
printf("Hello InstantC!\n");
```

Make the file executable, ```$ chmod +x hello```, and run the file:

```
$ ./hello
Hello InstantC!
```

Change the default language to C++ by adding ```$(lang c++)``` to the source. For example, in hellocpp:

```
#!/usr/bin/env instantc
  $(lang c++)
  std::cout << "InstantC++ Hello!\n";
```

Running hellocpp:

```
$ ./hellocpp
InstantC++ Hello!
```

## Command Line Arguments

All command line arguments are passed to your progam as an array of C-strings. ```$(ARGC)``` is the argument count, and ```$(ARGV)``` is the array. For example, create the file ```helloargs```:

```
#!/usr/bin/env instantc
  for(int i=0; i<$(ARGC); i++)
    printf("Hello argv[%d]:%s\n",i,$(ARGV)[i]);
```

To test this:

```
$ ./helloargs abc 100 "multi-word string"
Hello argv[0]:./helloargs-sc.exe
Hello argv[1]:abc
Hello argv[2]:100
Hello argv[3]:multi-word string
```

When building your program, InstantC replaces ```$(ARGC)``` and ```$(ARGV)``` with the argument-count and argument-array variable name. The dollar-sign-parentheses are meta-substitutions that InstantC processes when building your program.

## Main and Other Functions

As you've probably guessed, the code we've written so far makes up the body of the ```main``` function. To add functions, we introduce another meta-substition, ```$(functions)``` . This causes the lines of code that follow to be addedd into your progam before the main function.

For example, create the file ```hellofunctions``` :

```
#!/usr/bin/env instantc
  int a = square(16);
  printf("square:%d\n",a);

$(functions)
int square(int x) {
  return x*x;
} // square()
```

And, run your program:

```
$ ./hellofunctions
square:256
```

## Debugging

Writing good code is much easier when you can single step through every line, inspect variables, and think through your code as it executes. When you start your program with the ```+debug``` argument, InstantC will build a debuggable version, and launch it with gdb. For example:

```
$ ./hellofunctions +debug
GNU gdb (Ubuntu 8.1-0ubuntu3.2) 8.1.0.20180409-git
Copyright (C) 2018 Free Software Foundation, Inc.
...
Breakpoint 1, main (_scargc=1, _scargv=0x7ffffffee498) at ./hellofunctions-sc.c:9
9       int _scmainrc=0;
(gdb) 
```

Here are a few gdb commands:

```
s - next instruction - step into
n - next instruction - step over
p <var> - print variable
[enter] - repeat last command
```

See the links at the end of the article for more info on gdb.

## Pinpoint Debugging

As your program grows, it can become tedious to skip to the point in your program you are currently debugging. InstantC supports 'pinpoint debugging'. Add the ```$(gdb break)``` meta-substition to the line to break on. For example:

```
#!/usr/bin/env instantc
  int a = square(16);
  printf("square:%d\n",a);

$(functions)
int square(int x) {
  $(gdb break)
  return x*x;
} // square()
```

When your debug your program:

```
$ ./hellofunctions +debug
...
Breakpoint 1, square (x=16) at ./hellofunctions-sc.c:5
5       return x*x;
(gdb) p x
$1 = 16
```

Debugging is an important skill and should be used proactively. Many problems are avoided by taking the extra time to step through and think through your code when you first write it.

## Includes and Libraries

I've setup InstantC with a few defaults to make it easy to write quick programs. When no other includes are made, ```stdio.h``` is included by default. When you want to include other files, such as ```stdint.h```, you can use the ```$(include <stdint.h>)``` meta-substitution. When you add any includes, the defaults are no longer included.

```
#!/usr/bin/env instantc
  $(include <stdint.h>) $(include <stdio.h>)
  uint16_t u16 = 0x1234; // this type is defined in stdint.h
  printf("u16: 0x%x\n",u16);
```

Relatedly, to include and use the standard C math library, you need to add the ```math.h``` include file and link to the math library. The meta-substitution ```$(lflags -lm)``` causes the compiler to link the math library into your executable.

For example, the file ```hellomath``` :

```
#!/usr/bin/env instantc
  $(include <stdio.h>) $(include <math.h>) $(lflags -lm)
  printf("acos(0)=%f\n",acos(0));
```

When run:

```
$ ./hellomath
acos(0)=1.570796
```

## Boilerplate Behind The Curtains

InstantC works by adding boilerplate, handling meta-substitionts, and moving your code around a bit. To see behind the scenes, use the ```+build``` option. This will build the intermediate file. For example, ```$ ./hellomath +build``` builds:

```
// hellomath-sc.c
// gcc -std=c99 -g ./hellomath-sc.c -lm -o ./hellomath-sc.exe
#include <stdio.h>
#include <math.h>
int main(int _scargc,char const*_scargv[])
{
int _scmainrc=0;
printf("acos(0)=%f\n",acos(0));

return _scmainrc;
}
```

Normally, this file is created, compiled, and deleted after successful builds. The ```+build``` option run through the build steps, and doesn't delete build file.

## A Simple C++ Example

It's just as easy to write C++ code. Create a simple C++ example with a class. Write the lines below into the file ```helloclass```:

```
#!/usr/bin/env instantc
$(lang c++)
  class Hello {
  public:
    Hello() {std::cout << "ctor Hello class\n";}
    ~Hello() {std::cout << "dtor Hello class\n";}
  }; // class Hello

  Hello h;
  std::cout << "InstantC++ Hello!\n";
```

Run the code:

```
$ ./hellocpp
ctor Hello class
InstantC++ Hello!
dtor Hello class
```

## InstantC Is A Platform For Quick Turnaround

InstantC provides a quick turnaroud that makes it easy to test ideas. You don't need projects or makefiles or IDE's. Just write a few lines, and start it.

InstantC works to add minimal boilerplate needed to run your code. One of the great things about InstantC is that it isn't a new language and doesn't require you to learn any new syntax. All the code in the executable is your own code compiled with the compiler tools on your system.

## Resources

- [InstantC Webpage - install instructions](http://www.instantcompiler.com/)
- [shebang info](https://en.wikipedia.org/wiki/Shebang_(Unix))
- [gdb](https://www.gnu.org/software/gdb)
- [gdb basics](https://medium.com/@amit.kulkarni/gdb-basics-bf3407593285)

Copyright (C) 2020 Joe Linhoff - All Rights Reserved
