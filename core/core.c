// core.c -- Copyright (C) 2019 Joe Linhoff
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License. You may obtain a copy
// of the License at http://www.apache.org/licenses/LICENSE-2.0 Unless required
// by applicable law or agreed to in writing, software distributed under the
// License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
// OF ANY KIND, either express or implied. See the License for the specific
// language governing permissions and limitations under the License.
#include <time.h>
#include <unistd.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>

#include "lib/core.h"

typedef struct {
  int32 allocCount;
  int32 tzOffSeconds;
} coreGlobals;

static coreGlobals coreG;

// JFL 01 Oct 19
int CoreInit(void)
{
  MEMZ(coreG);
  CoreTimeZoneOffsetSeconds(&coreG.tzOffSeconds);
  return 0;
} // CoreInit()

// JFL 01 Oct 19
void CoreFinal()
{
  if(coreG.allocCount) {
    CorePrintf("** CoreMem %d allocations not freed!\n",coreG.allocCount);
  }
} // CoreFinal()

// JFL 01 Oct 19
// JFL 07 Nov 19
// JFL 20 Dec 19
int CorePrintf(cchr *fmt,...)
{
  int r,bufsize=0;
  chr bufstack[1024];
  chr *buf;
  chr *bufalloc=0;
  va_list args;
  va_list arg2;

  va_start(args, fmt);

  buf = bufstack,bufsize=sizeof(bufstack);
  for(;;) {
    va_copy(arg2,args);
    r = szfmt_v(buf, bufsize-2, fmt, args);
    if(r < bufsize-4)
      break;
    va_copy(args,arg2);
    if(bufalloc) {
      CoreMemFree(bufalloc);
      bufalloc=0;
    }
    bufsize <<= 1;
    if((r=CoreMemAlloc(&bufalloc,bufsize))<0)
      goto BAIL;
    buf=bufalloc;
  } // for

  printf("%s",buf);

  // r is string length printed
BAIL:
  va_end(args);

  if(bufalloc)
    CoreMemFree(bufalloc);
  return r;
} // CorePrintf()

// JFL 29 Nov 19
void CorePrintFlush()
{
  fflush(stdout);
} // CorePrintFlush()

////////////////////////////////////////////////////////////////////////////////

// JFL 01 Oct 19
int CoreMemAlloc(void *pp,uns size)
{
  void*p=malloc(size);
  if(!p) return -999;
  *((void**)pp)=p;
  coreG.allocCount++;
  return 0;
} // CoreMemAlloc()

// JFL 01 Oct 19
int CoreMemAlloz(void *pp,uns size)
{
  void*p=calloc(1,size);
  if(!p) return -999;
  *((void**)pp)=p;
  coreG.allocCount++;
  return 0;
} // CoreMemAlloz()

// JFL 01 Oct 19
void CoreMemFree(void *p)
{
  free(p);
  coreG.allocCount--;
} // CoreMemFree()

// JFL 01 Oct 19
int CoreMemCount()
{
  return coreG.allocCount;
} // CoreMemCount()

////////////////////////////////////////////////////////////////////////////////

// JFL 23 Aug 06
// JFL 20 Mar 08; re-worked from DL
// JFL 28 Jul 17
void CoreLinkMakeHead(CoreLink* h)
{
  h->p = h->n = h;
  h->fnc = 0;
  h->t = 0;
  h->f = 0;
} // CoreLinkMakeHead()

// JFL 23 Aug 06
// JFL 20 Mar 08; re-worked from DL
// JFL 28 Jul 17
void CoreLinkMakeNode(CoreLink* n, CoreType t,CoreFnc fnc)
{
  n->n = n->p = n;
  n->fnc = fnc;
  n->f = 0;
  n->t = t;
} // CoreLinkMakeNode()

// JFL 23 Aug 06
// JFL 20 Mar 08; re-worked from DL
// JFL 28 Jul 17
void CoreLinkBefore(CoreLink* h, CoreLink* n)
{
  n->p = h->p;
  n->p->n = n;
  n->n = h;
  h->p = n;
} // CoreLinkBefore()

// JFL 23 Aug 06
// JFL 20 Mar 08; re-worked from DL
// JFL 08 Mar 10; fixed link bug
void CoreLinkAfter(CoreLink *h,CoreLink *n)
{
   n->p=h;
   n->n=h->n;
   h->n->p=n;
   h->n=n;
} // CoreLinkAfter()

// JFL 28 Jul 17
void CoreLinkUnlink(CoreLink* n)
{
  n->p->n = n->n;
  n->n->p = n->p;
  n->n = n->p = n; // multiple unlinks OK
} // BaseUnlink()

// JFL 14 Oct 19
int CoreLinkFnc(CoreLink* lnk,uns32 op,...)
{
  int r=-99;
  va_list args;
  va_start(args,op);
  if(lnk->fnc)
    r=(lnk->fnc)(lnk,op,args);
  va_end(args);
  return r;
} // CoreLinkFnc()

// JFL 14 Oct 19
int CoreListTypeFnc(CoreLink* list,uns32 coreType,uns32 op,...)
{
  int r=0,rr;
  va_list args;
  va_start(args,op);
  CoreLink *lnk,*lnk2;
  for(lnk=list->n;lnk->t;lnk=lnk2) {
    lnk2=lnk->n;
    if(coreType && (lnk->t!=coreType))
      continue;
    if(!lnk->fnc)
      continue;
    if((rr=(lnk->fnc)(lnk,op,args))<0) {
      if(!r)
        r=rr;
    }
  } // for
  va_end(args);
  return r;
} // CoreListTypeFnc()

// JFL 14 Oct 19
int CoreLinkNew(CoreLink **out,CoreType t,CoreFnc fnc,uns32 totSize)
{
  int r;
  CoreLink *lnk;
  if((r=CoreMemAlloz(&lnk,totSize))<0)
    goto FINAL;
  CoreLinkMakeNode(lnk,t,fnc);
  if((lnk->fnc=fnc)) {
    va_list args={0};
    if((r=fnc(lnk,COREFNC_INIT,args))<0)
      goto FINAL;
  }
  if(out)
    *out=CL(lnk);
  r=0;
FINAL:
  return r;
} // CoreLinkNew()

////////////////////////////////////////////////////////////////////////////////

// JFL 23 Aug 06
// JFL 20 Mar 08; re-worked from DL
// JFL 28 Jul 17
void CoreNodeMakeHead(CoreNode* h)
{
  h->p = h->n = h;
  h->t = 0;
  h->f = 0;
} // CoreNodeMakeHead()

// JFL 23 Aug 06
// JFL 20 Mar 08; re-worked from DL
// JFL 28 Jul 17
void CoreNodeMakeNode(CoreNode* n, uns8 t)
{
  n->n = n->p = n;
  n->f = 0;
  n->t = (uns8)t;
} // CoreNodeMakeNode()

// JFL 23 Aug 06
// JFL 20 Mar 08; re-worked from DL
// JFL 28 Jul 17
void CoreNodeBefore(CoreNode* h, CoreNode* n)
{
  n->p = h->p;
  n->p->n = n;
  n->n = h;
  h->p = n;
} // CoreNodeBefore()

// JFL 23 Aug 06
// JFL 20 Mar 08; re-worked from DL
// JFL 08 Mar 10; fixed link bug
void CoreNodeAfter(CoreNode *h,CoreNode *n)
{
   n->p=h;
   n->n=h->n;
   h->n->p=n;
   h->n=n;
} // CoreNodeAfter()

// JFL 28 Jul 17
void CoreNodeUnlink(CoreNode* n)
{
  n->p->n = n->n;
  n->n->p = n->p;
  n->n = n->p = n; // multiple unlinks OK
} // CoreNodeUnlink()

////////////////////////////////////////////////////////////////////////////////

#if FUN_GMTIME_S
#define SECONDS_FROM_TM(tmSrc) _mkgmtime(tmSrc)
#define TM_GMT_FROM_SECONDS(tmDst,secSrc) gmtime_s(tmDst,(time_t const*)secSrc)
#define TM_LOC_FROM_SECONDS(tmDst,secSrc) localtime_s(tmDst,(time_t const*)secSrc)
#else
#define SECONDS_FROM_TM(tmSrc) timegm(tmSrc)
#define TM_LOC_FROM_SECONDS(tmDst,secSrc) localtime_r((time_t const*)secSrc,tmDst)
#define TM_GMT_FROM_SECONDS(tmDst,secSrc) gmtime_r((time_t const*)secSrc,tmDst)
#endif

// JFL 16 Nov 13
int CoreTimeNow(uns64 *secp)
{
  time_t tt;
  struct tm d1;
  tt = time(0);
  TM_GMT_FROM_SECONDS(&d1,&tt);
  tt = SECONDS_FROM_TM(&d1);
  *secp=tt;
  return 0;
} // CoreTimeNow()

// JFL 29 Jun 19
int CoreTimeZoneOffsetSeconds(int32 *secp)
{
  time_t tt = time(0);
  struct tm d1,d2;
  TM_GMT_FROM_SECONDS(&d1,&tt);
  TM_LOC_FROM_SECONDS(&d2,&tt);
  time_t sgmt = SECONDS_FROM_TM(&d1);
  time_t sloc = SECONDS_FROM_TM(&d2);
  *secp = sgmt-sloc;
  return 0;
} // CoreTimeZoneOffsetSeconds()

// JFL 16 Nov 13
// JFL 29 Jun 19; time-zone
int CoreTimeFromYMDHHMMSS(uns64 *secp,uns16 y,uns8 m,uns8 d,uns8 hh,uns8 mm,uns8 ss)
{ // y=year(2019==2019) m=month(1==jan) d=day(1st==1)
  time_t tt;
  struct tm d1,d2,d3;
  tt = time(0); // get current local time for any non-specifed field
  TM_GMT_FROM_SECONDS(&d1,&tt); // use GMT to avoid DST

  if(y>1900)
    d1.tm_year = y-1900;
  if(m)
    d1.tm_mon = m-1;
  else BRK();
  if(d)
    d1.tm_mday = d;
  else BRK();

  MEMZ(d2);
  d2.tm_year = d1.tm_year;
  d2.tm_mon = d1.tm_mon;
  d2.tm_mday = d1.tm_mday;
  d2.tm_hour = hh;
  d2.tm_min = mm;
  d2.tm_sec = ss;
  //d2.tm_isdst = -1;
  tt = SECONDS_FROM_TM(&d2);

#if DEBUG
{
  TM_GMT_FROM_SECONDS(&d3,&tt);

  if(y && (d3.tm_year+1900 != y)) BRK();
  if(d3.tm_mon+1 != m) BRK();
  if(d3.tm_mday != d) BRK();

  if(d3.tm_hour != hh) BRK();
  if(d3.tm_min != mm) BRK();
  if(d3.tm_sec != ss) BRK();
}
#endif // DEBUG

  *secp=tt;
  return 0;
} // CoreTimeFromYMDHHMMSS()

// JFL 08 Jul 19
int CoreTimeFromYJHHMMSS(uns64 *secp,uns16 y,uns16 j,uns8 hh,uns8 mm,uns8 ss)
{
  time_t tt;
  struct tm d1,d2;
  uns32 jj;

  if(y>1900) {
    d1.tm_year = y-1900;
  } else {
    tt = time(0); // get current local time for any non-specifed field
    TM_GMT_FROM_SECONDS(&d1,&tt); // use GMT to avoid DST
  }

  // find time at start of year
  MEMZ(d2);
  d2.tm_year = d1.tm_year;
  tt = SECONDS_FROM_TM(&d2);

  // add in days
  jj = j;
  jj *= 24*60*60; // days to seconds
  tt += jj;

  jj = hh*60*60;
  tt += jj;

  jj = mm*60;
  tt += jj;

  tt += ss;

  *secp=0;
  return 0;
} // CoreTimeFromYJHHMMSS()

int CoreTimeToYMDHHMMSS(uns64 t,uns16 *yOut,uns8 *mOut,uns8 *dOut,
  uns8 *hhOut,uns8 *mmOut,uns8 *ssOut)
{
  time_t tt = t;
  struct tm d1;
  TM_GMT_FROM_SECONDS(&d1,&tt);
  tt = SECONDS_FROM_TM(&d1);
  if(tt != t) BRK();
  if(yOut)
    *yOut=d1.tm_year+1900;
  if(mOut)
    *mOut=d1.tm_mon+1;
  if(dOut)
    *dOut=d1.tm_mday;
  if(hhOut)
    *hhOut=d1.tm_hour;
  if(mmOut)
    *mmOut=d1.tm_min;
  if(ssOut)
    *ssOut=d1.tm_sec;
  return 0;
} // CoreTimeToYMDHHMMSS()

static cchr *mainTimeFmtDefault = "m/d/Y H:M:S";

// JFL 23 Jun 19
int CoreTimeStr(chr *dst,int dstSize,cchr *fmt,uns64 t)
{
  int r,ds=dstSize;
  chr *dst0=dst;
  uns16 y;
  uns8 m,d,hh,mm,ss;
  chr c,chh,cmm,css;

  if(!dst || (dstSize<1))
    ret(CORERET_ERR_PARAM);

  dst[0]=0;

  if(!fmt)
    fmt=mainTimeFmtDefault;

  if(!szchr(fmt,0,'z'))
    t -= coreG.tzOffSeconds;
  if((r=CoreTimeToYMDHHMMSS(t,&y,&m,&d,&hh,&mm,&ss))<0)
    goto FINAL;

  chh = hh>9 ? 0 : '0';
  cmm = mm>9 ? 0 : '0';
  css = ss>9 ? 0 : '0';

  // y=year, Y=year%100 m=month, d=day, H=hour,M=min,S=sec
  for(;;) {
    if(ds<=1)
      break;
    if(!(c=*fmt++))
      break;

//printf("c='%c'\n",c);

    switch(c) {
    default:
      *dst++=c,ds--;
      break;
    case 'Y':
      y %= 100;
    case 'y':
      r=szfmt(dst,ds,"%d",y);
      dst+=r,ds-=r;
      break;
    case 'm':
      r=szfmt(dst,ds,"%d",m);
      dst+=r,ds-=r;
      break;
    case 'd':
      r=szfmt(dst,ds,"%d",d);
      dst+=r,ds-=r;
      break;
    case 'z':  {
      uns32 nn = hh;
      if(d>1)
        nn += (d-1)*24;
      r=szfmt(dst,ds,"%d",nn);
      dst+=r,ds-=r;
      } break;
    case 'H':
      r=szfmt(dst,ds,"%c%d",chh,hh);
      dst+=r,ds-=r;
      break;
    case 'M':
      r=szfmt(dst,ds,"%c%d",cmm,mm);
      dst+=r,ds-=r;
      break;
    case 'S':
      r=szfmt(dst,ds,"%c%d",css,ss);
      dst+=r,ds-=r;
      break;
    } // switch
  } // for

  r = dstSize-ds;
  if(r>=dstSize)
    r=dstSize-1;
  dst0[r]=0;
FINAL:
  return r;
} // CoreTimeStr()

// JFL 17 Sep 19
int CoreFileSizeTime(uns64 *sizep,uns64 *secp,cchr *path,cchr *pathx,int flags)
{
  int r,s;
  struct stat st;
  chr *alloc=0;

  if(pathx) {
    s=szsize(path,pathx);
    if((r=CoreMemAlloc(&alloc,s))<0)
      goto FINAL;
    szcpy(alloc,s,path,pathx);
    path=alloc;
  }

  if(stat(path,&st)<0)
    ret(CORERET_ERR_OPERATION);

  if(secp) {
    if(!(flags&(M_COREFILESIZETIME_ATIME
        |M_COREFILESIZETIME_CTIME|M_COREFILESIZETIME_MTIME)))
      flags|=M_COREFILESIZETIME_MTIME; // default
    *secp=0;
    if((flags&M_COREFILESIZETIME_ATIME)&&(*secp<st.st_atime))
      *secp = st.st_atime;
    if((flags&M_COREFILESIZETIME_CTIME)&&(*secp<st.st_ctime))
      *secp = st.st_ctime;
    if((flags&M_COREFILESIZETIME_MTIME)&&(*secp<st.st_mtime))
      *secp = st.st_mtime;
  }

  if(sizep)
    *sizep=st.st_size;

  r=0;
FINAL:
  if(alloc)
    CoreMemFree(alloc);
  return r;
} // CoreFileSizeTime()

// JFL 22 Feb 15
// JFL 31 May 19
// JFL 18 Sep 19
// JFL 06 Jan 20
int CoreReadFile(chr **bufp,cchr *fpath,cchr *fpathx,int flags)
{
  int r, s, top;
  FILE* fh = 0;
  chr *pathbuf = 0;
  chr *buf = 0;

  if(fpathx) {
    s=szsize(fpath,fpathx);
    if((r=CoreMemAlloc(&pathbuf,s))<0)
      goto FINAL;
    szcpy(pathbuf,s,fpath,fpathx);
    fpath=pathbuf,fpathx=0;
  }

  if(!(fh = fopen(fpath, "rb"))) {
    if(!(flags&M_COREREADFILE_QUIET))
      printf("* couldn't open '%s'\n",fpath);
    ret(-5);
  }

  // get file size
  fseek(fh, 0, SEEK_END);
  s = top = ftell(fh);
  fseek(fh, 0, SEEK_SET);

  if(bufp) {
    if((r=CoreMemAlloc(&buf,s+2))<0)
      goto FINAL;

    if((r = fread(buf, 1, s, fh)) != s)
      ret(-7);
    buf[s] = 0; // always term

    if(M_COREREADFILE_EOLGUARANTEE) {
      if(!C_IS_EOL(buf[s-1])) {
        buf[s++]='\n';
        buf[s]=0;
      }
    } // add EOL

    *bufp=buf,buf=0;
  }

  r = s;
FINAL:
  if(buf)
    CoreMemFree(buf);
  if(pathbuf)
    CoreMemFree(pathbuf);
  if(fh)
    fclose(fh);
  return r;
} // CoreReadFile()

// JFL 03 Jul 19
// JFL 18 Nov 20; reworked with getc
int CoreRunScript(chr **bufp,cchr *scr,cchr *scrx,int flagEcho)
{
  int r,c;
  FILE *echofd[2];
  int32 allocsize,bs,expandsize;
  FILE *pp=0;
  chr *alloc=0,*bb;
  chr *scralloc=0;

  expandsize=1024*4; // 4K initially
  echofd[0]=echofd[1]=0;

  if(scrx) {
    bs=szsize(scr,scrx);
    if((r=CoreMemAlloc(&scralloc,bs))<0)
      goto FINAL;
    szcpy(scralloc,bs,scr,scrx);
    scr=scralloc,scrx=0;
  }

  if(flagEcho==CORERUNSCRIPT_ECHOSTDOUT)
    echofd[0]=stdout;
  else if(flagEcho==CORERUNSCRIPT_ECHOSTDERR)
    echofd[1]=stderr;
  else if(flagEcho==CORERUNSCRIPT_ECHOSTDOUTERR)
    echofd[0]=stdout,echofd[1]=stderr;

  // try to start the command
  if(!(pp=popen(scr,"r")))
    ret(-1);

  allocsize=0;
  bs=0;
  while(1) {
    c=getc(pp);
    if(c==EOF) // EOF sent when process ends
      c=0;
    else {
      if(echofd[0])
        putc(c,echofd[0]);
      if(echofd[1])
        putc(c,echofd[1]);
      if(c=='\n') {
        if(echofd[0])
          fflush(echofd[0]);
        if(echofd[1])
          fflush(echofd[1]);
      }
    }
    if(!bs)
    {
        int bs2;
        chr *alloc2;

        // create new, larger buffer
        if(expandsize<1024*1024*16) // double in size until 16M
          expandsize*=2;
        bs2=allocsize+expandsize;
        if((r=CoreMemAlloc(&alloc2,bs2))<0)
          goto FINAL;
        r=allocsize-bs; // amount in current buffer
        if(alloc)
          memcpy(alloc2,alloc,r);

        // free old & swap
        bb = alloc2+r; // offset into new buf
        bs = bs2-r; // remaining in new buf
        allocsize = bs2;
        if(alloc)
          CoreMemFree(alloc);
        alloc = alloc2;
    }
    *bb++=c,bs--;
    if(!c)
      break;
  } // while

  if(bufp)
    *bufp=alloc,alloc=0;

  r = 0;
FINAL:
  if(scralloc)
    CoreMemFree(scralloc);
  if(alloc)
    CoreMemFree(alloc);
  if(pp) {
    int x = pclose(pp);
    if(r>=0) // override if there are no other errors
      r=x; //(shell scripts must >>=8 to get exit code
  }
  return r;
} // CoreRunScript()

// JFL 03 Jul 19
// JFL 18 Nov 20; reworked with getc
// JFL 23 Aug 21; with log
int CoreRunWithLog(chr **bufp,cchr *scr,cchr *scrx,cchr *logPath,int flagEcho)
{
  int c;
  FILE *echofd[2];
  int32 r,allocsize,bs,expandsize,loglast=0;
  FILE *pp=0;
  chr *alloc=0,*bb;
  chr *scralloc=0;

  expandsize=1024*4; // 4K initially
  echofd[0]=echofd[1]=0;

  if(scrx) {
    bs=szsize(scr,scrx);
    if((r=CoreMemAlloc(&scralloc,bs))<0)
      goto FINAL;
    szcpy(scralloc,bs,scr,scrx);
    scr=scralloc,scrx=0;
  }

  if(flagEcho==CORERUNSCRIPT_ECHOSTDOUT)
    echofd[0]=stdout;
  else if(flagEcho==CORERUNSCRIPT_ECHOSTDERR)
    echofd[1]=stderr;
  else if(flagEcho==CORERUNSCRIPT_ECHOSTDOUTERR)
    echofd[0]=stdout,echofd[1]=stderr;

  // try to start the command
  if(!(pp=popen(scr,"r")))
    ret(-1);

  allocsize=0;
  bs=0;
  while(1) {
    c=getc(pp);
    if(c==EOF) // EOF sent when process ends
      c=0;
    else {
      if(echofd[0])
        putc(c,echofd[0]);
      if(echofd[1])
        putc(c,echofd[1]);
    }
    if(!bs)
    {
        int bs2;
        chr *alloc2;

        // create new, larger buffer
        if(expandsize<1024*1024*16) // double in size until 16M
          expandsize*=2;
        bs2=allocsize+expandsize;
        if((r=CoreMemAlloc(&alloc2,bs2))<0)
          goto FINAL;
        r=allocsize-bs; // amount in current buffer
        if(alloc)
          memcpy(alloc2,alloc,r);

        // free old & swap
        bb = alloc2+r; // offset into new buf
        bs = bs2-r; // remaining in new buf
        allocsize = bs2;
        if(alloc)
          CoreMemFree(alloc);
        alloc = alloc2;
    }
    *bb++=c,bs--;

    if(!c || (c=='\n')) { // end-of-line
      if(logPath) { // add to log file
        FILE *fh;
        if((fh=fopen(logPath,"ab"))) { // log open
          r=allocsize-bs; // amount in current buffer
          if(!c) r--;
          fwrite(alloc+loglast,r-loglast,1,fh);
          fclose(fh);
          loglast=r;
        } // log open
      } // add to log file

      // flush to keep output up-to-date
      if(echofd[0])
        fflush(echofd[0]);
      if(echofd[1])
        fflush(echofd[1]);

    } // end-of-line

    if(!c)
      break;
  } // while

  if(bufp)
    *bufp=alloc,alloc=0;

  r = 0;
FINAL:
  if(scralloc)
    CoreMemFree(scralloc);
  if(alloc)
    CoreMemFree(alloc);
  if(pp) {
    int x = pclose(pp);
    if(r>=0) // override if there are no other errors
      r=x; //(shell scripts must >>=8 to get exit code
  }
  return r;
} // CoreRunWithLog()

// JFL 05 Nov 19
int CoreLaunchScript(cchr *scr,cchr *scrx,chr*const* envarr)
{ // experimental for gdb -tui
  chr *scralloc=0;
  chr *dst,*dstx;
  cchr *s1,*s2;
  int argc,r,s,nn;

  nn=szcountchrs(scr,scrx," ",0)+1;
  cchr *argv[nn];

  s=szsize(scr,scrx);
  if((r=CoreMemAlloc(&scralloc,s))<0)
    goto FINAL;

  dst=scralloc;
  r=szcpy(dst,s,scr,scrx);
  dstx=dst+r;

  argc=0;

  dst=(chr*)szskiparg(dst,dstx,&s1,&s2);
  argv[argc++]=s1;
  *((chr*)s2)=0;
  dst++;

  for(;;) {
    dst=(chr*)szskipwhite(dst,dstx);
    if(szeos(dst,dstx))
      break;
    dst=(chr*)szskiparg(dst,dstx,&s1,&s2);
    dst++;
    argv[argc++]=s1;
    *((chr*)s2)=0;
  } // for

  argv[argc++]=0;
  if(argc>nn) BRK();

  execve(argv[0],(char*const*)argv,(char*const*)envarr);

  r=0;
FINAL:
  if(scralloc)
    CoreMemFree(scralloc);
  return r;
} // CoreLaunchScript()

////////////////////////////////////////////////////////////////////////////////

// JFL 09 Aug 19
int CoreDirCat(CoreDirRec *dr,cchr **outFile,cchr **outFilex,
  cchr *cat1,cchr *cat1x,cchr *cat2,cchr *cat2x)
{
  int r,s;
  chr *s1,*sx;

  sx = PTR_ADD(dr->alloc,dr->allocSize);
  s1 = dr->entryx;

  s = PTR_DIFF(sx,s1);
  if(cat1) {
    r = szcpy(s1,s,cat1,cat1x);
    s1+=r,s-=r;
  }
  if(cat2) {
    r = szcpy(s1,s,cat2,cat2x);
    s1+=r,s-=r;
  }

  *outFile=dr->path,*outFilex=s1;

  r=0;
  return r;
} // CoreDirCat()

///////////////////////////////////////////////////////////////////////////////

/* Copyright (C) 1991-93,96,97,98,99,2001,2004 Free Software Foundation, Inc.
   This file is part of the GNU C Library.

   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307 USA.  */

/* Bits set in the FLAGS argument to `fnmatch'.  */
#define FNM_PATHNAME (1 << 0) /* No wildcard can ever match `/'.  */
#define FNM_NOESCAPE (1 << 1) /* Backslashes don't quote special chars.  */
#define FNM_PERIOD (1 << 2) /* Leading `.' is matched only explicitly.  */
#define FNM_FILE_NAME  FNM_PATHNAME        /* Preferred GNU name.  */
#define FNM_LEADING_DIR (1 << 3)        /* Ignore `/...' after a match.  */
#define FNM_CASEFOLD (1 << 4) /* Compare without regard to case.  */
#define FNM_EXTMATCH (1 << 5) /* Use ksh-like extended matching. */

/* Value returned by `fnmatch' if STRING does not match PATTERN.  */
#define FNM_NOMATCH 1
#define FOLD_TOLOWER(_c_) ((_c_) + (((_c_)>='A'&&(_c_)<='Z')?'a'-'A':0))

/* Match STRING against the filename pattern PATTERN, returning zero if
   it matches, nonzero if not.  */
int fnmatch (chrc *pattern, chrc *string, int flags) {
  chrc *p = pattern, *n = string;
  chr c;

  #define FOLD(c)	((flags & FNM_CASEFOLD) ? FOLD_TOLOWER (c) : (c))

  while ((c = *p++) != '\0') {
    c = FOLD (c);
    switch (c) {
    case '?':
      if (*n == '\0')
        return FNM_NOMATCH;
      else if ((flags & FNM_FILE_NAME) && *n == '/')
        return FNM_NOMATCH;
      else if ((flags & FNM_PERIOD) && *n == '.' &&
        (n == string || ((flags & FNM_FILE_NAME) && n[-1] == '/')))
      return FNM_NOMATCH;
	  break;

    case '\\':
      if (!(flags & FNM_NOESCAPE)) {
	      c = *p++;
	      c = FOLD (c);
	    }
      if (FOLD ((unsigned char)*n) != c)
        return FNM_NOMATCH;
      break;

    case '*':
      if ((flags & FNM_PERIOD) && *n == '.' &&
	      (n == string || ((flags & FNM_FILE_NAME) && n[-1] == '/')))
        return FNM_NOMATCH;

      for (c = *p++; c == '?' || c == '*'; c = *p++, ++n) {
	    if (((flags & FNM_FILE_NAME) && *n == '/') || (c == '?' && *n == '\0'))
        return FNM_NOMATCH;
      } // for

      if (c == '\0')
        return 0;

      { // block
	    unsigned char c1 = (!(flags & FNM_NOESCAPE) && c == '\\') ? *p : c;
	    c1 = FOLD (c1);
	    for (--p; *n != '\0'; ++n) {
	      if ((c == '[' || FOLD ((unsigned char)*n) == c1)
          && fnmatch (p, n, flags & ~FNM_PERIOD) == 0)
            return 0;
      } // for
	    return FNM_NOMATCH;
      } // block

    case '[': {
      /* Nonzero if the sense of the character class is inverted.  */
      int negate;

      if (*n == '\0')
        return FNM_NOMATCH;

      if ((flags & FNM_PERIOD) && *n == '.'
        && (n == string || ((flags & FNM_FILE_NAME) && n[-1] == '/')))
        return FNM_NOMATCH;

	    negate = (*p == '!' || *p == '^');
	    if (negate)
	      ++p;

	    c = *p++;
	    for (;;) {
        register unsigned char cstart = c, cend = c;

        if (!(flags & FNM_NOESCAPE) && c == '\\')
          cstart = cend = *p++;

        cstart = cend = FOLD (cstart);

      if (c == '\0') /* [ (unterminated) loses.  */
        return FNM_NOMATCH;

      c = *p++;
      c = FOLD (c);

      if ((flags & FNM_FILE_NAME) && c == '/') /* [/] can never match.  */
        return FNM_NOMATCH;

      if (c == '-' && *p != ']') {
		    cend = *p++;
		    if (!(flags & FNM_NOESCAPE) && cend == '\\')
		      cend = *p++;
		    if (cend == '\0')
		      return FNM_NOMATCH;
		    cend = FOLD (cend);

		    c = *p++;
		  } // if

      if (FOLD ((unsigned char)*n) >= cstart
		    && FOLD ((unsigned char)*n) <= cend)
		  goto matched;

      if (c == ']')
        break;
      } // for
	    if (!negate)
	      return FNM_NOMATCH;
	    break;

      matched:;
	    /* Skip the rest of the [...] that already matched.  */
	    while (c != ']') {
        if (c == '\0') /* [... (unterminated) loses.  */
          return FNM_NOMATCH;

        c = *p++;
        if (!(flags & FNM_NOESCAPE) && c == '\\') /* XXX 1003.2d11 is unclear if this is right.  */
          ++p;
      } // while
	    if (negate)
	      return FNM_NOMATCH;
	  } // case
	  break;

    default:
      if (c != FOLD ((unsigned char)*n))
        return FNM_NOMATCH;
    } // switch

    ++n;
  } // while

  if (*n == '\0')
    return 0;

/* The FNM_LEADING_DIR flag says that "foo*" matches "foobar/frobozz".  */
  if ((flags & FNM_LEADING_DIR) && *n == '/')
    return 0;

  return FNM_NOMATCH;
} // fnmatch()

////////////////////////////////////////////////////////////////////////////////

// MINGW1 <dirent.h> (on MSYS)
// MINGW2 <io.h> (on Windows)
#if FUN_FINDDATA_T //BUILD_MINGW
#include <io.h>

// JFL 03 Jun 19
int CoreDirStart(CoreDirRec *dr,cchr *path,cchr *pathx,int flags)
{
  int r,ssys,spath;
  cchr *slash,*s1;

  memset(dr,0,sizeof(*dr));

  ssys = sizeof(struct _finddata_t);
  spath = szsize(path,pathx);
  dr->allocSize = ssys + spath + 1024;
  if((r=CoreMemAlloc(&dr->alloc,dr->allocSize))<0)
    goto FINAL;
  dr->flags=flags;
  dr->sys=dr->alloc;
  dr->path=PTR_ADD(dr->sys,ssys);
  r=szcpy(dr->path,spath,path,pathx);
  if((slash=szlastslash(dr->path,dr->path+r))) {
    dr->basePathSize = szsize(dr->path,slash);
  }

  r=0;
FINAL:
  return r;
} // CoreDirStart()

// JFL 03 Jun 19
int CoreDirEntry(CoreDirRec *dr,cchr **outFile,cchr **outFilex)
{
  int r,s;
  struct _finddata_t *fd = (struct _finddata_t*)dr->sys;
  chr *s1,*sx;

  if(!dr->finder) {
    if((dr->finder=_findfirst(dr->path,fd))==-1)
      ret(0);
  } else {
    if(_findnext(dr->finder,fd))
      ret(0);
  }

  sx = PTR_ADD(dr->alloc,dr->allocSize);
  s1 = dr->path+dr->basePathSize;
  s = PTR_DIFF(sx,s1);
  r = szcpy(s1,s,fd->name,0);
  *outFile=dr->path,*outFilex=s1+r;
  dr->entryx=(chr*)*outFilex;

  r=(fd->attrib&_A_SUBDIR)?COREDIRENTRY_IS_DIR:COREDIRENTRY_IS_FILE;
FINAL:
  return r;
} // CoreDirEntry()

// JFL 03 Jun 19
void CoreDirFree(CoreDirRec *dr)
{
  if(dr->alloc)
    CoreMemFree(dr->alloc);
} // CoreDirFree()

#else

#if FUN_IO_H
#include <io.h>
#elif BUILD_DARWIN
#include <sys/uio.h>
#else // BUILD_LINUX
#include <sys/io.h>
#endif // BUILD_LINUX

#include <dirent.h>

// JFL 03 Jun 19
int CoreDirStart(CoreDirRec *dr,cchr *path,cchr *pathx,int flags)
{
  int r,spath,smatch,soff;
  cchr *slash,*s1;

  memset(dr,0,sizeof(*dr));

  if((slash=szlastslash(path,pathx))) {
    smatch=szsize(slash,pathx);
    soff=slash-path;
  } else {
    smatch=0;
  }
  spath = szsize(path,pathx);
  dr->flags=flags;
  dr->allocSize = smatch + spath + 1024;
  if((r=CoreMemAlloc(&dr->alloc,dr->allocSize))<0)
    goto FINAL;
  if(smatch)
    dr->sys = dr->alloc;
  dr->path=PTR_ADD(dr->alloc,smatch);
  r=szcpy(dr->path,spath,path,pathx);

  // copy name to match into dr->sys
  if(dr->sys) {
    dr->basePathSize = soff;
    szcpy(dr->sys,smatch,path+soff+1,pathx);
    dr->path[soff]=0; // remove last slash
  }

  r=0;
FINAL:
  return r;
} // CoreDirStart()

// JFL 03 Jun 19
int CoreDirEntry(CoreDirRec *dr,cchr **outFile,cchr **outFilex)
{
  int r,s;
  DIR *dir=(void*)dr->finder;
  struct dirent *dp;
  chr *s1;

  if(!dir) {
    if(!(dir=opendir(dr->path)))
      ret(0);
    dr->finder=(intptr_t)dir;
  }

  for(;;) {
    if(!(dp=readdir(dir)))
      ret(0);
    if((dr->flags&M_COREDIRSTART_ALLDIRS)&&(dp->d_type==DT_DIR))
      break;
    //r=fnmatch(dr->sys,dp->d_name,0);
    //PRINT_SYS_VRB("CoreDirEntry '%s' fnmatch '%s' = %d\n",dr->sys,dp->d_name,r);
    if(!fnmatch(dr->sys,dp->d_name,0))
      break;
  } // for

  s=dr->allocSize-dr->basePathSize;
  s1=dr->path+dr->basePathSize;
  r=szfmt(s1,s,"/%s",dp->d_name);
  *outFile=dr->path,*outFilex=s1+r;
  dr->entryx=(chr*)*outFilex;

  r=(dp->d_type==DT_DIR)?COREDIRENTRY_IS_DIR:COREDIRENTRY_IS_FILE;
FINAL:
  return r;
} // CoreDirEntry()

// JFL 03 Jun 19
void CoreDirFree(CoreDirRec *dr)
{
  if(dr->alloc)
    CoreMemFree(dr->alloc);
} // CoreDirFree()

#endif // BUILD_LINUX

///////////////////////////////////////////////////////////

// JFL 14 Oct 19
int CoreFileRecFnc(CoreFileRec *o,CoreFncOp op,CoreArgs args)
{
  switch(op) {
  default: break;
  case COREFNC_ZAP:
    CoreLinkUnlink(CL(o));
    CoreMemFree(o);
    break;
  } // switch
  return 0;
} // CoreFileRecFnc()

// JFL 14 Oct 19
int CoreFileRecNew(CoreFileRec **out,uns32 nameSize)
{
  int r;
  if((r=CoreLinkNew((void*)out,CORETYPE_FILEREC,
    (CoreFnc)CoreFileRecFnc,sizeof(**out)+nameSize))<0)
    goto FINAL;
  if(((*out)->nameSize=nameSize))
    (*out)->name=(void*)(1+*out);
  r=0;
FINAL:
  return r;
} // CoreFileRecNew()

///////////////////////////////////////////////////////////

// JFL 12 Oct 19
int CoreListFiles(CoreLink *list,cchr *path,cchr *pathx,int flags)
{
  int r,num=0,ff;
  cchr *s1,*lastslash;
  CoreFileRec *file;
  CoreDirRec drmem,*dr=0;
  chr *pathalloc=0;
  cchr *dotslash="./";
  //CorePrintf("CoreListFiles: '%S' ",path,pathx);

  // CorePrintf(" --> '%S'\n",path,pathx);
  lastslash=szlastslash(path,pathx);
  if(!lastslash) {
    int s = szsize(path,pathx)+8;
    if((r=CoreMemAlloc(&pathalloc,s))<0)
      goto FINAL;
    r=szfmt(pathalloc,s,"%s%S",dotslash,path,pathx);
    path=pathalloc,pathx=path+r;
  }

  ff=(flags&M_CORELISTFILES_RECURSE)?M_COREDIRSTART_ALLDIRS:0;
  if((r=CoreDirStart(&drmem,path,pathx,ff))<0)
    goto FINAL;
  dr=&drmem;

  cchr *fpath,*fpathx;
  while((r=CoreDirEntry(dr,&fpath,&fpathx))>0) {
    if((r==COREDIRENTRY_IS_DIR)&&(flags&M_CORELISTFILES_RECURSE)) {

      // skip . and ..
      if(!(s1=szlastslash(fpath,fpathx)))
        s1=fpath;
      else
        s1++;
      if(*s1=='.') {
        r=szlen(s1,fpathx);
        if((r==1)||((r==2)&&(s1[1]=='.')))
          continue; // skip
      }

      if((r=CoreDirCat(dr,&fpath,&fpathx,lastslash,pathx,0,0))<0)
        goto FINAL;
      if((r=CoreListFiles(list,fpath,fpathx,flags))<0)
        goto FINAL;
      num+=r;
      continue;
    } // recurse

    //
    // ADD FILE
    //

    num++;
    if((r=CoreFileRecNew(&file,szsize(fpath,fpathx)))<0)
      goto FINAL;
    CoreLinkBefore(list,CL(file));
    // if we added the ./ -- then don't add that here
    if(pathalloc && !szstart(dotslash,0,fpath,fpathx))
      fpath+=szlen(dotslash,0);
    szcpy(file->name,file->nameSize,fpath,fpathx);
  } // while
  r=num;
FINAL:
  if(pathalloc)
    CoreMemFree(pathalloc);
  if(dr)
    CoreDirFree(dr);
  return r;
} // CoreListFiles()

// JFL 16 Oct 19
int CoreGetLinesFromFile(chr **outAlloc,chr **outLastLine,chr **outLastLinex,
  int startLine,int numLines,cchr *fpath,cchr *fpathx)
{ // for now, get only last line
  int r,i,top,bak,nn;
  chr *alloc=0;
  cchr *s1,*s2,*end,*end2;
  FILE *fh=0;

  if(!(fh = fopen(fpath, "rb"))) {
    r=-99; goto FINAL;
  }

  // get file size
  fseek(fh, 0, SEEK_END);
  top = ftell(fh);
  fseek(fh, 0, SEEK_SET);

  bak=1024;
  for(;;) {
    if(bak>top)
      bak=top;

    if(alloc)
      CoreMemFree(alloc);
    if((r=CoreMemAlloc(&alloc,bak+1))<0)
      goto FINAL;
    fseek(fh, top-bak, SEEK_SET);

    if((r = fread(alloc, 1, bak, fh)) != bak) {
      r = -98; goto FINAL;
    }
    alloc[bak] = 0; // always term

    end = end2 = alloc+bak;
    nn=0;
loop:
    for(;;) {
      s1 = szrchr(alloc,end,'\n');
      s2 = szrchr(alloc,end,'\r');
      if(s1 && (end>s1)) end=s1;
      if(s2 && (end>s2)) end=s2;
      if(end2==end)
        break;
      end2=end;
      s1=szskipwhite(end2,alloc+bak);
      // first line must not be empty
      if(nn)
        nn--;
      else {
        if(!szeos(s1,alloc+bak))
          nn--;
      }
      if(nn<=startLine)
        goto found; // found non-empty line
    } // for

    if(bak>=top) {
      s1=alloc;
      break;
    }

    bak<<=1;
  } // for

found:

  if(outAlloc) {
    *outAlloc=alloc,alloc=0;
    if(outLastLine)
      *outLastLine=(chr*)s1;
    if(outLastLinex)
      *outLastLinex=(chr*)sztilleol(s1,0);
  } else {
   if(outLastLine) *outLastLine=0;
   if(outLastLinex) *outLastLinex=0;
  }

  r=0;
FINAL:
  if(alloc)
    CoreMemFree(alloc);
  if(fh)
    fclose(fh);
  return r;
} // CoreGetLinesFromFile()

// JFL 23 Oct 19
int CoreStringField(cchr **outStr,cchr **outStrx,cchr *str,cchr *strx,int n,int sep)
{ // used to find fields inside a line, e.g. for CSV
  int r;
  cchr *s0,*s1;

  for(;str;) {
    str=szskipwhite(str,strx);
    if(szeos(str,strx))
      break;

    // s0..s1 with str at next char
    s0=str;
    if((*str=='\"')||(*str=='\'')) {
      s0=str+1;
      if(!(s1=szchr(s0,strx,*str)))
        ret(-2);
      str=s1+1;
    } else {
      s1=szchr(s0,strx,sep);
      if((str=s1)) str++;
    }

    if(n--<=0) {
      if(outStr) *outStr=s0;
      if(outStrx) *outStrx=s1;
      ret(0);
    }

  } // for
  r=-1;
FINAL:
  return r;
} // CoreStringField()

// JFL 26 Nov 19
static int coreArgFind(cchr *str,cchr *strx,int outtype,void *outp,
  int argc,chrc **argv)
{
  int r;
  for(;argc;) {
    cchr *s1 = *argv++;argc--;
    if(szstart(s1,0,str,strx))
      continue;
    if(outtype=='I') {
      if(outp)
        *((int*)outp)=1;
      ret(0);
    }
    if(argc<=0) ret(-1);
    cchr *s2 = *argv++;argc--;
    // CorePrintf(">> %s %s\n",s1,s2);
    if(outtype=='s')
      *((cchr**)outp)=s2;
    else if(outtype=='i')
      sztobin(s2,0,10,outtype,outp);
    ret(0);
  } // for
  r=-1;
FINAL:
  return r;
} // coreArgFind()

// JFL 04 Nov 19
// JFL 26 Nov 19; continue on error
int CoreArg(int argc,chrc **argv,cchr *sp,...)
{ // argc/argv help -- i=int, s=cchr*
  int r,fail=0,count=0;
  va_list args;
  cchr *lookfor,*lookforx;
  int dsttype;
  void *dstp;
  chr c;

  va_start(args,sp);

  while((c=*sp++)) switch(c) {
  case 'a': // <arg><dsttype><lookfor>(void *outDstPtr)
    dsttype=*sp++; // get type
    lookfor=sp;
    lookforx=sztillwhite(lookfor,0);
    sp=szskipwhite(lookforx,0);
    dstp=va_arg(args,void*);
    if((r=coreArgFind(lookfor,lookforx,dsttype,dstp,argc,argv))<0)
      fail=r;
    else
      count++; // keep track of args found
    break;
  } // while switch

  r=fail?fail:count; // return error or number found
FINAL:
  va_end(args);
  return r;
} // CoreArg()

// JFL 29 Nov 19
int CoreFileDependency(cchr *fdst,...) {
  int depends=1;
  cchr *fname;
  va_list args;
  va_start(args,fdst);
  uns64 t0,t1;
  if(CoreFileSizeTime(0,&t0,fdst,0,0)<0)
    goto FINAL;
  for(;;) {
    if(!(fname=va_arg(args,cchr*))) break;
    if(CoreFileSizeTime(0,&t1,fname,0,0)<0)
      continue; // ignore if not found
    if(t1>=t0)
      goto FINAL;
  } // for
  depends=0;
FINAL:
  va_end(args);
  return depends;
} // CoreFileDependency()

// EOF
