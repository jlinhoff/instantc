# file: make-local.mk - Joe Linhoff
LIB=../lib/libcore.a

.PHONY: all clean help
NODEPS=clean help

CC=gcc
AR=ar
LD=gcc
DEL=rm -f

CFLAGS=-I. -I.. -O2 -DBUILD_PFM_LIB
ARFLAGS:=rc

UNAME:=$(shell uname -s)
ifneq ($(findstring MINGW,$(UNAME)),)
CFLAGS+=-DBUILD_MINGW
else ifneq ($(findstring Darwin,$(UNAME)),)
CFLAGS+=-DBUILD_DARWIN
else ifneq ($(findstring CYGWIN,$(UNAME)),)
CFLAGS+=-DBUILD_CYGWIN
else
CFLAGS+=-DBUILD_LINUX
endif

GCCINFO:=../gccinfo.txt

FUNFLAGS=$(shell $(CC) -x c -DFUN_IO_H $(GCCINFO) -o gccinfo 2> /dev/null && ./gccinfo && $(DEL) gccinfo)
FUNFLAGS+=$(shell $(CC) -x c -DFUN_FINDDATA_T $(GCCINFO) -o gccinfo 2> /dev/null && ./gccinfo && $(DEL) gccinfo)
FUNFLAGS+=$(shell $(CC) -x c -DFUN_GMTIME_R $(GCCINFO) -o gccinfo 2> /dev/null && ./gccinfo && $(DEL) gccinfo)
FUNFLAGS+=$(shell $(CC) -x c -DFUN_GMTIME_S $(GCCINFO) -o gccinfo 2> /dev/null && ./gccinfo && $(DEL) gccinfo)
CFLAGS+=$(FUNFLAGS)

all: $(LIB)

SRC=$(wildcard *.c) # list of source files

SUFFIXES += .d
DEPFILES=$(patsubst %.c,%.d,$(SRC))

ifeq (0, $(words $(findstring $(MAKECMDGOALS), $(NODEPS))))
-include $(DEPFILES)
endif

$(LIB): $(SRC:%.c=%.o)
	@echo building $(LIB)
	$(AR) $(ARFLAGS) $@ $(SRC:%.c=%.o)

%.d: %.c
	$(CC) $(CFLAGS) -MM -MT '$(patsubst %.c,%.o,$<)' $< -MF $@

%.o: %.c %.d %.h
	$(CC) $(CFLAGS) -o $@ -c $<

clean:
	$(DEL) $(LIB) *.o *.d

help:
	@echo make clean -- removes all built files
	@echo make -- build
