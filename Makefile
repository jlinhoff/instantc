# Makefile for InstantC
# Copyright (C) 2019-2024 Joe Linhoff, see LICENSE file

$(info InstantC Makefile v12)

.PHONY: all clean scripts man install uninstall help tests extra

ifeq ($(OS), Windows_NT)
UNAME := Windows
else
UNAME := $(shell uname -s)
endif

ifneq ($(findstring Window,$(UNAME)),)
# MSYS2
WINDOWS := 1
SUDO ?=
INSTALLMANARGS:=-m 0644
RMD := rm -rf
LN := ln -fs
# BUILDDIR:=$(shell cygpath -wa .)\\
else
SUDO ?= sudo
INSTALLMANARGS := -g 0 -o 0 -m 0644
RMD := rm -f
LN := ln -fs
endif

APP := instantc
APPCC := instantcc
EC := @echo
RM := rm -f
MKDIR := mkdir -p
CP := cp

BUILDDIR:=$(dir $(realpath $(firstword $(MAKEFILE_LIST))))
BUILDBINDIR:=$(BUILDDIR)bin/
INSTALLBINDIR:=/usr/local/bin/
INSTALLDIR:=/usr/local/
MANDIR:=/usr/local/man/man1/
SCRIPTDIR:=scripts/
TESTDIR:=tests/
EXTRADIR:=extra/
PRJDIR:=prj/
MANFILE:=$(APP).1
REBUILD:=+rebuild
CHMODX:=chmod +x
RUN:=instantc
MAKE:=make --no-print-directory

ifeq ($(WINDOWS),1)
DOSBUILDBINDIRAPP=$(subst \,\\,$(shell cygpath -wa $(BUILDBINDIR)$(APP)))
DOSBUILDDIR=$(subst \,\\,$(shell cygpath -wa $(BUILDDIR)))
LNAPP=cmd //C "mklink /H $(APP) $(DOSBUILDBINDIRAPP)"
LNAPPCC=cmd //C "mklink /H $(APPCC) $(DOSBUILDBINDIRAPP)"
LNDIR=cmd //C "mklink /J $(APP) $(DOSBUILDDIR)"
else
LNAPP=sudo $(LN) $(BUILDBINDIR)$(APP)
LNAPPCC=sudo $(LN) $(BUILDBINDIR)$(APP) $(APPCC)
LNDIR=sudo $(LN) $(BUILDDIR) $(APP)
endif

all:
	@$(MAKE) -C core -f make-local.mk $@
	@$(MAKE) -C prj/src -f make-local.mk $@

clean:
	@$(MAKE) -C core -f make-local.mk $@
	@$(MAKE) -C prj/src -f make-local.mk $@
	@$(MAKE) -C $(SCRIPTDIR) -f make-local.mk $@
	@$(MAKE) -C $(TESTDIR) -f make-local.mk $@

scripts:
	@$(MAKE) -C $(SCRIPTDIR) -f make-local.mk

man:
	$(EC) installing man page
	@$(SUDO) mkdir -p $(MANDIR)
	@$(SUDO) install $(INSTALLMANARGS) $(BUILDDIR)prj/$(MANFILE) $(MANDIR)
	@$(SUDO) gzip -f $(MANDIR)$(MANFILE)

rcfile:
	$(EC) installing rcfile if not-yet-installed
	@if [ ! -f "$(HOME)/.instantc" ]; then $(CP) $(PRJDIR)dotinstantc $(HOME)/.instantc; fi

integration:
	$(EC) adding to system, BUILDDIR=$(BUILDDIR)
	@$(SUDO) $(RMD) $(INSTALLDIR)$(APP)
	@$(SUDO) $(RM) $(INSTALLBINDIR)$(APP)
	@$(CHMODX) $(SCRIPTDIR)*
	@$(CHMODX) $(TESTDIR)*
	@$(CHMODX) $(EXTRADIR)*
	@if [ ! -d "$(INSTALLBINDIR)" ]; then $(SUDO) $(MKDIR) $(INSTALLBINDIR); fi
	@if [ ! -f "$(INSTALLBINDIR)$(APP)" ]; then cd $(INSTALLBINDIR) && $(LNAPP); fi
	@if [ ! -f "$(INSTALLBINDIR)$(APPCC)" ]; then cd $(INSTALLBINDIR) && $(LNAPPCC); fi
	@if [ ! -d "$(INSTALLDIR)$(APP)" ]; then cd $(INSTALLDIR) && $(LNDIR); fi

#	@if [ ! -f "$(INSTALLBINDIR)$(APP)" ]; then echo "A cd:$(INSTALLBINDIR) ln:$(BUILDBINDIR)$(APP)" && cd $(INSTALLBINDIR) && $(SUDO) $(LN) $(BUILDBINDIR)$(APP) && pwd && ls; fi
#	@if [ ! -d "$(INSTALLDIR)$(APP)" ]; then echo "B cd:$(INSTALLDIR) ln:$(BUILDDIR)" && cd $(INSTALLDIR) && $(SUDO) $(LN) $(BUILDDIR) $(APP) && pwd && ls; fi


install: all rcfile integration scripts tests man
	$(EC) installing $(APP)

uninstall:
	$(EC) uninstalling $(BUILDDIR)
	@sudo $(RM) $(INSTALLBINDIR)$(APP)
	@sudo $(RMD) $(INSTALLDIR)$(APP)
	@sudo $(RM) $(MANDIR)$(MANFILE)*
	@$(RM) $(HOME)/.instantc

help:
	$(EC) make all -- to rebuild all files
	$(EC) make clean -- remove all files that were built
	$(EC) make install -- install into $(INSTALLBINDIR)
	$(EC) make uninstall -- uninstall $(INSTALLBINDIR)
	$(EC) make man -- install $(MANLOCAL) into $(MANDIR)$(MANFILE)
	$(EC) make tests -- run all tests
	$(EC) -- notes ----------
	$(EC) Install adds a sym link from $(INSTALLBINDIR)$(APP) to $(BUILDDIR)

tests:
	$(EC) running tests
	@$(RUN) $(TESTDIR)test_hello $(REBUILD) InstantCompiler
	@$(RUN) $(TESTDIR)test_sz $(REBUILD)
	@$(RUN) $(TESTDIR)test_retcode $(REBUILD) 4 || [ $$? -eq 4 ]
	@$(RUN) $(TESTDIR)test_corelib $(REBUILD)
	@$(RUN) $(TESTDIR)test_corecpp $(REBUILD)
	@$(RUN) $(TESTDIR)test_mathlib $(REBUILD)
	@$(RUN) $(TESTDIR)test_shell $(REBUILD)
	@$(RUN) $(TESTDIR)test_corecpp $(REBUILD)
	@$(RUN) $(TESTDIR)test_alpha1 $(REBUILD)
	@$(RUN) $(TESTDIR)test_beta1 $(REBUILD)
	@$(RUN) $(TESTDIR)test_beta2 $(REBUILD) || [ $$? -eq 123 ]
	@$(RUN) $(TESTDIR)test_beta3 $(REBUILD)
	@$(RUN) $(TESTDIR)test_lib1 $(REBUILD)
	@$(RUN) $(TESTDIR)test_lib2 $(REBUILD)
	@$(RUN) $(TESTDIR)test_lib3 $(REBUILD)

extra:
	$(EC) building ymlcore
	extra/yamlcore +rebuild

# EOF
