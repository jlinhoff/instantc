// main.c -- Copyright (C) 2019-2025 Joe Linhoff
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License. You may obtain a copy
// of the License at http://www.apache.org/licenses/LICENSE-2.0 Unless required
// by applicable law or agreed to in writing, software distributed under the
// License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
// OF ANY KIND, either express or implied. See the License for the specific
// language governing permissions and limitations under the License.
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <inttypes.h>
#include "base.h"
#include "vers.h"
#include "stz.h"

// note on compiler flags to build
// - on Linux dev system: see make-local.mk
// - on Windows dev system: BUILD_MINGW;BUILD_WINRUN;FUN_GMTIME_S;DEBUG=1;BUILD_PFM_LIB=1;_WIN32

static chrc *MainCoprShort="Copyright (C) 2019-2024 Joe Linhoff - see LICENSE file";

#ifdef FUN_DIRROOT
// example: -DFUN_DIRROOT="c:/dev/msys64/"
#define X(a) XX(a)
#define XX(a) #a
chrc *scWinRoot=X(FUN_DIRROOT);
#undef XX
#undef X
#else // ndef FUN_DIRROOT
chrc *scWinRoot="";
#endif // ndef FUN_DIRROOT

////////////////////////////////////////////////////////////////////////////////

static chrc *extC2 = "c";
static chrc *extCPP2 = "cpp";
static chrc *extExe2 = "exe";
static chrc *dashSC = "-sc";
static chrc *dashBeta = "-bc";
static chrc *dashPre = "-pc";
static chrc *dashLib = "-lc";

static chrc *metaStart = "$(";
static const int metaStartLen = 2;
static chrc *metaEnd = ")";
static const int metaEndLen=1;

static chrc *rcfile=".instantc";

#define RUNPREFIX "./"

#define EXTCODE_SIZE_MAX (sizeof(extCPP)+1) // of C or CPP ext
#define FILEPATH_BUF_SIZE 2048 // replace with dynamic allocs
#define M_BLOCKID_REF 0x4000

#if DEBUG // used for development
#define DEV_RCFILE "D:\\ainstantc\\prj\\dotinstantc"
#endif // DEBUG

#define SC_ARGC "_scargc"
#define SC_ARGV "_scargv"
#define SC_MAINRC "_scmainrc"

static chrc *patchCInclude = "<stdio.h>";
static chrc *patchCIntInclude = "<stdint.h>";
static chrc *patchCCInclude = "<iostream>";
static chrc *patchDepCheckInclude = "<sys/stat.h>";
static chrc *patchLibH1 = "#include <%s>\n";
static chrc *patchLibH2 = "#include <%S.h>\n";
static chrc *patchMainHead = "int main(int "SC_ARGC",char const*"SC_ARGV"[])\n{\n" \
  "int "SC_MAINRC"=0;\n";
static chrc *patchMainTail = "return "SC_MAINRC";\n}\n";
static chrc *patchBetaInit = "int sc_beta=0;for(char const*s="SC_ARGV"[1];*s;)" \
  "sc_beta=(sc_beta*10)+(*s++-'0');\n";
static chrc *patchBetaStart = "if(sc_beta==%d){";
static chrc *patchBetaEnd = "}\n";

// FORWARDS
static int scMetaSet(BaseLink *list,chrc *orgbuf,chrc *str,chrc *strx);
static int scBuildPreCompileCmd(BaseLink *list,chrc *ccSrc,chrc *ccSrcx,chr **outCmdp);
//////////////////////////////////////////////////////

#define METATOKS_X \
  X(NONE) \
  X(ARGC) \
  X(ARGV) \
  X(BETA) \
  X(BLOCK) \
  X(CFLAGS) \
  X(EMIT) \
  X(FINAL) \
  X(GDB) \
  X(INCLUDE) \
  X(INCLUDELIB) \
  X(INIT) \
  X(LANG) \
  X(LFLAGS) \
  X(LIB) \
  X(LIBDEP) \
  X(LIBINLIB) \
  X(DEPCHECK) \
  X(LIBIMPLEMENTATION) \
  X(LIBINCLUDE) \
  X(LIBCODE) \
  X(LIBINIT) \
  X(LIBFINAL) \
  X(LIBHEAD) \
  X(LIBINTERFACE) \
  X(MAIN) \
  X(MAINRC) \
  X(FUNCTION) \
  X(FUNCTIONLIB) \
  X(FUNCTIONS) \
  X(SEC) \
  X(SECTION) \
  X(SET) \
  X(SHELL) \
  X(ALPHA) \
  X(SKIP) \
  X(REFID) \
  X(VERS) \
  X(ZMAX) \
  /* end of list */

// METASYSTOK_ user can't specify
#define METASYSTOKS_X \
  XX(INCLUDEBLOCK) \
  XX(INCLUDELIBBLOCK) \
  XX(FUNCTIONBLOCK) \
  XX(FUNCTIONLIBBLOCK) \
  XX(BETABLOCK) \
  XX(MAINBLOCK) \
  /* end of list */

#define XX(a) METASYSTOK_##a,
#define X(a) METATOK_##a,
typedef enum { METATOKS_X METASYSTOKS_X METATOK_NUM } metatok_enum;
#undef X
#undef XX

#define X(a) #a,
static chrc *metaTokTable[] = { METATOKS_X 0 };
#undef X

// blocktypes are different blocks in the file
#define BLOCKTYPES_X \
  X(NONE) \
  X(FIRST) \
  X(META) /* process all meta */ \
  X(INCLUDE) \
  X(INCLUDELIB) \
  X(LIBINTERFACE) \
  X(FUNCTIONLIB) \
  X(LIBIMPLEMENTATION) \
  X(FUNCTIONMAIN) \
  X(FUNCTIONBETA) \
  X(MAINHEAD) \
  X(MAININIT) \
  X(BETAINIT) \
  X(BETABODY) \
  X(MAINBODY) \
  X(MAINFINAL) \
  X(MAINTAIL) \
  X(LAST) \
  /* end of list */

#define X(a) BLOCKTYPE_##a,
typedef enum { BLOCKTYPES_X } blocktype_enum;
#undef X

#define X(a) #a,
static chrc *blockTypeTable[] = { BLOCKTYPES_X 0 };
#undef X

//////////////////////////////////////////////////////

int main_nobrk = 0;

// JFL 30 Jul 06
int MainLogOutput(chr* buf, uns len) {
  printf("%s",buf);
  return 0;
} // MainLogOutput()

#define X(a) "["#a"]",
chrc *printTypeNames[] = { PRINT_X };
#undef X

// JFL 02 May 18
void MainPrintf(int pf,chrc *fmt,...)
{
  chr buf[1024]; // max print size
  va_list args;
  va_start(args,fmt);

  if((pf<0)||(pf>NUM(printTypeNames))) pf = 0;
  int r = 0;
  if(pf)
    r += stzfmt(buf,sizeof(buf),"%s",printTypeNames[pf]);
  r += stzfmt_v(buf+r,sizeof(buf)-r,fmt,args);
  MainLogOutput(buf,r);

  va_end(args);
} // MainPrintf()

////////////////////////////////////////////////////////////////////////////////

#define OBJTYPES_X \
  X(None) \
  X(Buf) \
  X(Config) \
  X(SZ) \
  X(Mark) \
  /* end of list */

#define X(a) OBJTYPE_##a,
enum { OBJTYPES_X };
#undef X

enum {
  OBJFNC_NONE,
  OBJFNC_INIT,
  OBJFNC_ZAP,
};

////////////////////////////////////////

int ObjNoneFnc(void *o,int op,...) { return -1; }

////////////////////////////////////////

typedef struct {
  BaseLink lnk;
  chr *buf;
  uns32 bufTop;
  uns32 bufMax;
  uns8 flagFree;
  uns8 name;
} ObjBuf;

// JFL 18 Sep 19
int ObjBufFnc(ObjBuf *o,int op,...)
{
  va_list args;
  va_start(args,op);
  switch(op) {
  case OBJFNC_ZAP:
    if(o->flagFree)
      MemFree(o->buf);
    BaseLinkUnlink(BL(o));
    MemFree(o);
    break;
  } // switch
  va_end(args);
  return 0;
} // ObjBufFnc()

// JFL 18 Sep 19
int ObjBufNew(ObjBuf **out,uns32 maxSize)
{
  int r;
  ObjBuf *o;
  if((r=MemAlloz(&o,sizeof(*o)+maxSize))<0)
    goto BAIL;
  BaseLinkMakeNode(BL(o),OBJTYPE_Buf);
  if((o->bufMax=maxSize))
    o->buf=(void*)(o+1);

  *out=o;
  r=0;
BAIL:
  return r;
} // ObjBufNew()

////////////////////////////////////////

#define M_FILESCREATED_MAINLANG  0x0001
#define M_FILESCREATED_MAINEXE   0x0002
#define M_FILESCREATED_BETALANG  0x0010
#define M_FILESCREATED_BETAEXE   0x0020
#define M_FILESCREATED_LIBLANG   0x0100
#define M_FILESCREATED_LIBO      0x0200
#define M_FILESCREATED_LIBH      0x0400
#define M_FILESCREATED_LIBA      0x0800
#define M_FILESCREATED_PRECCLANG 0x1000
#define M_FILESCREATED_PRECCEXE  0x2000

#define M_OBJCONFIG_ISCPP    0x01
#define M_OBJCONFIG_DEBUGGER 0x02
#define M_OBJCONFIG_VERBOSE  0x04
#define M_OBJCONFIG_LAUNCH   0x08
#define M_OBJCONFIG_DEPCHECK 0x10
#define M_OBJCONFIG_DEPLIBCHECKINCLUDE 0x20

typedef struct {
  BaseLink lnk;
  uns64 fileSec64;
  chr *buf; // allocated buf
  chr *name; // pointer to name
  chr *dot; // pointer to dot
  chr *formPath; // pointer to last formPath call
  chr *bufHalf;
  chr *pathBuf;
  chr *winRoot;
  uns16 pathBufSize;
  uns16 formPathMax;
  uns16 dotToEndSize; //
  uns32 bufSize; // allocated buf size
  uns32 filesCreated; // M_FILESCREATED_
  uns8 flags;
  int betaPass;
  int libBuild;
  int argc;
  void *argv;
  chrc *orgpath;
  chrc *orgpathx;
  chr dashExtMain[16];
  chr dashExtBeta[16];
  chr dashExtPre[16];
  chr dashExtLib[16];
  chr cFlagStd[16];
  chr run[4];
  int pathTranslateDir;
  int pathTranslateRun;
  int pathTranslateFile;
  int numMetaTable[METATOK_NUM];
  uns16 nextRefId; // for references between objects
  blocktype_enum curBlockType;
  int curScanSectionTok;

  int debug; // use debug (not a mask-bit) to allow values
  int verbose;
  uns32 fhLineNum;

  chrc *cpath,*cpathx;
  chrc *ccpath,*ccpathx;
  chrc *arpath,*arpathx;
  chrc *dbpath,*dbpathx;
  chrc *sclib,*sclibx;
  chrc *sclibrel,*sclibrelx;
  chrc *cflags,*cflagsx;
  chrc *ccflags,*ccflagsx;
  chrc *ldcflags,*ldcflagsx;
  chrc *ldccflags,*ldccflagsx;
  chrc *arflags,*arflagsx;
  chrc *cstd,*cstdx;
  chrc *ccstd,*ccstdx;
  chrc *scdoc,*scdocx;
  chrc *dbarg,*dbargx;
  chrc *dbenv,*dbenvx;

} ObjConfig;

// JFL 18 Sep 19
int ObjConfigFnc(ObjConfig *o,int op,...)
{
  va_list args;
  va_start(args,op);
  switch(op) {
  case OBJFNC_ZAP:
    BaseLinkUnlink(BL(o));
    MemFree(o);
    break;
  } // switch
  va_end(args);
  return 0;
} // ObjConfigFnc()

// JFL 18 Sep 19
int ObjConfigNew(ObjConfig **out,uns32 bufSize)
{
  int r;
  ObjConfig *o;
  if((r=MemAlloz(&o,sizeof(*o)+bufSize))<0)
    goto BAIL;
  BaseLinkMakeNode(BL(o),OBJTYPE_Config);
  if((o->bufSize=bufSize))
    o->buf=(void*)(1+o);

  *out=o;
  r=0;
BAIL:
  return r;
} // ObjConfigNew()

////////////////////////////////////////

typedef struct {
  chrc *str;
  chrc *strx;
  uns32 fileOff;
  uns32 lineNum;
  uns16 refId;
} ObjSZOne;

typedef struct {
  BaseLink lnk;
  metatok_enum metaType; // METATOK_
  ObjSZOne *sz;
  chr *buf;
  uns32 szNum;
  uns32 szMax;
  uns32 bufSize;
} ObjSZ;

// JFL 29 Sep 19
int ObjSZFnc(ObjSZ *o,int op,...)
{
  va_list args;
  va_start(args,op);
  switch(op) {
  case OBJFNC_ZAP:
    BaseLinkUnlink(BL(o));
    MemFree(o);
    break;
  } // switch
  va_end(args);
  return 0;
} // ObjSZ()

// JFL 18 Sep 19
int ObjSZNew(ObjSZ **out,uns32 num,uns32 bufSize)
{
  int r;
  ObjSZ *o;

  bufSize=ALIGNMACH(bufSize);
  if((r=MemAlloz(&o,sizeof(*o)+num*sizeof(*o->sz)+bufSize))<0)
    goto BAIL;
  BaseLinkMakeNode(BL(o),OBJTYPE_SZ);
  if((o->szMax=num))
    o->sz=(void*)(1+o);
  if((o->bufSize=bufSize))
    o->buf=PTR_ADD(o,sizeof(*o)+num*sizeof(*o->sz));

  *out=o;
  r=0;
BAIL:
  return r;
} // ObjSZNew()

////////////////////////////////////////

typedef struct {
  BaseLink lnk;
  uns16 mType1;
  uns16 mType2;
  uns32 fhOff;
} ObjMark;

// JFL 13 Oct 19
int ObjMarkFnc(ObjMark *o,int op,...)
{
  va_list args;
  va_start(args,op);
  switch(op) {
  case OBJFNC_ZAP:
    BaseLinkUnlink(BL(o));
    MemFree(o);
    break;
  } // switch
  va_end(args);
  return 0;
} // ObjMarkFnc()

// JFL 13 Oct 19
int ObjMarkNew(ObjMark **out,uns16 mtype1,uns16 mtype2)
{
  int r;
  ObjMark *o;
  if((r=MemAlloz(&o,sizeof(*o)))<0)
    goto BAIL;
  BaseLinkMakeNode(BL(o),OBJTYPE_Mark);
  o->mType1=mtype1;
  o->mType2=mtype2;

  *out=o;
  r=0;
BAIL:
  return r;
} // ObjMarkNew()

////////////////////////////////////////

// JFL 09 Jun 19
void ObjListZap(BaseLink *list)
{
  BaseLink *lnk;
  while((lnk=list->n)&&lnk->t) {
    switch(lnk->t) {
    default:
      PRINT_SYS_ERR("BaseListZap %d not handled\n",lnk->t);
      BaseLinkUnlink(lnk);
      MemFree(lnk);
      break;
    #define X(a) case OBJTYPE_##a: Obj##a##Fnc((void*)lnk,OBJFNC_ZAP);break;
    OBJTYPES_X
    #undef X
    } // switch
  } // while
} // ObjListZap()

////////////////////////////////////////////////////////////////////////////////

// JFL 13 Oct 19
static int scAddMarks(BaseLink *list)
{
  int r,i;
  ObjMark *omark;
  int const marks[] = {
    BLOCKTYPE_FIRST,BLOCKTYPE_META,
    BLOCKTYPE_INCLUDE,BLOCKTYPE_INCLUDELIB,
    BLOCKTYPE_FUNCTIONLIB,BLOCKTYPE_FUNCTIONMAIN,BLOCKTYPE_FUNCTIONBETA,
    BLOCKTYPE_MAINHEAD,BLOCKTYPE_MAININIT,
    BLOCKTYPE_BETAINIT,BLOCKTYPE_BETABODY,
    BLOCKTYPE_MAINBODY,BLOCKTYPE_MAINFINAL,
    BLOCKTYPE_MAINTAIL,BLOCKTYPE_LAST};

  for(i=0;i<NUM(marks);i++) {
    if((r=ObjMarkNew(&omark,marks[i],0))<0)
      goto BAIL;
    BaseLinkBefore(list,BL(omark));
  } // for

  r=0;
BAIL:
  return r;
} // scAddMarks()

////////////////////////////////////////////////////////////////////////////////

// JFL 13 Oct 19
BaseLink* scBlockNext(BaseLink *lnk,uns16 mtype1,uns16 mtype2)
{
  ObjMark *omark;
  for(lnk=lnk->n;lnk->t;lnk=lnk->n) {
      if(lnk->t!=OBJTYPE_Mark) continue;
      omark=(ObjMark*)lnk;
      if(mtype1 && (mtype1!=omark->mType1)) continue;
      if(mtype2 && (mtype2!=omark->mType2)) continue;
      return lnk;
  } // for
  return 0;
} // scBlockNext()

// JFL 13 Oct 19
BaseLink* scBlockFind(BaseLink *list,uns16 mtype1,uns16 mtype2,int flags)
{
  BaseLink *lnk;
  ObjMark *omark;
  for(lnk=list->n;lnk->t;lnk=lnk->n) {
      if(lnk->t!=OBJTYPE_Mark) continue;
      omark=(ObjMark*)lnk;
      if(mtype1 && (mtype1!=omark->mType1)) continue;
      if(mtype2 && (mtype2!=omark->mType2)) continue;
      goto found;
  } // for
  lnk=0;
  goto done;
found:
  if(flags) { // find-next
    // next block - any type
    lnk=scBlockNext(lnk,0,0);
  } // find-next

done:
  return lnk;
} // scBlockFind()

////////////////////////////////////////////////////////////////////////////////

// JFL 16 Sep 19
static ObjConfig* scGetConfig(BaseLink *list)
{
  BaseLink *lnk;
  for(lnk=list->n;lnk->t;lnk=lnk->n) {
    if(lnk->t==OBJTYPE_Config)
      return (ObjConfig*)lnk;
  } // for
  return 0;
} // scGetConfig()

#define FORMPATHS_X \
  X(NONE) \
  X(MAINLANG) \
  X(MAINEXEFILE) \
  X(MAINEXERUN) \
  X(LIBHDEF) \
  X(LIBHREF) \
  X(LIBLANG) \
  X(LIBDIR) \
  X(LIBO) \
  X(LIBA) \
  X(BETAEXEFILE) \
  X(BETAEXERUN) \
  X(BETALANG) \
  X(PRECCEXEFILE) \
  X(PRECCEXERUN) \
  X(PRECCEXELANG) \
  /* end-of-list */

#define X(a) FORMPATH_##a,
typedef enum { FORMPATHS_X } formpath_t;
#undef X

#define M_SCPATHTRANSLATE_WINSLASH 0x01
#define M_SCPATHTRANSLATE_DOUBLESLASH 0x02
#define M_SCPATHTRANSLATE_UNIXSLASH 0x04
#define M_SCPATHTRANSLATE_ROOT 0x08

static int scPathTranslate(chr *dst,int32_t dstSize,
  chrc *pre,chrc *prex,
  chrc *src,chrc *srcx,int flags) {
  chr *dst0=dst,*dstx=dst+dstSize;
  chr c;

  if(!pre)
    goto prefix_done;

  for(;dst<dstx;) {
    if(prex&&(pre>=prex)) c = 0;
    else c=*pre++;

    if((c=='/') && (flags&M_SCPATHTRANSLATE_WINSLASH)) {
      c='\\';
    } else if((c=='\\')&&(flags&M_SCPATHTRANSLATE_UNIXSLASH)) {
      if((pre[1]==c)&&(flags&M_SCPATHTRANSLATE_DOUBLESLASH))
        pre++; // remove
      c='/';
    }

    *dst=c;
    if(!c) break;
    dst++;

    if((c=='\\')&&(flags&M_SCPATHTRANSLATE_DOUBLESLASH)&&((dst+1)<dstx))
      *dst++=c;

  } // for

prefix_done:

  for(;dst<dstx;) {
    if(srcx&&(src>=srcx)) c = 0;
    else c=*src++;

    if((c=='/') && (flags&M_SCPATHTRANSLATE_WINSLASH)) {
      c='\\';
    } else if((c=='\\')&&(flags&M_SCPATHTRANSLATE_UNIXSLASH)) {
      if((src[1]==c)&&(flags&M_SCPATHTRANSLATE_DOUBLESLASH))
        src++; // remove
      c='/';
    }

    *dst=c;
    if(!c) break;
    dst++;

    if((c=='\\')&&(flags&M_SCPATHTRANSLATE_DOUBLESLASH)&&((dst+1)<dstx))
      *dst++=c;

  } // for
  return dst-dst0;
} // scPathTranslate()

#define X(a) #a,
static chrc *formPathName[] = {FORMPATHS_X};
#undef X

// JFL 19 Nov 19
// JFL 31 Jul 20
// JFL 19 Jan 21
static int scFormPath(ObjConfig *oconfig,formpath_t formpath,int scpath)
{
  int r;
  chrc *ext,*dash,*prefix=0;

  oconfig->formPath=oconfig->pathBuf;
  oconfig->formPathMax=oconfig->pathBufSize;
  switch(formpath) {
  default:
    bret(-20);
  case FORMPATH_MAINLANG:
    ext = (oconfig->flags&M_OBJCONFIG_ISCPP)?extCPP2:extC2;
    dash = oconfig->betaPass?oconfig->dashExtBeta:oconfig->dashExtMain;
    r=stzfmt(oconfig->bufHalf,oconfig->formPathMax,"%S%s.%s",
      oconfig->buf,oconfig->dot,dash,ext);
    break;
  case FORMPATH_MAINEXEFILE:
    r=stzfmt(oconfig->bufHalf,oconfig->formPathMax,"%S%s.%s",
      oconfig->buf,oconfig->dot,oconfig->dashExtMain,extExe2);
    //PRINT_SYS_VRB("FORMPATH_MAINEXEFILE:%s prefix:%s\n",oconfig->bufHalf,prefix);
    break;
  case FORMPATH_MAINEXERUN:
    r=stzfmt(oconfig->bufHalf,oconfig->formPathMax,"%S%s.%s",
      oconfig->buf,oconfig->dot,
      oconfig->dashExtMain,extExe2);
    if(!stzcountchrs(oconfig->bufHalf,0,"/\\",0))
      prefix=oconfig->run; // add prefix only if no dir-slash
    //PRINT_SYS_VRB("FORMPATH_MAINEXERUN:%s buf.dot:%S prefix:%s\n",
    //  oconfig->bufHalf,oconfig->buf,oconfig->dot,prefix);
    break;
  case FORMPATH_LIBHDEF:
    r=stzfmt(oconfig->bufHalf,oconfig->formPathMax,"%S%S.h",
      oconfig->sclib,oconfig->sclibx,
      oconfig->name,oconfig->dot);
    break;
  case FORMPATH_LIBHREF:
    r=stzfmt(oconfig->bufHalf,oconfig->formPathMax,"%S.h",
      oconfig->name,oconfig->dot);
    break;
  case FORMPATH_LIBO:
    r=stzfmt(oconfig->bufHalf,oconfig->formPathMax,"%S.o",
      oconfig->buf,oconfig->dot);
    break;
  case FORMPATH_LIBA:
    r=stzfmt(oconfig->bufHalf,oconfig->formPathMax,"%Slib%S.a",
      oconfig->sclib,oconfig->sclibx,
      oconfig->name,oconfig->dot);
    break;
  case FORMPATH_LIBDIR:
    r=stzfmt(oconfig->bufHalf,oconfig->formPathMax,"%S",
      oconfig->sclib,oconfig->sclibx);
    break;
  case FORMPATH_LIBLANG:
    ext = (oconfig->flags&M_OBJCONFIG_ISCPP)?extCPP2:extC2;
    dash = oconfig->dashExtLib;
    r=stzfmt(oconfig->bufHalf,oconfig->formPathMax,"%S%s.%s",
      oconfig->buf,oconfig->dot,dash,ext);
    break;
  case FORMPATH_BETAEXEFILE:
    r=stzfmt(oconfig->bufHalf,oconfig->formPathMax,"%S%s.%s",
      oconfig->buf,oconfig->dot,
      oconfig->dashExtBeta,extExe2);
    break;
  case FORMPATH_BETAEXERUN:
    r=stzfmt(oconfig->bufHalf,oconfig->formPathMax,"%S%s.%s",
      oconfig->buf,oconfig->dot,oconfig->dashExtBeta,extExe2);
    if(!stzcountchrs(oconfig->bufHalf,0,"/\\",0))
      prefix=oconfig->run; // add prefix only if no dir-slash
    break;
  case FORMPATH_BETALANG:
    ext = (oconfig->flags&M_OBJCONFIG_ISCPP)?extCPP2:extC2;
    r=stzfmt(oconfig->bufHalf,oconfig->formPathMax,"%S%s.%s",
      oconfig->buf,oconfig->dot,oconfig->dashExtBeta,ext);
    break;
  case FORMPATH_PRECCEXEFILE:
    r=stzfmt(oconfig->bufHalf,oconfig->formPathMax,"%S%s.%s",
      oconfig->buf,oconfig->dot,
      oconfig->dashExtPre,extExe2);
    break;
  case FORMPATH_PRECCEXERUN:
    r=stzfmt(oconfig->bufHalf,oconfig->formPathMax,"%S%s.%s",
      oconfig->buf,oconfig->dot,oconfig->dashExtPre,extExe2);
    if(!stzcountchrs(oconfig->bufHalf,0,"/\\",0))
      prefix=oconfig->run; // add prefix only if no dir-slash
    break;
  case FORMPATH_PRECCEXELANG:
    ext = (oconfig->flags&M_OBJCONFIG_ISCPP)?extCPP2:extC2;
    r=stzfmt(oconfig->bufHalf,oconfig->formPathMax,"%S%s.%s",
      oconfig->buf,oconfig->dot,oconfig->dashExtPre,ext);
    break;
  } // switch

  if(scpath||prefix) {
    chrc *pre,*prex=0;
    if(!(pre=prefix)&&(scpath&M_SCPATHTRANSLATE_ROOT)) {
      if((oconfig->bufHalf[0]=='/')||(oconfig->bufHalf[0]=='\\')) {
        pre=oconfig->winRoot,prex=pre+stzlen(pre,0);
        if((prex[-1]=='/')||(prex[-1]=='\\'))
          prex--;
      }
    }

    // don't double up the prefix
    //  if(pre && !stzstart(pre,prex,oconfig->bufHalf,0))
    //    pre=0;

    r=scPathTranslate(oconfig->formPath,oconfig->formPathMax,
      pre,prex,
      oconfig->bufHalf,oconfig->bufHalf+r,scpath);
  }

  PRINT_SYS_VRB("scFormPath: %s '%s'\n",formPathName[formpath],oconfig->formPath);

  // r is len
BAIL:
  return r;
} // scFormPath()

static int scAddMeta(BaseLink *list,int tok,chrc *str,chrc *strx) {
  int r;
  BaseLink *lnk,*n;
  ObjSZ *osz;

  for(n=list->n;n->t;n=n->n) {
    if(n->t!=OBJTYPE_SZ) continue;
    osz=(ObjSZ*)n;
    if(osz->metaType!=tok) continue;
    if(osz->szNum<1) continue;
    if(!stzicmp(osz->sz[0].str,osz->sz[0].strx,str,strx))
      ret(1); // already added
  } // for

  if(!(lnk=scBlockFind(list,BLOCKTYPE_INCLUDE,0,1)))
    bret(-75);

  if((r=ObjSZNew(&osz,1,0))<0)
    goto BAIL;
  BaseLinkBefore(lnk,BL(osz));

  osz->metaType=tok;
  osz->szNum=1;
  osz->sz[0].str=str,osz->sz[0].strx=strx;

  r=0;
BAIL:
  return r;
} // scAddMeta()

// JFL 05 Oct 19
static int scHandleMeta(ObjConfig *oconfig,BaseLink *list,chrc *bufStart,
  chrc *str,chrc *strx)
{ // return metaTok
  int r;
  BaseLink *lnk;
  metatok_enum tok;
  blocktype_enum blocktype;
  chrc *s1,*s2;
  ObjSZ *osz;

  s1=stzskipwhite(str,strx);
  str=stzskipid(s1,strx);
  tok=stzimap(metaTokTable,s1,str);
  s2=stzskipwhite(str,strx);
  switch(tok) {
  default:
    PRINT_SYS_ERR("scHandleMeta line:%d tok-not-handled: '%S'\n",
      stzcountlines(bufStart,str),s1,str);
    BRK();
    break;
  case METATOK_SET:
  case METATOK_LANG:
    if((r=scMetaSet(list,bufStart,s2,strx))<0)
      goto BAIL;
    break;

  // change code section
  case METATOK_BLOCK:
    PRINT_SYS_ERR("'block' depricatedd, use 'section'\n");
  case METATOK_SEC:
  case METATOK_SECTION:
    s1=stztillwhite(s2,strx);
    tok=stzimap(metaTokTable,s2,s1);
    switch(tok) {
    default:
      PRINT_SYS_ERR("unknown section type '%S'\n",s2,s1);
      ret(-1);
    case METATOK_INCLUDELIB:
    case METATOK_LIBINTERFACE:
    case METATOK_LIBINCLUDE:
    case METATOK_LIBHEAD:
      oconfig->libBuild=1;
      tok=METASYSTOK_INCLUDELIBBLOCK;
      break;
    case METATOK_LIB:
    case METATOK_FUNCTIONLIB:
    case METATOK_LIBIMPLEMENTATION:
    case METATOK_LIBCODE:
      oconfig->libBuild=1;
      tok=METASYSTOK_FUNCTIONLIBBLOCK;
      break;
    case METATOK_FUNCTION:
    case METATOK_FUNCTIONS:
      tok=METASYSTOK_FUNCTIONBLOCK;
      break;
    case METATOK_MAIN:
      tok=METASYSTOK_MAINBLOCK;
      break;
    case METATOK_INCLUDE:
      tok=METASYSTOK_INCLUDEBLOCK;
      break;
    case METATOK_BETA:
      tok=METASYSTOK_BETABLOCK;
      break;
    } // switch
    oconfig->curScanSectionTok=tok;
    // tok set
    break;
  case METATOK_FUNCTION:
  case METATOK_FUNCTIONS:
    tok=METASYSTOK_FUNCTIONBLOCK;
    break;
  case METATOK_MAIN:
    tok=METASYSTOK_MAINBLOCK;
    break;
  include_block:
    tok=METASYSTOK_INCLUDEBLOCK;
    break;
  beta_block:
    tok=METASYSTOK_BETABLOCK;
    break;

  // include files and libraries
  case METATOK_INCLUDE:
    if(s2==strx) // treat as block-marker with empty meta-sub
      goto include_block;
    // drop through
  case METATOK_LIB:
    // lib's included in libraries are translated
    // b/c we need to control where the include files go
    if(oconfig->curScanSectionTok==METASYSTOK_FUNCTIONLIBBLOCK)
        tok=METATOK_LIBINLIB;
  case METATOK_LIBINLIB:
    if((r=scAddMeta(list,tok,s2,strx))<0)
      goto BAIL;
    break;

  // process during meta pass
  case METATOK_BETA:
    if(s2==strx) // treat as block-marker with empty meta-sub
      goto beta_block;
    // think we need another block
    blocktype=BLOCKTYPE_BETABODY;
    goto link_into_block;
  case METATOK_EMIT:
    r=0;
  case METATOK_SHELL:
  case METATOK_ALPHA:
  case METATOK_GDB:
  case METATOK_ARGC:
  case METATOK_ARGV:
  case METATOK_MAINRC:
  case METATOK_VERS:
  case METATOK_CFLAGS:
  case METATOK_LFLAGS:
    blocktype=BLOCKTYPE_META;
    link_into_block:

    // link all substitution data into the meta block
    if(!(lnk=scBlockFind(list,blocktype,0,1)))
      bret(-75);

    if((r=ObjSZNew(&osz,1,0))<0)
      goto BAIL;
    BaseLinkBefore(lnk,BL(osz));

    osz->metaType=tok;
    osz->szNum=1;
    osz->sz[0].str=s2,osz->sz[0].strx=strx;
    osz->sz[0].refId=++oconfig->nextRefId;
    tok=METATOK_REFID;

    break;
  } // switch

  r=tok;
BAIL:
  if(r<0)
    tok=METATOK_NONE;
  return r;
} // scHandleMeta()

// JFL 10 Oct 19
static int scMeta(chrc *str,chrc *strx,chrc *multilinex,
  chrc **sp,chrc **mp,chrc **ap,chrc **ep,chrc **np)
{ // return meta-token id
  // the quote is optional, can be any non-id character, must match at end
  // $("meta arg  ")   --
  // |  |    |    | \  --
  // s  m    a    e  n -- s:start m:meta a:arg e:end n:next
  int tok;
  chrc *s2, *s1=stzsub(metaStart,metaStart+metaStartLen,str,strx);
  if(!s1)
    return -1; // return <0 if not found
  if(sp)
    *sp=s1;

  chr match[metaEndLen+3],*mm=match;
  s1+=metaStartLen;
  if(!C_IS_ID(*s1)) {
    *mm++=*s1++;

    // with extra match chrs, allow multiline meta-blocks
    if(multilinex)
      strx=multilinex;
  }
  stzcpy(mm,sizeof(match),metaEnd,metaEnd+metaEndLen);

  if(mp) // meta-pointer
    *mp=s1;

  // look for match
  s2 = stzskipid(s1,strx);
  tok = stzimap(metaTokTable,s1,s2);
  if(tok<0) // not-found is OK -- not an error
    tok=METATOK_NONE;

  s2=stzskipwhite(s2,strx);
  if(ap) // arg-pointer
    *ap=s2;

  // look for match
  if(!(s1=stzsub(match,0,s2,strx))) {
    tok=-2;
    goto BAIL; // return <0 if incomplete
  }

  if(ep) // end-pointer
    *ep=s1;

  s1 += stzlen(match,0);
  if(np) // next-pointer
    *np=s1;

BAIL:
  return tok;
} // scMeta()

// JFL 13 Oct 19
static int scGetLastSZ(BaseLink *list,ObjSZ **out,
  uns16 markType,uns16 metaType,uns16 createNeedNum)
{
  int r;
  ObjSZ *osz,*oszlast=0;
  ObjMark *omark;
  BaseLink *lnk,*lnkbefore=0;

  // look for mark type
  for(lnk=list->n;lnk->t;lnk=lnk->n) {
    if(lnk->t!=OBJTYPE_Mark)
      continue;
    omark=(ObjMark*)lnk;
    if(omark->mType1!=markType)
      continue;
    goto found_mark1;
  } // for
  bret(-1);
found_mark1:

  // find last SZ in this block
  for(lnk=lnk->n;lnk->t;lnk=lnk->n) {
    if(lnk->t==OBJTYPE_Mark) {
      lnkbefore=lnk;
      break; // mark to end block
    }
    if(lnk->t!=OBJTYPE_SZ)
      continue;
    osz=(ObjSZ*)lnk;
    if(metaType && (metaType != osz->metaType))
      continue;
    oszlast=osz;
  } // for

  if(oszlast && ((oszlast->szMax-oszlast->szNum)>=createNeedNum)) {
    if(out)
      *out=oszlast;
    ret(0);
  }

  if(!createNeedNum || !lnkbefore)
    ret(1); // not created

  // create a few more than asked for
  if(createNeedNum<8)
    createNeedNum+=4;
  else
    createNeedNum+=createNeedNum>>1;

  if((r=ObjSZNew(&osz,createNeedNum,0))<0)
    goto BAIL;
  BaseLinkBefore(lnkbefore,BL(osz));
  osz->metaType=metaType;

  if(out)
    *out=osz;

  r=0;
BAIL:
  return r;
} // scGetLastSZ()

// JFL 16 Sep 19
static int scScan(BaseLink *list,chrc *scpath,chrc *scpathx)
{
  int r,s,nsubneed;
  metatok_enum tok;
  blocktype_enum blocktype;
  ObjBuf *obuf;
  ObjSZ *osz;
  ObjSZOne *sz;
  chr *srcalloc=0;
  chrc *s1,*s2,*buf,*bufx,*eol,*eos;
  ObjConfig *oconfig;

  if(!(oconfig=scGetConfig(list)))
    bret(-1);

  //
  // READ FILE
  //

  if((r=BaseReadFile(scpath,scpathx,&srcalloc,0))<0)
    goto BAIL;
  s=r; // size
  if((r=ObjBufNew(&obuf,0))<0)
    goto BAIL;
  BaseLinkBefore(list,BL(obuf));
  obuf->buf=srcalloc,srcalloc=0; // pass ownership
  obuf->flagFree=1; // request to be freed
  obuf->name=1; // only buffer with name for now
  buf=obuf->buf,bufx=obuf->buf+s;

  //
  // COUNT MAX SUBSTITUTIONS
  //

  nsubneed = 4;
  eol = buf;
  while((s1 = stzsub(metaStart,0,eol,bufx)))
    eol = s1+2, nsubneed++;

  //
  //
  //

  blocktype=BLOCKTYPE_MAINBODY;
  if((r=scGetLastSZ(list,&osz,blocktype,0,nsubneed))<0)
    goto BAIL;
  sz=0;

  //
  //
  //

  s1=stzskipwhite(buf,bufx);
  if((*s1=='#')&&(s1[1]=='!')) {
    // skip first line if it starts with #!
    s1=stztilleol(buf,bufx);
    eol=stzskipeol(s1,bufx);
  } else {
    eol=s1;
  }

  for(;;) {
    // eol is end-of-last string, start of next
    // FIX: must skip only eol if starts with eol, leave at least one eol..
    // problem with concat of pre-processor lines
    buf=stzskipwhite(eol,bufx); // skips white (& eol)
    if(stzeos(buf,bufx))
      break; // done
    eol=stztilleol(buf,bufx); // end-of-line
    s1=stzsub("//",0,buf,eol); // todo: handle false-positives
    eos=s1?s1:eol; // end-of-string
    if(stzeos(buf,eos))
      continue; // skip whole line

    // start a new string -- we may break this up later
    if(!sz) {
      if(osz->szNum>=osz->szMax)
        bret(-44);
      sz=osz->sz+osz->szNum; // bump up when we end the string
      sz->str=buf,sz->strx=bufx;
    }

    // look for well-formed meta-substitution
    // eos is end-of-statement (before eol)
    chrc *metastart=0,*metaend;
    if((s1=stzsub(metaStart,0,buf,eos))) { // look for meta-sub
      scMeta(s1,eos,bufx,&s1,&metastart,0,&metaend,&s2);
      if(eol < s2) // for multi-line blocks, find next eol
        eol = stztilleol(s2,bufx);

      // $(...)
      // |     |
      // s1    s2 (next)
    } // look for meta-sub

    if(metastart) { // meta-sub

      //
      // END CURRENT STRING
      //

      // end current string when we start a substitution
      if(sz->str!=s1) { // skip empty string
        sz->strx=s1;
        osz->szNum++;
        sz=0;
        nsubneed--;
      } // end current

      //
      // HANDLE META-SUBSTITUTION
      //

      // handle substitution
      oconfig->curBlockType=blocktype;
      if((r=scHandleMeta(oconfig,list,obuf->buf,metastart,metaend))<0)
        goto BAIL;
      tok=r;

      // meta-sub on end of line or meta-sub meta-sub -- skip whitespace
      // s2 is used for next chr
      if((s1=stzskipwhitespace(s2,eol))==eol)
        s2=stzskipwhitespace(eol,bufx);
      else if(!stzstart(metaStart,0,s1,0))
        s2=s1;

      // (possibly) switch block
      switch(tok) {
      default: r=blocktype; break;
      case METASYSTOK_INCLUDEBLOCK: r=BLOCKTYPE_INCLUDE; break;
      case METASYSTOK_INCLUDELIBBLOCK: r=BLOCKTYPE_INCLUDELIB; break;
      case METASYSTOK_FUNCTIONLIBBLOCK: r=BLOCKTYPE_FUNCTIONLIB; break;
      case METASYSTOK_FUNCTIONBLOCK: r=BLOCKTYPE_FUNCTIONMAIN; break;
      case METASYSTOK_BETABLOCK: r=BLOCKTYPE_FUNCTIONBETA; break;
      case METASYSTOK_MAINBLOCK: r=BLOCKTYPE_MAINBODY; break;
      } // switch

      if(blocktype!=r) {
        blocktype=r;
        if((r=scGetLastSZ(list,&osz,blocktype,0,nsubneed))<0)
          goto BAIL;
        sz=0;
      } // switch to block

      // start a new string -- we may break this up later
      if(!sz) {
        if(osz->szNum>=osz->szMax)
          bret(-44);
        sz=osz->sz+osz->szNum; // bump up when we end the string
      }

      //
      // START OF NEW STRING
      //

      // start of next string == end of meta-sub
      if(tok == METATOK_REFID) {
        // 'use' this sz, setup for next
        sz->str=s2,sz->strx=s2;
        sz->refId=oconfig->nextRefId | M_BLOCKID_REF; // depends on pre-inc
        if(osz->szNum>=osz->szMax)
          bret(-44);
        sz=osz->sz+(++osz->szNum); // bump up
      }
      sz->str=s2,sz->strx=bufx;

      eol=s2;
    } // meta-sub
  } // for

  // end current string if there is one
  if(sz) {
    sz->strx=s1;
    osz->szNum++;
  }

  // count
  BaseLink *lnk;
  for(lnk=list->n;lnk->t;lnk=lnk->n) {
    if(lnk->t!=OBJTYPE_SZ) continue;
    osz=(ObjSZ*)lnk;
    if((osz->metaType>=0) && (osz->metaType<NUM(oconfig->numMetaTable)))
      oconfig->numMetaTable[osz->metaType]++;
  } // for

  r=0;
BAIL:
  if(srcalloc)
    MemFree(srcalloc);
  return r;
} // scScan()

// JFL 10 Oct 19
static int scPatchLib(BaseLink *list,ObjSZOne *sz)
{
  int r,s;
  metatok_enum tok;
  blocktype_enum blocktype;
  chr buf[FILEPATH_BUF_SIZE];
  chr *filebuf=0,*filebufx;
  chrc *str,*a,*e,*n;
  ObjSZ *osz;
  ObjConfig *oconfig;
  BaseLink *lnk;

  if(!(oconfig=scGetConfig(list)))
    bret(-99);

  // lib + path
  if((r=scFormPath(oconfig,FORMPATH_LIBDIR,oconfig->pathTranslateDir))<0)
    goto BAIL;
  stzfmt(buf,sizeof(buf),"%s%S.h",oconfig->formPath,sz->str,sz->strx);
  PRINT_SYS_VRB("scPatchLib: %s\n",buf);
  if((r=BaseReadFile(buf,0,&filebuf,0))<0)
    bret(-77);
  filebufx=filebuf+r;

  for(str=filebuf;;) {

    // chrc **sp,chrc **mp,chrc **ap,chrc **ep,chrc **np)
    if((r=scMeta(str,filebufx,0,0,0,&a,&e,&n))<0)
      break;
    str=n;
    tok=r;

    switch(tok) {
    default:
      bret(-1);
    case METATOK_INIT:
    case METATOK_LIBINIT:
      blocktype=BLOCKTYPE_MAININIT;
      // drop through
      if(0) {
    case METATOK_FINAL:
    case METATOK_LIBFINAL:
      blocktype=BLOCKTYPE_MAINFINAL;
      }
      s=stzsize(a,e)+2; // add room for eol
      if((r=ObjSZNew(&osz,1,s))<0)
        goto BAIL;

      if(!(lnk=scBlockFind(list,blocktype,0,1)))
        bret(-99);
      BaseLinkBefore(lnk,BL(osz));

      r=stzcpy(osz->buf,osz->bufSize,a,e);
      osz->szNum=1;
      osz->sz[0].str=osz->buf;

      if(!C_IS_EOL(osz->buf[r])) {
        stzcpy(osz->buf+r,osz->bufSize-r,"\n",0);
      }

      break;
    case METATOK_LIBDEP:
      PRINT_SYS_MSG("- METATOK_LIBDEP is used -\n");
      e=stztrimwhite(a,e);
      if((r=scAddMeta(list,METATOK_LIB,a,e))<0)
        goto BAIL;
      break;
    } // switch
  } // for

  r=0;
BAIL:
 if(filebuf)
    MemFree(filebuf);
  return r;
} // scPatchLib()

static int scAddDepCheck(ObjConfig *oconfig,BaseLink *list,ObjSZOne *sz) {
  int r;
  BaseLink *lnk;
  ObjSZ *osz;

  if(!(lnk=scBlockFind(list,BLOCKTYPE_MAININIT,0,1)))
    bret(-31);
  if((r=ObjSZNew(&osz,1,0))<0)
    goto BAIL;
  BaseLinkBefore(lnk,BL(osz));
  osz->metaType=METATOK_DEPCHECK;
  osz->szNum=1;
  osz->sz[0]=*sz; // OK to share data

  // add the .h once
  if(!(oconfig->flags&M_OBJCONFIG_DEPLIBCHECKINCLUDE)) {
    oconfig->flags|=M_OBJCONFIG_DEPLIBCHECKINCLUDE;
    if(!(lnk=scBlockFind(list,BLOCKTYPE_INCLUDE,0,1)))
      bret(-30);
    if((r=ObjSZNew(&osz,1,0))<0)
      goto BAIL;
    BaseLinkBefore(lnk,BL(osz));
    osz->metaType=METATOK_INCLUDE;
    osz->sz[0].str=patchDepCheckInclude;
    osz->szNum=1;
  }

  r=0;
BAIL:
  return r;
} // scAddDepCheck()

// JFL 29 Sep 19
static int scPatch(BaseLink *list)
{
  int r,i;
  blocktype_enum blocktype;
  ObjSZ *osz;
  ObjSZOne *sz;
  BaseLink *lnk;
  ObjConfig* oconfig;

  if(!(oconfig=scGetConfig(list)))
    bret(-99);

  //
  // FIND LIBRARY INIT & FINAL
  //

  for(lnk=list->n;lnk->t;lnk=lnk->n) {
    if(lnk->t!=OBJTYPE_SZ) continue;
    osz=(ObjSZ*)lnk;
    if((osz->metaType!=METATOK_LIB)&&(osz->metaType!=METATOK_LIBINLIB)) continue;
    for(sz=osz->sz,i=0;i<osz->szNum;i++,sz++) {
      if((r=scPatchLib(list,sz))<0)
        goto BAIL;
      if(oconfig->flags&M_OBJCONFIG_DEPCHECK) { // add depcheck
        if((r=scAddDepCheck(oconfig,list,sz))<0)
          goto BAIL;
      } // add depcheck
    } // for
  } // for

  //
  // HEADERS
  //

  // add default include if no lib, no other includes
  if(!oconfig->numMetaTable[METATOK_INCLUDE]
    && !(oconfig->numMetaTable[METATOK_LIB]+oconfig->numMetaTable[METATOK_LIBINLIB])) {
    blocktype=BLOCKTYPE_INCLUDE;
    if(!(lnk=scBlockFind(list,blocktype,0,1)))
      bret(-30);
   
    if((r=ObjSZNew(&osz,1,0))<0)
      goto BAIL;
    BaseLinkBefore(lnk,BL(osz));
    osz->metaType=METATOK_INCLUDE;

    if(!(oconfig->flags&M_OBJCONFIG_ISCPP)) { // C
      osz->sz[0].str=patchCInclude;
      osz->szNum=1;

      if((r=ObjSZNew(&osz,1,0))<0)
        goto BAIL;
      BaseLinkBefore(lnk,BL(osz));
      osz->metaType=METATOK_INCLUDE;
      osz->sz[0].str=patchCIntInclude;
      osz->szNum=1;

    } else { // CC
      osz->sz[0].str=patchCCInclude;
      osz->szNum=1;
    } // CC
  } // default includes -- no other includes

  // add main header
  if(!(lnk=scBlockFind(list,BLOCKTYPE_MAINHEAD,0,1)))
    bret(-31);
  if((r=ObjSZNew(&osz,1,0))<0)
    goto BAIL;
  BaseLinkBefore(lnk,BL(osz));
  osz->sz[0].str=patchMainHead;
  osz->szNum=1;

  // code to choose beta-function
  if(oconfig->betaPass) {
    if(!(lnk=scBlockFind(list,BLOCKTYPE_BETAINIT,0,1)))
      bret(-31);
    if((r=ObjSZNew(&osz,1,0))<0)
      goto BAIL;
    BaseLinkBefore(lnk,BL(osz));
    osz->sz[0].str=patchBetaInit;
    osz->szNum=1;
  }

  //
  // FOOTERS
  //

  // add main final
  if(!(lnk=scBlockFind(list,BLOCKTYPE_MAINTAIL,0,1)))
    bret(-31);

  if((r=ObjSZNew(&osz,1,0))<0)
    goto BAIL;
  BaseLinkBefore(lnk,BL(osz));

  osz->sz[0].str=patchMainTail;
  osz->szNum=1;

  r=0;
BAIL:
  return r;
} // scPatch()

// JFL 09 Oct 19
static int scFindSZRef(ObjSZOne const **outp,BaseLink *list,uns16 metaType,uns16 refId)
{
  BaseLink *lnk;
  ObjSZ *osz;
  ObjSZOne *sz,*szx;

  for(lnk=list->n;lnk->t;lnk=lnk->n) {
    if(lnk->t!=OBJTYPE_SZ)
      continue;
    osz=(ObjSZ*)lnk;
    if(metaType && (osz->metaType!=metaType))
      continue;
    sz=osz->sz;
    szx=sz+osz->szNum;
    for(;sz!=szx;sz++) {
      if(sz->refId==refId) {
        if(outp)
          *outp=sz;
        return osz->metaType;
      }
    } // for
  } // for
  return -1;
} // scFindSZRef()

// JFL 10 Oct 19
static int scCleanResponse(chr *str,chr *strx,int flags)
{
  if(flags)
    goto done;

  if(!strx)
    strx=str+stzlen(str,0);
  if((strx>str) && (C_IS_EOL(strx[-1]))) {
    strx--;
    if(C_IS_EOL(strx[-1])&&(strx[-1]!=strx[0]))
      strx--;
  }

done:
  return stzlen(str,strx);
} // scCleanResponse()

#define WRITE(_buf_,_siz_,_fh_,_oconfig_) do{ \
  if(1!=fwrite(_buf_,_siz_,1,_fh_)) bret(-20); \
  (_oconfig_)->fhLineNum+=stzcountlines(_buf_,(_buf_)+(_siz_))-1; \
  }while(0)

// dep-check-note:
// - idea is to cause a script to build when a lib the script depends on changes
// = the determination is handled by the previously compiled script
// = script returns code when it determines it needs to be rebuilt
// - the script compares its build time agains all dependencies

#define MAINRC_REBUILD 121
#define MAINRC_REBUILD_STR "121"
//standard error codes
//1 - Catchall for general errors
//2 - Misuse of shell builtins (according to Bash documentation)
//126 - Command invoked cannot execute
//127 - “command not found”
//128 - Invalid argument to exit
//128+n - Fatal error signal “n”
//130 - Script terminated by Control-C
//255\* - Exit status out of range

#define FMT_DEPCHECK_QUIET /* code to run dep check */ \
  "{/* check dependency */ struct stat st;" \
  "char const *deppath=\"%s%S.h\";" \
  "if(stat(deppath,&st)<0) st.st_ctime=0;" \
  "if(st.st_ctime>%"PRIu64") {return "MAINRC_REBUILD_STR";}}\n"

#define FMT_DEPCHECK_VRB /* code to run dep check */ \
  "{/* check dependency */ struct stat st;" \
  "char const *deppath=\"%s%S.h\";" \
  "if(stat(deppath,&st)<0) st.st_ctime=0;" \
  "if(st.st_ctime>%"PRIu64") {int rr="MAINRC_REBUILD_STR";" \
  "printf(\"[DEP]return to rebuild (%%s) r:%%d\\n\",deppath,rr);return rr;}}\n"

// JFL 27 Mar 22
static int scPreCC(BaseLink *list,chr **resp,cchr *scr,cchr *scrx)
{
  int r;
  chr *cmd=0,*buildresp=0;
  ObjConfig *oconfig;

  if(!(oconfig=scGetConfig(list)))
    bret(-1);

  if((r=scBuildPreCompileCmd(list,scr,scrx,&cmd))<0)
    goto BAIL;

  oconfig->filesCreated|=M_FILESCREATED_PRECCEXE;
  PRINT_SYS_VRB("PCC> %s\n",cmd);
  if((r=BaseRunScript(&buildresp,cmd,0,BASERUNSCRIPT_QUIET))) {
    r=1; // failed compiles result in no output
    goto BAIL;
  }

  if((r=scFormPath(oconfig,FORMPATH_PRECCEXERUN,oconfig->pathTranslateDir))<0)
    goto BAIL;
  if((r=BaseRunScript(resp,oconfig->formPath,0,0))) {
    r=2;
    goto BAIL;
  }

  r=0;
BAIL:
  if(buildresp) MemFree(buildresp);
  if(cmd) MemFree(cmd);
  return r;
} // scPreCC()

// JFL 09 Oct 19
static int scWriteSubstitution(BaseLink *list,ObjSZ *osz,ObjSZOne *sz,FILE *fh,ObjConfig *oconfig)
{ // return non-zero to not-write the sz itself
  int r;
  metatok_enum metaType;
  ObjSZOne const *sz2;
  chr buf[2048];
  chr *resp=0,*rs=0,*rsx;

  if(sz->refId) {
    if((r=scFindSZRef(&sz2,list,0,sz->refId&~M_BLOCKID_REF))<0)
      bret(1);
    metaType=r;
  } else if(osz) {
    metaType=osz->metaType;
  } else {
    metaType=0;
  }

  switch(metaType) {
  default:
    PRINT_SYS_ERR("scWriteSubstitution unhandled type:%d\n",metaType);
    bret(1);
  case METASYSTOK_FUNCTIONLIBBLOCK:
    ret(0);
  case METATOK_CFLAGS:
  case METATOK_LFLAGS:
    break;
  case METATOK_GDB:
    ret(0);
  case METATOK_SHELL:
    PRINT_SYS_VRB("S> '%S'\n",sz2->str,sz2->strx);
    if((r=BaseRunScript(&resp,sz2->str,sz2->strx,0))<0) {
      r=stzfmt(buf,sizeof(buf),"/* SHELL '%S' */",sz2->str,sz2->strx);
      rs=buf,rsx=0;
    } else {
      if(r) PRINT_SYS_ERR("ERR* r=%d S> %S\n",r,sz2->str,sz2->strx);
      r=scCleanResponse(resp,0,0);
      rs=resp,rsx=rs+r;
    }
    break;
  case METATOK_ALPHA:
    PRINT_SYS_VRB("PCC> '%S'\n",sz2->str,sz2->strx);
    if((r=scPreCC(list,&resp,sz2->str,sz2->strx))<0) {
      r=stzfmt(buf,sizeof(buf),"/* PRECC '%S' */",sz2->str,sz2->strx);
      rs=buf,rsx=0;
    } else {
      // failure is OK
      if(r) PRINT_SYS_VRB("ERR* r=%d PCC> %S\n",r,sz2->str,sz2->strx);
      r=scCleanResponse(resp,0,0);
      rs=resp,rsx=rs+r;
    }
    break;
  case METATOK_BETA:
    if(oconfig->betaPass)
      break;
    if(!(sz->refId&M_BLOCKID_REF))
      break;
    if((r=scFormPath(oconfig,FORMPATH_BETAEXERUN,oconfig->pathTranslateRun))<0)
      goto BAIL;
    stzfmt(oconfig->formPath+r,oconfig->formPathMax-r," %d",
      sz->refId&~M_BLOCKID_REF);
    PRINT_SYS_VRB("B> %s\n",oconfig->formPath);
    if((r=BaseRunScript(&resp,oconfig->formPath,0,0))<0) {
      r=stzfmt(buf,sizeof(buf),"/* BETA '%s' */",oconfig->buf);
      rs=buf,rsx=0;
    } else {
      if(r) PRINT_SYS_ERR("ERR* r=%d B> %s\n",r,oconfig->formPath);
      r=scCleanResponse(resp,0,1);
      rs=resp,rsx=rs+r;
    }
    break;

  case METATOK_EMIT:
    r=stzfmt(buf,sizeof(buf),"%S",sz2->str,sz2->strx);
    rs=buf,rsx=0;
    break;
  case METATOK_ARGV:
    r=stzfmt(buf,sizeof(buf),SC_ARGV);
    rs=buf,rsx=0;
    break;
  case METATOK_ARGC:
    r=stzfmt(buf,sizeof(buf),SC_ARGC);
    rs=buf,rsx=0;
    break;
  case METATOK_MAINRC:
    r=stzfmt(buf,sizeof(buf),SC_MAINRC);
    rs=buf,rsx=0;
    break;
  case METATOK_VERS:
    r=stzfmt(buf,sizeof(buf),"%s",MAINVERS_SZ);
    rs=buf,rsx=0;
    break;
  case METATOK_INCLUDE:
    r=stzfmt(buf,sizeof(buf),"#include %S\n",sz->str,sz->strx);
    rs=buf,rsx=0;
    break;
  case METATOK_LIB:
  case METATOK_LIBINLIB:
    r=stzfmt(buf,sizeof(buf),"#include <%S.h>\n",sz->str,sz->strx);
    rs=buf,rsx=0;
    break;
  case METATOK_DEPCHECK: // write test into file that depends on lib
    if((r=scFormPath(oconfig,FORMPATH_LIBDIR,oconfig->pathTranslateDir))<0)
      goto BAIL;

    if(oconfig->flags&M_OBJCONFIG_VERBOSE) {
      r=stzfmt(buf,sizeof(buf),FMT_DEPCHECK_VRB,
        oconfig->formPath,sz->str,sz->strx,oconfig->fileSec64);
    } else {
      r=stzfmt(buf,sizeof(buf),FMT_DEPCHECK_QUIET,
        oconfig->formPath,sz->str,sz->strx,oconfig->fileSec64);
    }
    rs=buf,rsx=0;
    break;
  } // switch

  if((r=stzlen(rs,rsx))>0) {
    //sz->fileOff=ftell(fh);
    WRITE(rs,r,fh,oconfig);
  }

  r=metaType;
BAIL:
  if(resp)
    MemFree(resp);
  return r;
} // scWriteSubstitution()

// JFL 16 Nov 19
static int scLibPassFileMark(BaseLink *list,ObjConfig *oconfig,
  BaseLink *lnk,FILE *fh)
{
  int r;
  ObjMark *omark;

  if(lnk->t!=OBJTYPE_Mark)
    ret(1);
  omark=(ObjMark*)lnk;
  omark->fhOff=ftell(fh);

  r=0;
BAIL:
  return r;
} // scLibPassFileMark()

// JFL 16 Sep 19
// JFL 26 Oct 19
// JFL 06 Jan 20
// JFL 18 Nov 20; line numbers
static int scWriteCode(BaseLink *list,chrc *compileCmd)
{
  int r,i,skip=0;
  ObjConfig *oconfig;
  ObjSZ *osz;
  ObjSZOne *sz;
  FILE *fh=0;
  BaseLink *lnk;
  uns8 flagbeta=0;
  chr smbuf[64];

  if(!(oconfig=scGetConfig(list)))
    bret(-1);

  //
  // CREATE C or C++ FILE
  //

  if((r=scFormPath(oconfig,FORMPATH_MAINLANG,oconfig->pathTranslateFile))<0)
    goto BAIL;
  if(!(fh=fopen(oconfig->formPath,"wb")))
    bret(-10);
  oconfig->filesCreated|=M_FILESCREATED_MAINLANG;
  oconfig->fhLineNum=1;

  // WRITE HEADER
  r=stzfmt(smbuf,sizeof(smbuf),"// %s\n",oconfig->name);
  WRITE(smbuf,r,fh,oconfig);
  r=stzfmt(smbuf,sizeof(smbuf),"// ");
  WRITE(smbuf,r,fh,oconfig);
  r=stzlen(compileCmd,0);
  WRITE(compileCmd,r,fh,oconfig);
  r=stzfmt(smbuf,sizeof(smbuf),"\n");
  WRITE(smbuf,r,fh,oconfig);

  //
  // WRITE ALL IN ORDER
  //

  for(lnk=list->n;lnk->t;lnk=lnk->n) {

    //
    // IDENTIFY BLOCKS
    //

    if(lnk->t==OBJTYPE_Mark) { // show blocks
      ObjMark *omark=(ObjMark*)lnk;

      if(oconfig->betaPass) {
        // beta-pass
        if(omark->mType1 == BLOCKTYPE_MAINBODY)
          skip=1; // skip main on beta-pass
        else
          skip=0;
      } else {
        // main-pass
        if((omark->mType1 == BLOCKTYPE_FUNCTIONBETA)
          ||(omark->mType1 == BLOCKTYPE_BETAINIT)
          ||(omark->mType1 == BLOCKTYPE_BETABODY))
          skip=1; // skip beta on main-pass
        else
          skip=0;

        if(oconfig->libBuild) {
          if((r=scLibPassFileMark(list,oconfig,lnk,fh))<0)
            goto BAIL;
        }
      } // main-pass

      if(oconfig->flags&M_OBJCONFIG_VERBOSE) {
        chrc *s2=skip?"skipping ":"";
        chrc *s1 = (omark->mType1<NUM(blockTypeTable))
          ? blockTypeTable[omark->mType1]:"unk";
        r=stzfmt(smbuf,sizeof(smbuf),"/* %s%s */\n",s2,s1);
        WRITE(smbuf,r,fh,oconfig);
      }

    } // show blocks

    if(skip)
      continue;

    //
    //
    //

    if(lnk->t!=OBJTYPE_SZ)
      continue;
    osz=(ObjSZ*)lnk;

    switch(osz->metaType) {
    default:
      break;
    case METATOK_DEPCHECK:
    case METATOK_LIB:
    case METATOK_LIBINLIB:
    case METATOK_BETA:
    case METATOK_NONE:
    case METATOK_INCLUDE:

      for(sz=osz->sz,i=0;i<osz->szNum;i++,sz++) {

        sz->fileOff=ftell(fh);
        sz->lineNum=oconfig->fhLineNum;
        if((osz->metaType==METATOK_BETA)&&oconfig->betaPass) {
          chr buf[256];
          flagbeta=1;
          r=stzfmt(buf,sizeof(buf),patchBetaStart,sz->refId);
          WRITE(buf,r,fh,oconfig);
        }
        if(sz->refId) {
          if((r=scWriteSubstitution(list,0,sz,fh,oconfig))<0)
            goto BAIL;
          if(r==METATOK_SKIP)
            continue;
        } else if(osz->metaType) {
          if((r=scWriteSubstitution(list,osz,sz,fh,oconfig))<0)
            goto BAIL;
          continue;
        }

        if((r=stzlen(sz->str,sz->strx))) {
          WRITE(sz->str,r,fh,oconfig);
        }

        if(flagbeta) {
          flagbeta=0;
          r=stzlen(patchBetaEnd,0);
          WRITE(patchBetaEnd,r,fh,oconfig);
        }

      } // for

    } // switch
  } // for

  r=0;
BAIL:
  if(fh)
    fclose(fh);
  return r;
} // scWriteCode()

// JFL 25 Jun 21
static int scCreateLibFile(ObjConfig *oconfig,FILE **outFHp,formpath_t formpath) {
  int r;
  FILE *fh;

  if((r=scFormPath(oconfig,formpath,oconfig->pathTranslateFile))<0)
    goto BAIL;
  PRINT_SYS_VRB("writeLibFile: %s\n",oconfig->formPath);
  if(!(fh=fopen(oconfig->formPath,"wb"))) {
    PRINT_SYS_ERR("* fopen failed '%s'\n",oconfig->formPath);
    bret(-11);
  }

  *outFHp=fh;

  r=0;
BAIL:
  return r;
} // scCreateLibFile()

// JFL 16 Nov 19
// JFL 12 Nov 22
static int scWriteLibFile(ObjConfig *oconfig,FILE *fh,formpath_t formpath,
  BaseLink *list,chrc *srcbuf,uns32 off1,uns32 off2)
{
  int r,s,bufsize;
  chrc *p;
  chr *bufalloc=0;

  if(off1>=off2)
    bret(-10);

  switch(formpath) {
  default: bret(-7);

  case FORMPATH_LIBHDEF:
    oconfig->filesCreated|=M_FILESCREATED_LIBH;
    s=off2-off1;
    p=srcbuf+off1;
    if(1!=fwrite(p,s,1,fh))
      bret(-19);
    break;

  case FORMPATH_LIBLANG:
    oconfig->filesCreated|=M_FILESCREATED_LIBLANG;

    // alloc buffer for h file building

    if((r=scFormPath(oconfig,FORMPATH_LIBHREF,oconfig->pathTranslateDir))<0)
      goto BAIL;
    bufsize=stzsize(oconfig->formPath,0)+stzsize(patchLibH1,0)+64;
    if((r=MemAlloc(&bufalloc,bufsize))<0)
      goto BAIL;

    // add all .h from included libs

    { // add lib .h when lib is included in a lib
    BaseLink *lnk;
    ObjSZ *osz;
    ObjSZOne *sz;

    // kind of not right when we include libraries in other libraries
    // foists lib and lib-in-lib includes to the top of both .sc and .lc files
    if(oconfig->numMetaTable[METATOK_LIBINLIB]) { // lib
      for(lnk=list->n;lnk->t;lnk=lnk->n) {
        if(lnk->t!=OBJTYPE_SZ)
          continue;
        osz=(ObjSZ*)lnk;
        if(osz->metaType!=METATOK_LIBINLIB)
          continue;
        if(!osz->szNum)
          continue;
        sz=&osz->sz[0];

        r=stzfmt(bufalloc,bufsize,patchLibH2,sz->str,sz->strx);
        if(1!=fwrite(bufalloc,r,1,fh)) bret(-21);

      } // for
    } // lib
    } // add lib .h when lib is included in a lib

    // add .h for our own lib

    r=stzfmt(bufalloc,bufsize,patchLibH1,oconfig->formPath);
    if(1!=fwrite(bufalloc,r,1,fh))
      bret(-18);

    s=off2-off1;
    p=srcbuf+off1;
    if(1!=fwrite(p,s,1,fh))
      bret(-19);
    break;
  } // switch

  r=0;
BAIL:
  if(bufalloc)
    MemFree(bufalloc);
  return r;
} // scWriteLibFile()

// JFL 16 Nov 19
static int scWriteLibCode(BaseLink *list,chrc *compileCmd,chrc *arCmd)
{
  int r;
  ObjConfig *oconfig;
  BaseLink *lnk;
  ObjMark *omark=0,*omark2;
  chr *bufsrc=0;
  chr smbuf[1024];
  FILE *fh=0;

  if(!(oconfig=scGetConfig(list)))
    bret(-1);

  if((r=scFormPath(oconfig,FORMPATH_MAINLANG,oconfig->pathTranslateFile))<0)
    goto BAIL;
  if((r=BaseReadFile(oconfig->formPath,0,&bufsrc,0))<0)
    goto BAIL;

  // find include & code sections for the lib

  for(lnk=list->n;lnk->t;lnk=lnk->n) {
    if(lnk->t!=OBJTYPE_Mark) continue;
    omark2=omark;
    omark=(ObjMark*)lnk;
    if(!omark2) continue;
    if(omark2->mType1 == BLOCKTYPE_INCLUDELIB) {
      PRINT_SYS_VRB("--includelib--\n");
      if((r=scCreateLibFile(oconfig,&fh,FORMPATH_LIBHDEF))<0)
        goto BAIL;
      if((r=scWriteLibFile(oconfig,fh,FORMPATH_LIBHDEF,
        list,bufsrc,omark2->fhOff,omark->fhOff))<0)
        goto BAIL;
      fclose(fh); fh=0;
    } else if(omark2->mType1 == BLOCKTYPE_FUNCTIONLIB) {
      PRINT_SYS_VRB("--functionlib--\n");
      if((r=scCreateLibFile(oconfig,&fh,FORMPATH_LIBLANG))<0)
        goto BAIL;

  //if((r=scFormPath(oconfig,FORMPATH_LIBLANG,oconfig->pathTranslateFile))<0)
  //  goto BAIL;
    
      // WRITE HEADER
      r=stzfmt(smbuf,sizeof(smbuf),"// %s\n",oconfig->formPath);
      WRITE(smbuf,r,fh,oconfig);
      r=stzfmt(smbuf,sizeof(smbuf),"// ");
      WRITE(smbuf,r,fh,oconfig);
      r=stzlen(compileCmd,0);
      WRITE(compileCmd,r,fh,oconfig);
      r=stzfmt(smbuf,sizeof(smbuf),"\n");
      WRITE(smbuf,r,fh,oconfig);
      r=stzfmt(smbuf,sizeof(smbuf),"// ");
      WRITE(smbuf,r,fh,oconfig);
      r=stzlen(arCmd,0);
      WRITE(arCmd,r,fh,oconfig);
      r=stzfmt(smbuf,sizeof(smbuf),"\n");
      WRITE(smbuf,r,fh,oconfig);

      if((r=scWriteLibFile(oconfig,fh,FORMPATH_LIBLANG,
        list,bufsrc,omark2->fhOff,omark->fhOff))<0)
        goto BAIL;
      fclose(fh); fh=0;
    }
  } // for

  r=0;
BAIL:
  if(fh)
    fclose(fh);
  if(bufsrc)
    MemFree(bufsrc);
  return r;
} // scWriteLibCode()

// JFL 16 Sep 19
static int scBuildCompileCmd(BaseLink *list,chr **outCmdp)
{
  int r,s,bs;
  ObjConfig *oconfig;
  chrc *compiler,*compilerx;
  chr *cmdalloc=0,*bb;
  chrc *s1,*s2,*libup,*libupx;
  BaseLink *lnk;
  ObjSZ *osz;
  ObjSZOne *sz,*szx;

  if(!(oconfig=scGetConfig(list)))
    bret(-1);

  //
  // COMPILE
  //

  // find path to directory one up from the library folder
  libup=oconfig->sclib;
  libupx=stzlastslash(oconfig->sclib,oconfig->sclibx);
  if(stzlen(libupx,oconfig->sclibx)<2) {
    s1=libupx-1;
    libupx=stzlastslash(libup,s1);
  }

  // compiler
  if(oconfig->flags&M_OBJCONFIG_ISCPP)
    compiler=oconfig->ccpath,compilerx=oconfig->ccpathx;
  else
    compiler=oconfig->cpath,compilerx=oconfig->cpathx;

  s = stzsize(compiler,compilerx);
  s += stzsize(oconfig->buf,0);
  if(oconfig->numMetaTable[METATOK_LIB]+oconfig->numMetaTable[METATOK_LIBINLIB]) {
    s += stzsize(libup,libupx);
    s += 256 * oconfig->numMetaTable[METATOK_LIB]; // todo: get lib name size
    s += 256 * oconfig->numMetaTable[METATOK_LIBINLIB]; // todo: get lib name size
  }
  s += 1024+64;
  if((r=MemAlloc(&cmdalloc,s))<0)
    goto BAIL;
  bb = cmdalloc,bs=s;

  //
  //
  //

  r=stzfmt(bb,bs,"%S",compiler,compilerx);
  bb+=r,bs-=r;

  // C/C++ STANDARD
  if(!oconfig->cFlagStd[0]) {
    if(oconfig->flags&M_OBJCONFIG_ISCPP)
      s1=oconfig->ccstd,s2=oconfig->ccstdx;
    else
      s1=oconfig->cstd,s2=oconfig->cstdx;
    stzcpy(oconfig->cFlagStd,sizeof(oconfig->cFlagStd),s1,s2);
  }
  if((oconfig->cFlagStd[0]=='C')||((oconfig->cFlagStd[0]=='c'))) {
    r=stzfmt(bb,bs," -std=%s",oconfig->cFlagStd);
    bb+=r,bs-=r;
  }

  //
  // CFLAGS
  //

  if(oconfig->numMetaTable[METATOK_CFLAGS]) {
    for(lnk=list->n;lnk->t;lnk=lnk->n) {
      if(lnk->t!=OBJTYPE_SZ) continue;
      osz=(ObjSZ*)lnk;
      if(osz->metaType!=METATOK_CFLAGS) continue;
      for(sz=osz->sz,szx=sz+osz->szNum;sz!=szx;sz++) {
        r=stzfmt(bb,bs," %S",sz->str,sz->strx);
        bb+=r,bs-=r;
      } // for
    } // for
  } // CFLAGS

  if(oconfig->debug) {
    r=stzfmt(bb,bs," -g");
    bb+=r,bs-=r;
  }

  if(oconfig->flags&M_OBJCONFIG_ISCPP) {
    if(oconfig->ccflags) {
      r=stzfmt(bb,bs," %S",oconfig->ccflags,oconfig->ccflagsx);
      bb+=r,bs-=r;
    }
  } else {
    if(oconfig->cflags) {
      r=stzfmt(bb,bs," %S",oconfig->cflags,oconfig->cflagsx);
      bb+=r,bs-=r;
    }
  }

  // if there are any libs, add include path
  if(oconfig->numMetaTable[METATOK_LIB]+oconfig->numMetaTable[METATOK_LIBINLIB]) {

    // add include path
    if((r=scFormPath(oconfig,FORMPATH_LIBDIR,oconfig->pathTranslateDir))<0)
      goto BAIL;

    r=stzfmt(bb,bs," -I%s",oconfig->formPath);
    bb+=r,bs-=r;

    // add lib path
    r=stzfmt(bb,bs," -L%s",oconfig->formPath);
    bb+=r,bs-=r;
  } // lib

  //
  //
  //

  if(bs) {*bb++=' ';bs--;}
  if((r=scFormPath(oconfig,FORMPATH_MAINLANG,oconfig->pathTranslateDir))<0)
    goto BAIL;
  r=stzcpy(bb,bs,oconfig->formPath,0);
  bb+=r,bs-=r;

  // link in libs
  if(oconfig->numMetaTable[METATOK_LIB]+oconfig->numMetaTable[METATOK_LIBINLIB]) { // lib

    if(1) {
      r=stzfmt(bb,bs," -Wl,--start-group");
      bb+=r,bs-=r;
    }

    for(lnk=list->n;lnk->t;lnk=lnk->n) {
      if(lnk->t!=OBJTYPE_SZ)
        continue;
      osz=(ObjSZ*)lnk;
      if((osz->metaType!=METATOK_LIB)&&(osz->metaType!=METATOK_LIBINLIB))
        continue;
      if(!osz->szNum)
        continue;
      sz=&osz->sz[0];

      r=stzfmt(bb,bs," -l%S",sz->str,sz->strx);
      bb+=r,bs-=r;

    } // for

    if(1) {
      r=stzfmt(bb,bs," -Wl,--end-group");
      bb+=r,bs-=r;
    }

  } // lib

  // linker flags
  if(oconfig->flags&M_OBJCONFIG_ISCPP) {
    if(oconfig->ldccflags) {
      r=stzfmt(bb,bs," %S",oconfig->ldccflags,oconfig->ldccflagsx);
      bb+=r,bs-=r;
    } else {
      r=stzfmt(bb,bs," -lstdc++");
      bb+=r,bs-=r;
    }
  } else {
    if(oconfig->ldcflags) {
      r=stzfmt(bb,bs," %S",oconfig->ldcflags,oconfig->ldcflagsx);
      bb+=r,bs-=r;
    }
  }

  //
  // LFLAGS
  //

  if(oconfig->numMetaTable[METATOK_LFLAGS]) {
    for(lnk=list->n;lnk->t;lnk=lnk->n) {
      if(lnk->t!=OBJTYPE_SZ) continue;
      osz=(ObjSZ*)lnk;
      if(osz->metaType!=METATOK_LFLAGS) continue;
      for(sz=osz->sz,szx=sz+osz->szNum;sz!=szx;sz++) {
        r=stzfmt(bb,bs," %S",sz->str,sz->strx);
        bb+=r,bs-=r;
      } // for
    } // for
  } // LFLAGS

  if(oconfig->betaPass) {
    if((r=scFormPath(oconfig,FORMPATH_BETAEXEFILE,oconfig->pathTranslateDir))<0)
      goto BAIL;
  } else {
    if((r=scFormPath(oconfig,FORMPATH_MAINEXEFILE,oconfig->pathTranslateDir))<0)
      goto BAIL;
  }

  r=stzfmt(bb,bs," -o %s",oconfig->formPath);
  bb+=r,bs-=r;

  if(1) {
    r=stzfmt(bb,bs," 2>&1");
    bb+=r,bs-=r;
  }

  if(outCmdp)
    *outCmdp=cmdalloc,cmdalloc=0;

BAIL:
  if(cmdalloc)
    MemFree(cmdalloc);
  return r;
} // scBuildCompileCmd()

// JFL 16 Sep 19
// JFL 27 Mar 22
static int scBuildPreCompileCmd(BaseLink *list,chrc *ccSrc,chrc *ccSrcx,chr **outCmdp)
{
  int r,s,bs;
  ObjConfig *oconfig;
  chrc *compiler,*compilerx;
  chr *cmdalloc=0,*bb;
  chrc *s1,*s2;
  BaseLink *lnk;
  ObjSZ *osz;
  ObjSZOne *sz,*szx;
  FILE *fh=0;
  chr smbuf[1024];

  if(!(oconfig=scGetConfig(list)))
    bret(-1);

  //
  // WRITE SOURCE TO TMP FILE
  //

  if((r=scFormPath(oconfig,FORMPATH_PRECCEXELANG,oconfig->pathTranslateFile))<0)
    goto BAIL;
  if(!(fh=fopen(oconfig->formPath,"wb")))
    bret(-10);
  oconfig->filesCreated|=M_FILESCREATED_PRECCLANG;
  oconfig->fhLineNum=1;

  // WRITE HEADER & source
  r=stzfmt(smbuf,sizeof(smbuf),"// %s\n",oconfig->name);
  WRITE(smbuf,r,fh,oconfig);
  r=stzlen(ccSrc,ccSrcx);
  WRITE(ccSrc,r,fh,oconfig);
  fclose(fh);
  fh=0;

  //
  // COMPILE
  //

  // compiler
  if(oconfig->flags&M_OBJCONFIG_ISCPP)
    compiler=oconfig->ccpath,compilerx=oconfig->ccpathx;
  else
    compiler=oconfig->cpath,compilerx=oconfig->cpathx;

  s = stzsize(compiler,compilerx);
  s += stzsize(oconfig->buf,0);
  s += 1024;
  if((r=MemAlloc(&cmdalloc,s))<0)
    goto BAIL;
  bb = cmdalloc,bs=s;

  //
  //
  //

  r=stzfmt(bb,bs,"%S",compiler,compilerx);
  bb+=r,bs-=r;

  // C/C++ STANDARD
  if(!oconfig->cFlagStd[0]) {
    if(oconfig->flags&M_OBJCONFIG_ISCPP)
      s1=oconfig->ccstd,s2=oconfig->ccstdx;
    else
      s1=oconfig->cstd,s2=oconfig->cstdx;
    stzcpy(oconfig->cFlagStd,sizeof(oconfig->cFlagStd),s1,s2);
  }
  if((oconfig->cFlagStd[0]=='C')||((oconfig->cFlagStd[0]=='c'))) {
    r=stzfmt(bb,bs," -std=%s",oconfig->cFlagStd);
    bb+=r,bs-=r;
  }

  //
  // CFLAGS
  //

  if(oconfig->numMetaTable[METATOK_CFLAGS]) {
    for(lnk=list->n;lnk->t;lnk=lnk->n) {
      if(lnk->t!=OBJTYPE_SZ) continue;
      osz=(ObjSZ*)lnk;
      if(osz->metaType!=METATOK_CFLAGS) continue;
      for(sz=osz->sz,szx=sz+osz->szNum;sz!=szx;sz++) {
        r=stzfmt(bb,bs," %S",sz->str,sz->strx);
        bb+=r,bs-=r;
      } // for
    } // for
  } // CFLAGS

  if(oconfig->debug) {
    r=stzfmt(bb,bs," -g");
    bb+=r,bs-=r;
  }

  if(oconfig->flags&M_OBJCONFIG_ISCPP) {
    if(oconfig->ccflags) {
      r=stzfmt(bb,bs," %S",oconfig->ccflags,oconfig->ccflagsx);
      bb+=r,bs-=r;
    }
  } else {
    if(oconfig->cflags) {
      r=stzfmt(bb,bs," %S",oconfig->cflags,oconfig->cflagsx);
      bb+=r,bs-=r;
    }
  }

  //
  //
  //

  if(bs) {*bb++=' ';bs--;}
  if((r=scFormPath(oconfig,FORMPATH_PRECCEXELANG,oconfig->pathTranslateDir))<0)
    goto BAIL;
  r=stzcpy(bb,bs,oconfig->formPath,0);
  bb+=r,bs-=r;

  //
  //
  //

  // linker flags
  if(oconfig->flags&M_OBJCONFIG_ISCPP) {
    if(oconfig->ldccflags) {
      r=stzfmt(bb,bs," %S",oconfig->ldccflags,oconfig->ldccflagsx);
      bb+=r,bs-=r;
    } else {
      r=stzfmt(bb,bs," -lstdc++");
      bb+=r,bs-=r;
    }
  } else {
    if(oconfig->ldcflags) {
      r=stzfmt(bb,bs," %S",oconfig->ldcflags,oconfig->ldcflagsx);
      bb+=r,bs-=r;
    }
  }

  //
  // LFLAGS
  //

  if(oconfig->numMetaTable[METATOK_LFLAGS]) {
    for(lnk=list->n;lnk->t;lnk=lnk->n) {
      if(lnk->t!=OBJTYPE_SZ) continue;
      osz=(ObjSZ*)lnk;
      if(osz->metaType!=METATOK_LFLAGS) continue;
      for(sz=osz->sz,szx=sz+osz->szNum;sz!=szx;sz++) {
        r=stzfmt(bb,bs," %S",sz->str,sz->strx);
        bb+=r,bs-=r;
      } // for
    } // for
  } // LFLAGS

  // exe file
  if((r=scFormPath(oconfig,FORMPATH_PRECCEXEFILE,oconfig->pathTranslateDir))<0)
    goto BAIL;
  r=stzfmt(bb,bs," -o %s",oconfig->formPath);
  bb+=r,bs-=r;

  if(1) { // stderr to stdout
    r=stzfmt(bb,bs," 2>&1");
    bb+=r,bs-=r;
  }

  if(outCmdp)
    *outCmdp=cmdalloc,cmdalloc=0;

BAIL:
  if(fh)
    fclose(fh);
  if(cmdalloc)
    MemFree(cmdalloc);
  return r;
} // scBuildPreCompileCmd()

// JFL 16 Sep 19
// JFL 24 Jun 21
static int scBuildLibCompileCmd(BaseLink *list,chr **outCmdp)
{
  int r,bs,cmdsize;
  ObjConfig *oconfig;
  chrc *compiler,*compilerx;
  chr *cmdalloc=0,*bb;

  if(!(oconfig=scGetConfig(list)))
    bret(-1);

  // compiler
  if(oconfig->flags&M_OBJCONFIG_ISCPP)
    compiler=oconfig->ccpath,compilerx=oconfig->ccpathx;
  else
    compiler=oconfig->cpath,compilerx=oconfig->cpathx;

  // language file
  if((r=scFormPath(oconfig,FORMPATH_LIBLANG,oconfig->pathTranslateDir))<0)
    goto BAIL;

  cmdsize = stzsize(compiler,compilerx);
  cmdsize += 2*stzsize(oconfig->formPath,0);
  cmdsize += 1024;
  if((r=MemAlloc(&cmdalloc,cmdsize))<0)
    goto BAIL;

  //
  // COMPILE
  //

  bb=cmdalloc,bs=cmdsize;

  r=stzfmt(bb,bs,"%S",compiler,compilerx);
  bb+=r,bs-=r;

  r=stzfmt(bb,bs," -c %s",oconfig->formPath);
  bb+=r,bs-=r;

  //
  // CFLAGS
  //

  if(oconfig->numMetaTable[METATOK_CFLAGS]) {
    BaseLink *lnk; ObjSZ *osz; ObjSZOne *sz,*szx;
    for(lnk=list->n;lnk->t;lnk=lnk->n) {
      if(lnk->t!=OBJTYPE_SZ) continue;
      osz=(ObjSZ*)lnk;
      if(osz->metaType!=METATOK_CFLAGS) continue;
      for(sz=osz->sz,szx=sz+osz->szNum;sz!=szx;sz++) {
        r=stzfmt(bb,bs," %S",sz->str,sz->strx);
        bb+=r,bs-=r;
      } // for
    } // for
  } // CFLAGS

  r=stzfmt(bb,bs," -I.");
  bb+=r,bs-=r;

  if((r=scFormPath(oconfig,FORMPATH_LIBDIR,oconfig->pathTranslateDir))>=0) {
    r=stzfmt(bb,bs," -I%s",oconfig->formPath);
    bb+=r,bs-=r;
  }

  // output
  if((r=scFormPath(oconfig,FORMPATH_LIBO,oconfig->pathTranslateDir))<0)
    goto BAIL;
  r=stzfmt(bb,bs," -o %s",oconfig->formPath);
  bb+=r,bs-=r;

  if(outCmdp)
    *outCmdp=cmdalloc,cmdalloc=0;

  r=0;
BAIL:
  if(cmdalloc)
    MemFree(cmdalloc);
  return r;
} // scBuildLibCompileCmd()

// JFL 16 Sep 19
// JFL 24 Jun 21
static int scBuildLibArCmd(BaseLink *list,chr **outCmdp)
{
  int r,bs,cmdsize;
  ObjConfig *oconfig;
  chr *cmdalloc=0,*bb;
  chrc *ar,*arx;

  if(!(oconfig=scGetConfig(list)))
    bret(-1);

  ar=oconfig->arpath,arx=oconfig->arpathx;

  cmdsize = stzsize(ar,arx);
  cmdsize += 2*stzsize(oconfig->formPath,0);
  cmdsize += 1024;
  if((r=MemAlloc(&cmdalloc,cmdsize))<0)
    goto BAIL;

  //
  // BUILD LIB
  //

  bb=cmdalloc,bs=cmdsize;

  r=stzfmt(bb,bs,"%S %S",ar,arx,oconfig->arflags,oconfig->arflagsx);
  bb+=r,bs-=r;

  if((r=scFormPath(oconfig,FORMPATH_LIBA,oconfig->pathTranslateDir))<0)
    goto BAIL;
  r=stzfmt(bb,bs," %s",oconfig->formPath);
  bb+=r,bs-=r;

  if((r=scFormPath(oconfig,FORMPATH_LIBO,oconfig->pathTranslateDir))<0)
    goto BAIL;
  r=stzfmt(bb,bs," %s",oconfig->formPath);
  bb+=r,bs-=r;

  if(outCmdp)
    *outCmdp=cmdalloc,cmdalloc=0;

  r=0;
BAIL:
  if(cmdalloc)
    MemFree(cmdalloc);
  return r;
} // scBuildLibArCmd()

// JFL 16 Jan 21
static int32 scMapLine(BaseLink *list,uns32 line) {
  int32 r=0;
  BaseLink *lnk;
  ObjBuf *obuf=0;
  ObjSZ *osz;
  ObjSZOne *sz,*szx,*szbest=0;

  for(lnk=list->n;lnk->t;lnk=lnk->n) {
    if(!obuf&&(lnk->t==OBJTYPE_Buf)&&((ObjBuf*)lnk)->name==1)
      obuf=(ObjBuf*)lnk;
    if(lnk->t!=OBJTYPE_SZ) continue;
    osz=(ObjSZ*)lnk;
    for(sz=osz->sz,szx=sz+osz->szNum;sz!=szx;sz++) {
      if(!szbest || ((sz->lineNum>szbest->lineNum)&&(sz->lineNum<=line)))
        szbest=sz;
    } // for
  } // for

  if(!obuf || !szbest)
    ret(-1);
  r = stzcountlines(obuf->buf,szbest->str);
  r += line - szbest->lineNum;

BAIL:
  return r;
} // scMapLine()

// JFL 15 Nov 20
// JFL 28 Nov 20; show actual file:line:col: first
// JFL 13 Jan 21
static int scReformatError(BaseLink *list,chr **outBufp,chrc *str) {
  ObjConfig *oconfig;
  int32 bs,allocsize;
  chr *bb,*alloc=0;
  int32 len=stzlen(str,0);
  chrc *strx=str+len,*eol,*s1,*s2;
  chrc *firstcolon,*secondcolon;
  chrc *sfile,*sfilex,*ofile,*ofilex;
  //chr sbuf[1024]; // TODO
  uns16 ofilelen;
  int line1,line2;
  int r;

  if(!(oconfig=scGetConfig(list)))
    bret(-1);

  if((r=scFormPath(oconfig,FORMPATH_MAINLANG,oconfig->pathTranslateDir))<0)
    goto BAIL;
  ofile=oconfig->formPath;
  ofilex=ofile+r;
  ofilelen=stzlen(ofile,ofilex);
  sfile=oconfig->orgpath;
  sfilex=oconfig->orgpathx;

  // SIZE
  bs=0,allocsize=0;
  for(s2=str;;s2=stzskipeol(eol,strx)) {
    s2=stzskipwhite(s2,strx);
    if(stzeos(s2,strx)) break;
    eol=stztilleol(s2,strx);
    if(stzchr(s2,eol,':')) {
      bs++;
      r=PTR_DIFF(eol,s2);
      if(allocsize<r) allocsize=r;
    }
  } // for

  // ALLOC
  if(!(allocsize*=bs)) bret(-1);
  allocsize+=len+256;
  if((r=MemAlloc(&alloc,allocsize))<0)
    goto BAIL;
  bb=alloc,bs=allocsize;

  // SCAN & COPY
  for(;;str=stzskipeol(eol,strx)) {
    str=stzskipwhite(str,strx);
    if(stzeos(str,strx)) break;
    eol=stztilleol(str,strx);
    if(stzstart(ofile,ofilex,str,eol)) {
copy_line:
      r=stzfmt(bb,bs,"%S\n",str,eol);
      bb+=r,bs-=r;
      continue;
    }
    s1=str; // start of line
    s1+=ofilelen;
    firstcolon=s1;
    if(*s1++!=':') goto copy_line;
    if(!C_IS_DIGIT(*s1)) goto copy_line;
    if(!(secondcolon=stzchr(s1,eol,':'))) goto copy_line;
    line1=0;
    s2=stztobin(s1,secondcolon,0,'i',&line1);
    if(!line1) goto copy_line;
    if((line2=scMapLine(list,line1))<=0)
      goto copy_line;

    // add the info, but in different format so it's not picked up by tools
    r=stzfmt(bb,bs,"(generated-file %S-%d)\n",str,firstcolon,line1);
    bb+=r,bs-=r;

    // replaced file name, remapped line, col, error to eol
    r=stzfmt(bb,bs,"%S:%d%S\n",sfile,sfilex,line2,secondcolon,eol);
    bb+=r,bs-=r;

  } // for

  if(outBufp) *outBufp=alloc,alloc=0;

  r=allocsize-bs;
BAIL:
  if(alloc) MemFree(alloc);
  return r;
} // scReformatError();

// JFL 16 Sep 19
static int scRunCompileCmd(BaseLink *list,chrc *cmd)
{
  int r;
  ObjConfig *oconfig;
  chr *resp=0;

  if(!(oconfig=scGetConfig(list)))
    bret(-1);

  oconfig->filesCreated|=M_FILESCREATED_MAINEXE;
  PRINT_SYS_VRB("C> %s\n",cmd);
  if((r=BaseRunScript(&resp,cmd,0,BASERUNSCRIPT_QUIET)))
    goto BAIL;

BAIL:
  if(r || (resp&&resp[0])) { // return from run
    if(r<0) PRINT_SYS_ERR("ERR* r=%d C> %s\n",r,cmd);
    else if(r) PRINT_SYS_ERR("MSG r=%d C> %s\n",r,cmd);
    if(resp) {
      chr *resp2=0;
      scReformatError(list,&resp2,resp);
      if(resp2) {
        PRINT_SYS_ERR("\n%s",resp2);
        MemFree(resp2);
      } else {
        PRINT_SYS_ERR("\n%s",resp);
      }
    }
  } // val from run
  if(resp) MemFree(resp);
  return r;
} // scRunCompileCmd()

// JFL 16 Nov 19
// JFL 31 Jul 20
// JFL 24 Jun 21
static int scCompileLib(BaseLink *list,chrc *compileCmd,chrc *arCmd)
{
  int r;//,cmdsize,bs;
  ObjConfig *oconfig;

  if(!(oconfig=scGetConfig(list)))
    bret(-1);

  //
  //
  //

  oconfig->filesCreated|=M_FILESCREATED_LIBO;
  //PRINT_SYS_VRB("C2> %s\n",compileCmd);
  PRINT_SYS_VRB("C> %s\n",compileCmd);
  if((r=BaseRunScript(0,compileCmd,0,BASERUNSCRIPT_STDOUT))) {
    PRINT_SYS_ERR("ERR* r=%d C> %s\n",r,compileCmd);
    goto BAIL;
  }

  //
  // BUILD LIB
  //

  oconfig->filesCreated|=M_FILESCREATED_LIBA;
  //PRINT_SYS_VRB("A2> %s\n",arCmd);
  PRINT_SYS_VRB("A> %s\n",arCmd);
  if((r=BaseRunScript(0,arCmd,0,BASERUNSCRIPT_STDOUT))) {
    PRINT_SYS_ERR("ERR* r=%d A> %s\n",r,arCmd);
    goto BAIL;
  }

  r=0;
BAIL:
  return r;
} // scCompileLib()

// JFL 16 Sep 19
static int scConfig(BaseLink *list,
  chrc *scpath,chrc *scpathx,
  int argc,chrc *argv[])
{
  int r,onesize;
  ObjConfig *oconfig;

  if(!scpath || !stzlen(scpath,scpathx))
    ret(-1);

  //
  // ALLOC
  //

  // alloc to work on file name
  onesize = stzsize(scpath,scpathx);
  onesize += stzlen(scWinRoot,0);
  onesize += 512;
  if((r=ObjConfigNew(&oconfig,onesize*3))<0)
    goto BAIL;
  BaseLinkBefore(list,BL(oconfig));
  oconfig->bufHalf = PTR_ADD(oconfig->buf,onesize);
  oconfig->pathBuf = PTR_ADD(oconfig->bufHalf,onesize);
  oconfig->pathBufSize = onesize;

  stzcpy(oconfig->buf,onesize,scpath,scpathx);
  stzcpy(oconfig->run,sizeof(oconfig->run),RUNPREFIX,0);
  oconfig->winRoot=(void*)scWinRoot;

  oconfig->argc=argc;
  oconfig->argv=argv;
  oconfig->orgpath=scpath;
  oconfig->orgpathx=scpathx;

  r = 0;
BAIL:
  return r;
} // scConfig()

// JFL 16 Sep 19
static int scCheckTimes(BaseLink *list)
{
  int r;
  chrc *s1,*bufx;
  uns64 seca64,secb64;
  ObjConfig *oconfig;

  if(!(oconfig=scGetConfig(list)))
    bret(-1);

  //
  // FILE NAMES
  //

  if(oconfig->dot) BRK(); // only do this once..
  bufx=oconfig->buf+stzlen(oconfig->buf,0);

  if(!(s1=stzlastslash(oconfig->buf,bufx)))
    s1=oconfig->buf;
  if(!(oconfig->dot=(chr*)stzrchr(s1,bufx,'.')))
    oconfig->dot=((chr*)oconfig->buf)+stzlen(oconfig->buf,0);
  stzcpy(oconfig->dashExtMain,sizeof(oconfig->dashExtMain),dashSC,0);
  stzcpy(oconfig->dashExtBeta,sizeof(oconfig->dashExtBeta),dashBeta,0);
  stzcpy(oconfig->dashExtPre,sizeof(oconfig->dashExtPre),dashPre,0);
  stzcpy(oconfig->dashExtLib,sizeof(oconfig->dashExtLib),dashLib,0);

  oconfig->dotToEndSize=PTR_DIFF(oconfig->buf+oconfig->bufSize,oconfig->dot);

  oconfig->name = oconfig->buf;
  bufx=oconfig->buf+stzlen(oconfig->buf,0);
  if((s1=stzrchr(oconfig->name,bufx,'/')) || (s1=stzrchr(oconfig->name,bufx,'\\')))
    oconfig->name=(chr*)(s1+1);

  // check extension for C or CPP
  if(stzisub("cc",0,oconfig->dot,bufx) || stzisub("cpp",0,oconfig->dot,bufx))
    oconfig->flags|=M_OBJCONFIG_ISCPP;

  //
  // CHECK IF SCRIPT IS NEWER
  //

  if((r=BaseFileDate(&seca64,oconfig->buf,0))<0)
    seca64=0;

  //stzfmt(oconfig->dot,sizeof(extExe)+1,"%s",extExe);
  stzfmt(oconfig->dot,oconfig->dotToEndSize,"%s.%s",oconfig->dashExtMain,extExe2);
  if((r=BaseFileDate(&secb64,oconfig->buf,0))<0)
    secb64=0;

  uns64 now=MainTimeNow();
  PRINT_SYS_VRB("checktime %s %"PRIu64" (now:%"PRIu64")\n",oconfig->buf,secb64,now);
  oconfig->fileSec64=now; // time of build for lib dep check

  r = (seca64<secb64) ? 1 : 0; // 1 = no need to build
BAIL:
  return r;
} // scCheckTimes()

#define ARGV_START 2

// JFL 19 Sep 19
static int scRun(BaseLink *list)
{
  int r,i,bs;
  ObjConfig *oconfig;
  chr *bb,*cmdline=0;
  chrc **argv,*s1;

  if(!(oconfig=scGetConfig(list)))
    bret(-1);
  argv=oconfig->argv; // get argv passed in

  // SIZE & ALLOC

  bs=stzsize(oconfig->buf,0);
  bs+=oconfig->dotToEndSize;

  for(i=ARGV_START;i<oconfig->argc;i++) {
    s1 = argv[i];
    bs+=stzsize(s1,0) + 2; // room for quotes
  } // for

  if((r=MemAlloz(&cmdline,bs))<0)
    goto BAIL;
  bb=cmdline;

  // COMMAND LINE
  if((r=scFormPath(oconfig,FORMPATH_MAINEXERUN,oconfig->pathTranslateRun))<0)
    goto BAIL;
  r=stzfmt(bb,bs,"%s",oconfig->formPath);
  bb+=r,bs-=r;

  // don't add script to path - it will run the same as a script and as exe
  for(i=ARGV_START;i<oconfig->argc;i++) {
    s1 = argv[i];
    r=stzfmt(bb,bs," \"%s\"",s1);
    bb+=r,bs-=r;
  } // for

  if(oconfig->flags&M_OBJCONFIG_LAUNCH) {
    PRINT_SYS_VRB("L> '%s'\n",cmdline);
    if((r=BaseLaunchScript(cmdline,0,0)))
      goto BAIL;
  } else {
    PRINT_SYS_VRB("R> '%s'\n",cmdline);
    if((r=BaseRunScript(0,cmdline,0,BASERUNSCRIPT_STDOUT))) {
      if(r<0) PRINT_SYS_ERR("ERR* r=%d R> %s\n",r,cmdline);
      goto BAIL;
    }
  }

  r=0;
BAIL:
  if(cmdline)
    MemFree(cmdline);
  return r;
} // scRun()

// JFL 22 Oct 19
static int scFindLineOfFileMark(BaseLink *list,ObjConfig *oconfig,chr **allocp,uns32 mark)
{
  int r;
  chrc *s1;
  chr *alloc=0;
  if(!allocp)
    bret(-1);
  if(!(alloc=*allocp)) {
    //chrc *ext = (oconfig->flags&M_OBJCONFIG_ISCPP)?extCPP2:extC2;
    //stzfmt(oconfig->dot,oconfig->dotToEndSize,"%s.%s",oconfig->dashExtMain,ext);
    if((r=scFormPath(oconfig,FORMPATH_MAINLANG,oconfig->pathTranslateFile))<0)
      goto BAIL;
    if((r=BaseReadFile(oconfig->formPath,0,&alloc,0))<0)
      goto BAIL;
    *allocp=alloc;
  }
  s1=alloc+mark; // should be OK if it runs past end of buf
  r=stzcountlines(alloc,s1);
BAIL:
  return r;
} // scFineLineOfFileMark()

// JFL 10 Nov 19
static int scBuildArgArr(void *outp,chrc *str,chrc *strx)
{ // build zero-terminated array of pointers to strings
  int r,num,spt,sst;
  chr **pt,**ptx;
  chr *ss,*ssx;
  void *alloc=0;
  chr c;

  if(!strx) strx=str+stzlen(str,0);
  sst=stzsize(str,strx);
  if(sst<2)
    ret(0);
  num=stzcountchrs(str,strx,";",0)+2;
  spt = num*sizeof(*pt);

  if((r=MemAlloc(&alloc, spt + sst + 1))<0)
    goto BAIL;
  pt = alloc;
  ptx = pt + num;
  ss = PTR_ADD(pt, spt);
  ssx = PTR_ADD(alloc,spt+sst);

  *pt++ = ss;
  for(;ss<ssx;) {
    if(!(c=*str++) || (str>=strx))
      break;
    if(c==';') c=0;

    if(!(*ss++=c)) {
      *pt++=ss;
    }

  } // for
  *ss=0;
  *pt=0;

  if(pt>=ptx)
    bret(-1);

  if(outp)
    *((void**)outp)=alloc,alloc=0;

  r=0;
BAIL:
  if(alloc)
    MemFree(alloc);
  return r;
} // scBuildArgArr()

// JFL 19 Sep 19
static int scDebug(BaseLink *list)
{
  int r,i,bs;
  ObjConfig *oconfig;
  chr *bb,*cmdline=0;
  chrc **argv,*s1;
  chr *filealloc=0;
  BaseLink *lnk;
  ObjSZ *osz;
  ObjSZOne *sz,*szx;

  if(!(oconfig=scGetConfig(list)))
    bret(-1);
  argv=oconfig->argv;

  bs=512;
  bs+=stzsize(oconfig->dbpath,oconfig->dbpathx);
  bs+=stzsize(oconfig->buf,0);
  bs+=oconfig->dotToEndSize;

  for(i=ARGV_START;i<oconfig->argc;i++) {
    s1 = argv[i];
    bs+=stzsize(s1,0) + 2; // room for quotes
  } // for

  bs += oconfig->numMetaTable[METATOK_GDB]*512; // todo: find size

  if((r=MemAlloz(&cmdline,bs))<0)
    goto BAIL;
  bb=cmdline;

  if(stzisub("-tui",0,cmdline,0))
    oconfig->flags|=M_OBJCONFIG_LAUNCH;

  if(oconfig->flags&M_OBJCONFIG_LAUNCH) {
    r=stzcpy(bb,bs,oconfig->run,0);
    bb+=r,bs-=r;
  }

  // path/to/gdb
  r=stzfmt(bb,bs,"%S",oconfig->dbpath,oconfig->dbpathx);
  bb+=r,bs-=r;

  if(oconfig->dbarg) {
    r=stzfmt(bb,bs," %S",oconfig->dbarg,oconfig->dbargx);
    bb+=r,bs-=r;
  }

  // execute break
  if(oconfig->numMetaTable[METATOK_GDB]) {
    for(lnk=list->n;lnk->t;lnk=lnk->n) {
      if(lnk->t!=OBJTYPE_SZ) continue;
      osz=(ObjSZ*)lnk;
      if(osz->metaType!=METATOK_GDB) continue;
      for(sz=osz->sz,szx=sz+osz->szNum;sz!=szx;sz++) {
        // if break is only word, break at the line
        s1=stztrimwhite(sz->str,sz->strx);
        if(!stzicmp("break",0,sz->str,s1)) {
          // break -- change to: b <linenumber>
          ObjSZOne const *sz2;
          if((r=scFindSZRef(&sz2,list,0,sz->refId|M_BLOCKID_REF))<0)
            continue;
          if((r=scFindLineOfFileMark(list,oconfig,&filealloc,sz2->fileOff))<0)
            continue;
          i=r;
          r=stzfmt(bb,bs," -ex \"b %d\"",i);
        } else {
          // add the command
          r=stzfmt(bb,bs," -ex \"%S\"",sz->str,sz->strx);
        }
        bb+=r,bs-=r;
      } // for
    } // for
  } else {
    r=stzfmt(bb,bs," -ex \"b main\"");
    bb+=r,bs-=r;
  }

  // run
  if((r=scFormPath(oconfig,FORMPATH_MAINEXEFILE,oconfig->pathTranslateRun))<0)
    goto BAIL;
  r=stzfmt(bb,bs," -ex r --args %s",oconfig->formPath);
  bb+=r,bs-=r;

  // args
  if(oconfig->argc>1) {
    for(i=ARGV_START;i<oconfig->argc;i++) {
      s1 = argv[i];
      r=stzfmt(bb,bs," \"%s\"",s1);
      bb+=r,bs-=r;
    } // for
  }

  if(oconfig->flags&M_OBJCONFIG_LAUNCH) {
    //chr* envarr[]={"TERM=xterm-256color",0}
    void *argarr=0;
    if((r=scBuildArgArr(&argarr,oconfig->dbenv,oconfig->dbenvx))<0)
      ; // do nothing
    PRINT_SYS_VRB("L> %s\n",cmdline);
    r=BaseLaunchScript(cmdline,0,argarr);
    if(argarr)
      MemFree(argarr);
    if(r<0)
      goto BAIL;
  } else {
    PRINT_SYS_VRB("R> %s\n",cmdline);
    if((r=BaseRunScript(0,cmdline,0,BASERUNSCRIPT_STDERR))) {
      if(r<0) PRINT_SYS_ERR("ERR* r=%d R> %s\n",r,cmdline);
      goto BAIL;
    }
  }

  r=0;
BAIL:
  if(filealloc)
    MemFree(filealloc);
  if(cmdline)
    MemFree(cmdline);
  return r;
} // scDebug()

////////////////////////////////////////////////////////////////////////////////
// RC FILE

#define RCTOKS_X \
  X(NONE) \
  X(ARFLAGS) \
  X(ARPATH) \
  X(CCFLAGS) \
  X(CCPATH) \
  X(CCSTD) \
  X(CFLAGS) \
  X(CPATH) \
  X(CSTD) \
  X(DBARG) \
  X(DBENV) \
  X(DBPATH) \
  X(ELSE) \
  X(ELSEIF) \
  X(ENDIF) \
  X(IF) \
  X(IFNOT) \
  X(LDCCFLAGS) \
  X(LDCFLAGS) \
  X(PRINT) \
  X(SCDOC) \
  X(SCLIB) \
  X(SET) \
  X(ZMAX)

#define X(a) RCTOK_##a,
typedef enum { RCTOKS_X RCTOK_NUM } rctok_enum;
#undef X

#define X(a) #a,
static chrc *rcTokTable[] = { RCTOKS_X 0 };
#undef X

#define CONFIGVERS 0

#define RCDEFS_X \
  X(NONE) \
  X(BUILD_MINGW) \
  X(BUILD_MINGW32) /* defunct - use MINGW */ \
  X(BUILD_CYGWIN) \
  X(BUILD_LINUX) \
  X(BUILD_WINRUN) \
  X(DEBUG) \
  X(VERBOSE) \
  X(CONFIGVERS) \
  /* end-of-list */

#ifndef NONE
#define NONE 0
#endif // NONE

#ifndef BUILD_LINUX
#define BUILD_LINUX 0
#endif // BUILD_LINUX

#ifndef BUILD_MINGW32
#define BUILD_MINGW32 0
#endif // BUILD_MINGW32

#ifndef BUILD_MINGW
#define BUILD_MINGW 0
#endif // BUILD_MINGW

#ifndef BUILD_WINRUN // running in a windows shell
#define BUILD_WINRUN 0
#endif // BUILD_WINRUN

#ifndef BUILD_CYGWIN
#define BUILD_CYGWIN 0
#endif // BUILD_CYGWIN

#ifndef DEBUG
#define DEBUG 0
#endif // DEBUG

#ifndef VERBOSE
#define VERBOSE 0
#endif // VERBOSE

#define RCDEFSYMS_X \
  XX(CPP,"c++") \
  XX(C,"c")

#define XX(a,b) RCDEF_##a,
#define X(a) RCDEF_##a,
enum { RCDEFS_X RCDEFSYMS_X };
#undef X
#undef XX

#define XX(a,b) b,
#define X(a) #a,
static chrc* rcDefTokTable[] = { RCDEFS_X RCDEFSYMS_X 0 };
#undef X
#undef XX

#define XX(a,b) 0,
#define X(a) a,
static int rcDefValTable[] = { RCDEFS_X RCDEFSYMS_X };
#undef X
#undef XX

int rcDefTokRemap(int tok) {
  if(tok==BUILD_MINGW32) tok=BUILD_MINGW;
  return tok;
} // rcDefTokRemap()

#define RC_WARNIFCONFIG_LT 6

// JFL 11 Oct 19
// JFL 19 Nov 19
// JFL 16 Jan 21; elseif
static int scScanRC(BaseLink *list,chr *buf,int bufSize,chrc *path,chrc *pathx)
{
  int r,tok,skip=0,notskipped=0,pathflags=0;
  chrc *s1,*s2,*s3,*sx,*eol,*rcbuf,*rcbufx;
  ObjBuf *obuf;
  ObjConfig *oconfig;
  chr *pathnorm[RCTOK_NUM*2];

  MEMZ(pathnorm);

  if(!(oconfig=scGetConfig(list)))
    bret(-1);

  if((r=ObjBufNew(&obuf,0))<0)
    goto BAIL;
  BaseLinkBefore(list,BL(obuf));
  rcbuf=buf,rcbufx=buf+bufSize;
  obuf->buf=buf; // pass ownership
  obuf->flagFree=1; // request to be freed

  if(oconfig->debug) // override if set on command line
    rcDefValTable[RCDEF_DEBUG]=1;

  // remove Windows doubleslash, convert to unix slashes
  pathflags=M_SCPATHTRANSLATE_DOUBLESLASH|M_SCPATHTRANSLATE_UNIXSLASH;

  #if BUILD_MINGW
  oconfig->pathTranslateRun=M_SCPATHTRANSLATE_UNIXSLASH|M_SCPATHTRANSLATE_DOUBLESLASH;
  oconfig->pathTranslateDir=M_SCPATHTRANSLATE_UNIXSLASH|M_SCPATHTRANSLATE_DOUBLESLASH;
  oconfig->pathTranslateFile=M_SCPATHTRANSLATE_ROOT|M_SCPATHTRANSLATE_WINSLASH|M_SCPATHTRANSLATE_DOUBLESLASH;
  #elif BUILD_CYGWIN
  oconfig->pathTranslateRun=M_SCPATHTRANSLATE_UNIXSLASH|M_SCPATHTRANSLATE_DOUBLESLASH;
  oconfig->pathTranslateDir=M_SCPATHTRANSLATE_UNIXSLASH|M_SCPATHTRANSLATE_DOUBLESLASH;
  oconfig->pathTranslateFile=M_SCPATHTRANSLATE_UNIXSLASH|M_SCPATHTRANSLATE_DOUBLESLASH;
  #elif !BUILD_LINUX
  oconfig->pathTranslateRun=M_SCPATHTRANSLATE_WINSLASH|M_SCPATHTRANSLATE_DOUBLESLASH;
  oconfig->pathTranslateDir=M_SCPATHTRANSLATE_UNIXSLASH|M_SCPATHTRANSLATE_DOUBLESLASH;
  oconfig->pathTranslateFile=M_SCPATHTRANSLATE_UNIXSLASH|M_SCPATHTRANSLATE_DOUBLESLASH;
  #else // BUILD_LINUX
  oconfig->pathTranslateRun=M_SCPATHTRANSLATE_UNIXSLASH|M_SCPATHTRANSLATE_DOUBLESLASH;
  oconfig->pathTranslateDir=M_SCPATHTRANSLATE_UNIXSLASH|M_SCPATHTRANSLATE_DOUBLESLASH;
  oconfig->pathTranslateFile=M_SCPATHTRANSLATE_UNIXSLASH|M_SCPATHTRANSLATE_DOUBLESLASH;
  #endif // BUILD_LINUX

  #if BUILD_WINRUN
  // override for windows systems
  oconfig->pathTranslateRun=M_SCPATHTRANSLATE_WINSLASH|M_SCPATHTRANSLATE_DOUBLESLASH;
  #endif // BUILD_WINRUN

  //
  //
  //

  for(eol=rcbuf;;) {
    s1=stzskipwhite(eol,rcbufx);
    if(stzeos(s1,rcbufx))
      break;
    eol=stztilleol(s1,rcbufx);

    sx=stzsub("//",0,s1,eol);
    if(!sx)
      sx=eol;

    if(s1>=sx)
      continue;

    s2=stztillwhite(s1,sx);
    tok=stzimap(rcTokTable,s1,s2);
    s3=stzskipwhite(s2,sx);

    if(skip && (tok!=RCTOK_ENDIF) && (tok!=RCTOK_ELSE) && (tok!=RCTOK_ELSEIF))
      continue;

    switch(tok) {
    default:
      PRINT_SYS_ERR("rc syntax: %S, line:%d, '%S'\n",
        path,pathx,stzcountlines(buf,s2),s1,s2);
      break;
    case RCTOK_SET:
      s2=stztillwhite(s3,sx);
      r=rcDefTokRemap(stzimap(rcDefTokTable,s3,s2));
      if((r<0)||(r>NUM(rcDefValTable))) {
        sx=s2;
        goto unknownval;
      }
      // override if set from command line
      if((r==RCDEF_DEBUG) && oconfig->debug)
        break;
      s2=stzskipwhite(s2,sx);
      stztobin(s2,sx,10,'i',&rcDefValTable[r]);
      break;
    case RCTOK_IF:
    case RCTOK_ELSEIF:
    case RCTOK_IFNOT:
      sx=stztrimwhite(s3,sx);
      r=rcDefTokRemap(stzimap(rcDefTokTable,s3,sx));
      if((r<0)||(r>NUM(rcDefValTable))) {
      unknownval:
        PRINT_SYS_ERR("rc unknown-token: %S, line:%d, '%S'\n",
          path,pathx,stzcountlines(buf,s2),s3,sx);
        break;
      }
      switch(tok) {
      default: goto unknownval;
      case RCTOK_IF:
        skip = rcDefValTable[r] ? 0 : 1;
        notskipped = 0; // reset
        break;
      case RCTOK_IFNOT:
        skip = rcDefValTable[r] ? 1 : 0;
        notskipped = 0; // reset
        break;
      case RCTOK_ELSEIF:
        if(notskipped) skip = 1;  // already had a block
        else skip = rcDefValTable[r] ? 0 : 1;
        break;
      } // switch
      notskipped |= !skip;
      break;
    case RCTOK_ELSE:
      if(notskipped) skip=1;
      else skip=0;
      break;
    case RCTOK_ENDIF:
      skip=0;
      break;
    case RCTOK_CPATH:
      oconfig->cpath=s3,oconfig->cpathx=stztrimwhite(s3,sx);
#define PATHNORM(tt,pp) pathnorm[tt*2]=(void*)pp,pathnorm[1+tt*2]=(void*)pp##x
      PATHNORM(RCTOK_CPATH,oconfig->cpath);
      break;
    case RCTOK_CCPATH:
      oconfig->ccpath=s3,oconfig->ccpathx=stztrimwhite(s3,sx);
      PATHNORM(RCTOK_CCPATH,oconfig->ccpath);
      break;
    case RCTOK_ARPATH:
      oconfig->arpath=s3,oconfig->arpathx=stztrimwhite(s3,sx);
      PATHNORM(RCTOK_ARPATH,oconfig->arpath);
      break;
    case RCTOK_ARFLAGS:
      oconfig->arflags=s3,oconfig->arflagsx=stztrimwhite(s3,sx);
      break;
    case RCTOK_DBPATH:
      oconfig->dbpath=s3,oconfig->dbpathx=stztrimwhite(s3,sx);
      PATHNORM(RCTOK_DBPATH,oconfig->dbpath);
      break;
    case RCTOK_SCLIB:
      oconfig->sclib=s3,oconfig->sclibx=stztrimwhite(s3,sx);
      s2=stzlastslash(s3,sx),oconfig->sclibrelx=stztrimwhite(s3,sx);
      oconfig->sclibrel=stzlastslash(s3,s2);
      if(oconfig->sclibrel && ((*(oconfig->sclibrel)=='/')
        || (*(oconfig->sclibrel)=='\\')))
        oconfig->sclibrel++;
      PATHNORM(RCTOK_SCLIB,oconfig->sclib);
      break;
    case RCTOK_CFLAGS:
      oconfig->cflags=s3,oconfig->cflagsx=stztrimwhite(s3,sx);
      break;
    case RCTOK_CCFLAGS:
      oconfig->ccflags=s3,oconfig->ccflagsx=stztrimwhite(s3,sx);
      break;
    case RCTOK_LDCFLAGS:
      oconfig->ldcflags=s3,oconfig->ldcflagsx=stztrimwhite(s3,sx);
      break;
    case RCTOK_LDCCFLAGS:
      oconfig->ldccflags=s3,oconfig->ldccflagsx=stztrimwhite(s3,sx);
      break;
    case RCTOK_CSTD:
      oconfig->cstd=s3,oconfig->cstdx=stztrimwhite(s3,sx);
      break;
    case RCTOK_CCSTD:
      oconfig->ccstd=s3,oconfig->ccstdx=stztrimwhite(s3,sx);
      break;
    case RCTOK_SCDOC:
      oconfig->scdoc=s3,oconfig->scdocx=stztrimwhite(s3,sx);
      break;
    case RCTOK_DBARG:
      oconfig->dbarg=s3,oconfig->dbargx=stztrimwhite(s3,sx);
      break;
    case RCTOK_DBENV:
      oconfig->dbenv=s3,oconfig->dbenvx=stztrimwhite(s3,sx);
      break;
    case RCTOK_PRINT:
      PRINT_SYS_MSG("%S\n",s3,sx);
      break;
    } // switch
  } // for

  oconfig->debug=rcDefValTable[RCDEF_DEBUG];
  oconfig->verbose=rcDefValTable[RCDEF_VERBOSE];

  if(rcDefValTable[RCDEF_CONFIGVERS]<RC_WARNIFCONFIG_LT) {
    PRINT_SYS_ERR("update config file %S, from %d to %d -- remove old & make install\n",
      path,pathx,rcDefValTable[RCDEF_CONFIGVERS],RC_WARNIFCONFIG_LT);
  }

  // normalize paths
  if(pathflags) {
    chr **pn,**pnx;
    pn=pathnorm,pnx=pn+NUM(pathnorm);
    for(;pn<pnx;pn+=2) {
      if(!*pn) continue;
      scPathTranslate(pn[0],pn[1]-pn[0],0,0,pn[0],pn[1],pathflags);
    }
  } // pathflags

  r=0;
BAIL:
  return r;
} // scScanRC()

// JFL 20 Oct 19
static int scMetaSet(BaseLink *list,chrc *orgbuf,chrc *str,chrc *strx)
{
  int r,tok;
  chrc *s1;
  ObjConfig *oconfig;

  if(!(oconfig=scGetConfig(list)))
    bret(-1);
  s1=stzskipwhite(str,strx);
  str=stztillwhite(s1,strx);
  tok=rcDefTokRemap(stzimap(rcDefTokTable,s1,str));
  switch(tok) {
  default:
    // look for matching C standards
    if((*s1=='c')||(*s1=='C')) {
      if((s1[1]=='+')&&(s1[2]=='+')) {
        stzcpy(oconfig->cFlagStd,sizeof(oconfig->cFlagStd),s1,str);
        oconfig->flags|=M_OBJCONFIG_ISCPP;
        break;
      } else if(C_IS_DIGIT(s1[1])&&(C_IS_DIGIT(s1[2]))) {
        stzcpy(oconfig->cFlagStd,sizeof(oconfig->cFlagStd),s1,str);
        oconfig->flags&=~M_OBJCONFIG_ISCPP;
        break;
      }
    } // look for C standad
    PRINT_SYS_ERR("unknown set-token '%S' (%d)\n",
      s1,str,stzcountlines(orgbuf,s1));
    ret(-1);
  case RCDEF_CPP:
    oconfig->flags|=M_OBJCONFIG_ISCPP;
    break;
  case RCDEF_C:
    oconfig->flags&=~M_OBJCONFIG_ISCPP;
    break;
  } // switch

  r=0;
BAIL:
  return r;
} // scMetaSet()

// JFL 11 Oct 19
static int scLoadRC(BaseLink *list,chrc *scpath,int flagshowconfig)
{
  int r,s;
  chr buf[FILEPATH_BUF_SIZE];
  chr *loadbuf=0;
  chrc *s1,*sx;
  int pathflags=M_SCPATHTRANSLATE_DOUBLESLASH|M_SCPATHTRANSLATE_UNIXSLASH;
  int printsave;

  printsave = BaseG.printLevel[PRINT_RAW];
  if(flagshowconfig) // || BaseG.printLevel[PRINT_VRB])
    BaseG.printLevel[PRINT_RAW]=0;

 //     BaseG.printLevel[k]|=M_PRINT_MSG|M_PRINT_VRB|M_PRINT_ERR;

  // try home
  if((r=BaseHomeDir(buf,sizeof(buf)))>0) {
    stzfmt(buf+r,sizeof(buf)-r,"/%s",rcfile);
    scPathTranslate(buf,sizeof(buf),0,0,buf,0,pathflags);
    if((s=BaseReadFile(buf,0,&loadbuf,M_BASEREADFILE_QUIET))>0) {
      PRINT_RAW_MSG("config file '%s'\n",buf);
      scScanRC(list,loadbuf,s,buf,0);
      loadbuf=0;
    } else {
      PRINT_RAW_MSG("config file '%s' - not found\n",buf);
    }
  }

  // try script dir
  sx=scpath+stzlen(scpath,0);
  if((s1=stzrchr(scpath,sx,'/'))||(s1=stzrchr(scpath,sx,'\\'))) {
    stzfmt(buf,sizeof(buf),"%S/%s",scpath,s1,rcfile);
    scPathTranslate(buf,sizeof(buf),0,0,buf,0,pathflags);
    if((s=BaseReadFile(buf,0,&loadbuf,M_BASEREADFILE_QUIET))>0) {
      PRINT_RAW_MSG("config file '%s' - cascade\n",buf);
      scScanRC(list,loadbuf,s,buf,0);
      loadbuf=0;
    }
  }

  #ifdef DEV_RCFILE
  r=stzfmt(buf,sizeof(buf),DEV_RCFILE);
  scPathTranslate(buf,sizeof(buf),0,0,buf,0,pathflags);
  if((s=BaseReadFile(buf,0,&loadbuf,M_BASEREADFILE_QUIET))>0) {
    PRINT_RAW_MSG("config file '%s' - dev config\n",buf);
    scScanRC(list,loadbuf,s,buf,0);
    loadbuf=0;
  } else {
    PRINT_RAW_MSG("config file '%s' - dev config not found\n",buf);
  }
  #endif // ifdef DEV_RCFILE

  r=0;
//BAIL:
  BaseG.printLevel[PRINT_RAW]=printsave;
  if(loadbuf)
    MemFree(loadbuf);
  return r;
} // scLoadRC()

// JFL 21 Oct 19
static void scCleanTempFiles(BaseLink *list)
{
  ObjConfig *oconfig;

  if(!(oconfig=scGetConfig(list)))
    goto BAIL;

  // remove main build file
  if(oconfig->filesCreated&M_FILESCREATED_MAINLANG) {
    if(scFormPath(oconfig,FORMPATH_MAINLANG,oconfig->pathTranslateFile)>=0)
      unlink(oconfig->formPath);
  }

  // remove beta build & run files
  if(oconfig->filesCreated&M_FILESCREATED_BETALANG) {
    if(scFormPath(oconfig,FORMPATH_BETALANG,oconfig->pathTranslateFile)>=0)
      unlink(oconfig->formPath);
  }
  if(oconfig->filesCreated&M_FILESCREATED_BETAEXE) {
    if(scFormPath(oconfig,FORMPATH_BETAEXEFILE,oconfig->pathTranslateFile)>=0)
      unlink(oconfig->formPath);
  }

  if(oconfig->filesCreated&M_FILESCREATED_LIBLANG) {
    if(scFormPath(oconfig,FORMPATH_LIBLANG,oconfig->pathTranslateFile)>=0)
      unlink(oconfig->formPath);
  }
  if(oconfig->filesCreated&M_FILESCREATED_LIBO) {
    if(scFormPath(oconfig,FORMPATH_LIBO,oconfig->pathTranslateFile)>=0)
      unlink(oconfig->formPath);
  }
  if(oconfig->filesCreated&M_FILESCREATED_PRECCLANG) {
    if(scFormPath(oconfig,FORMPATH_PRECCEXELANG,oconfig->pathTranslateFile)>=0)
      unlink(oconfig->formPath);
  }
  if(oconfig->filesCreated&M_FILESCREATED_PRECCEXE) {
    if(scFormPath(oconfig,FORMPATH_PRECCEXEFILE,oconfig->pathTranslateDir)>=0)
      unlink(oconfig->formPath);
  }
BAIL:
  return;
} // scCleanTempFiles()

////////////////////////////////////////////////////////////////////////////////

#define INSTANTCOPTS_X \
  X(NONE,0) \
  X(BUILD,"build, don't run") \
  X(DEBUG,"debug with gdb") \
  X(HELP,"show help") \
  X(LAUNCH,"launch") \
  X(REBUILD,"rebuild and run") \
  X(VERBOSE,"verbose") \
  X(SHOWCONFIG,"show config") \
  X(META,"meta token") \
  X(NOLIBDEP,"don't add library dependency check") \
  X(ZMAX,0)

#define X(a,b) INSTANTCOPT_##a,
enum { INSTANTCOPTS_X };
#undef X

#define X(a,b) #a,
static chrc *instantCOptTable[] = { INSTANTCOPTS_X 0 };
#undef X

#define X(a,b) b,
static chrc *instantCOptHelp[] = { INSTANTCOPTS_X 0 };
#undef X

////////////////////////////////////////////////////////////////////////////////

// JFL 31 Oct 19
static int mainDoc(ObjConfig *oconfig,int argc,chrc **argv)
{
  int r,i,bs;
  chr *rundoc=0;
  chr *bb;
  chrc *s1;

  // size of all args
  bs=stzlen(oconfig->scdoc,oconfig->scdocx);
  if(bs<2) {
    PRINT_SYS_ERR("scdoc not configured in rc file\n");
    ret(1);
  }
  for(i=0;i<argc;i++) {
    s1=argv[i];
    bs+=stzlen(s1,0)+16;
  } // for

  if((r=MemAlloc(&rundoc,bs))<0)
    goto BAIL;
  bb=rundoc;

  r=stzfmt(bb,bs,"%S",oconfig->scdoc,oconfig->scdocx);
  bb+=r,bs-=r;

  for(i=0;i<argc;i++) {
    s1=argv[i];
    r=stzfmt(bb,bs," %s",s1);
    bb+=r,bs-=r;
  } // for

  if((r=BaseRunScript(0,rundoc,0,BASERUNSCRIPT_STDERR))<0)
    goto BAIL;

  r=0;
BAIL:
  if(rundoc)
    MemFree(rundoc);
  return r;
} // mainDoc()

#define SHOWCONFIGS_X \
  X(RCTOK_CPATH,cpath,cpathx,"C compiler") \
  X(RCTOK_CSTD,cstd,cstdx,"C standard") \
  X(RCTOK_CFLAGS,cflags,cflagsx,"C compile flags") \
  X(RCTOK_CCPATH,ccpath,ccpathx,"C++ compiler") \
  X(RCTOK_CCSTD,ccstd,ccstdx,"C++ standard") \
  X(RCTOK_CCFLAGS,ccflags,ccflagsx,"C++ compile flags") \
  X(RCTOK_ARPATH,arpath,arpathx,"ar tool") \
  X(RCTOK_ARFLAGS,arflags,arflagsx,"ar flags") \
  X(RCTOK_DBPATH,dbpath,dbpathx,"debugger") \
  X(RCTOK_DBARG,dbarg,dbargx,"debugger args") \
  X(RCTOK_DBENV,dbenv,dbenvx,"debugger env") \
  X(RCTOK_SCLIB,sclib,sclibx,"library directory") \
  /* end of list */

typedef struct {
  int tok;
  chrc *name;
  chrc *msg;
  int off0;
  int off1;
} showConfigRec;

#define OFF(s,f) ((int)((intptr_t)(&((s*)0)->f)))
#define X(tok,off0,off1,msg) {tok,#off0,msg,OFF(ObjConfig,off0),OFF(ObjConfig,off1)},
showConfigRec showConfigTable[] = {SHOWCONFIGS_X};
#undef X

// JFL 04 Aug 20
static void mainShowConfig(ObjConfig *oconfig) {
  showConfigRec *scr,*scrx;
  chrc *s1,*s2;
  int printsave;

  printsave = BaseG.printLevel[PRINT_RAW];
  BaseG.printLevel[PRINT_RAW]=0;

  scr=showConfigTable,scrx=scr+NUM(showConfigTable);
  for(;scr<scrx;scr++) {
    s1=s2=0;
    if(!scr->off0) continue;
    s1=*((chrc**)PTR_ADD(oconfig,scr->off0));
    if(scr->off1)
      s2=*((chrc**)PTR_ADD(oconfig,scr->off1));
    PRINT_RAW_MSG("%s='%S' -- %s\n",scr->name,s1,s2,scr->msg);
  } // for

  BaseG.printLevel[PRINT_RAW]=printsave;
} // mainShowConfig()

// JFL 20 Oct 19
static void mainHelp(chrc *exefile)
{
  int i;
  chr name[32];

  printf("%s [options]\n",exefile);
  printf("------------------\n");

  printf("Options that start with a + are handled by InstantC:\n");
  for(i=0;i<NUM(instantCOptHelp);i++) {
    if(!instantCOptHelp[i]) continue;
    stzcpy(name,sizeof(name),instantCOptTable[i],0);
    stztolower(name,0);
    printf(" +%s -- %s\n",name,instantCOptHelp[i]);
  } // for

  printf("--------------------------------\n");
  printf("A C/C++ scripting compiler tool. Version: "MAINVERS_SZ"\n");
  printf("See 'man instantc' for documentation.\n");
  printf("Contact: instantcompiler@gmail.com\n");
  printf("%s\n",MainCoprShort);
} // mainHelp()

////////////////////////////////////////////////////////////////////////////////

// JFL 19 Sep 19
// JFL 04 Jan 25
int main(int argc, char **argv)
{
  int r,i,k,argc2=0,rc;
  chrc *scpath=0,*exefile,*s1;
  BaseLink head;
  uns8 flagdontrun=0,flagbuild=0,flagdebug=0,flagvrb=0,flagdepcheck=1;
  uns8 flagclean=0,flaghelp=0,flaglaunch=0,flagshowconfig=0,flagcc=0;
  chrc *argv2[argc];
  ObjConfig *oconfig;

  BaseLinkMakeHead(&head);

  //
  // ARGUMENTS
  //

  if((exefile=stzlastslash(argv[0],0))) exefile++;
  else exefile=argv[0];

  // check for instantc or instantcc
  r=stzlen(exefile,0);
  if((r>4)  // look for last two chars cc
    && ((exefile[r-2]=='c')||(exefile[r-2]=='C')) 
    && ((exefile[r-1]=='c')||(exefile[r-1]=='C'))) { 
      flagcc=1;
  }

  for(i=0;i<argc;i++) {
    s1=argv[i];
    if(*s1=='+') { // options for us
      s1++;
      k=stzimap(instantCOptTable,s1,0);
      switch(k) {
      default:
        PRINT_SYS_ERR("Option '%s' not recognized\n",s1);
        // drop through
      case INSTANTCOPT_HELP:
        flaghelp=1;
        break;
      case INSTANTCOPT_BUILD:
        flagbuild=1,flagdontrun=1;
        break;
      case INSTANTCOPT_REBUILD:
        flagbuild=1,flagclean=1;
        break;
      case INSTANTCOPT_DEBUG:
        flagdebug=1;
        break;
      case INSTANTCOPT_VERBOSE:
        flagvrb=1;
        break;
      case INSTANTCOPT_LAUNCH:
        flaglaunch=1;
        break;
      case INSTANTCOPT_SHOWCONFIG:
        flagshowconfig=1;
        break;
      case INSTANTCOPT_NOLIBDEP:
        flagdepcheck=0;
        break;
      case INSTANTCOPT_META:
        if(i>=argc) {PRINT_SYS_ERR("missing arg for %s\n",s1);break;}
        s1=argv[i++];
        PRINT_SYS_MSG("arg %s\n",s1);
        break;
      } // switch
      continue;
    } // options for us

    argv2[argc2++]=s1;

    // script path is first non-option param after script path
    if(!scpath && i && *s1!='-')
      scpath=s1;
  } // for

  //
  //
  //

  if(!scpath) {
    mainHelp(exefile);
    ret(1);
  } else if(flaghelp) {
    if(argc<2) {
      mainHelp(exefile);
      ret(1);
    }
  } // help message

  // set print level bits (set to not-print)
  for(k=0;k<NUM(BaseG.printLevel);k++) {
    if(!flagvrb)
      BaseG.printLevel[k]|=M_PRINT_MSG|M_PRINT_VRB|M_PRINT_ERR;
  } // for

  PRINT_SYS_MSG("%s v"MAINVERS_SZ", %s\n",exefile,MainCoprShort);

  if((r=scConfig(&head,scpath,0,argc2,(chrc**)argv2))<0)
    goto BAIL;
  if(!(oconfig=scGetConfig(&head)))
    bret(-1);
  if(flagcc) oconfig->flags|=M_OBJCONFIG_ISCPP;
  if((r=scLoadRC(&head,scpath,flagshowconfig))<0)
    goto BAIL;

  if(flaghelp) {
    mainDoc(oconfig,argc2,argv2);
    ret(0);
  }

  if(flagshowconfig) {
    mainShowConfig(oconfig);
  }

  if(flagdebug) { // setup for rc file override
    oconfig->flags|=M_OBJCONFIG_DEBUGGER;
    oconfig->debug=1;
    flagbuild=1;
  }
  if(flaglaunch)
    oconfig->flags|=M_OBJCONFIG_LAUNCH;
  if(flagdepcheck)
    oconfig->flags|=M_OBJCONFIG_DEPCHECK;

  // if no build/debug options set, clean temporary files
  if(!flagdebug && !flagbuild && !flagvrb)
    flagclean=1;

  if((r=scCheckTimes(&head))<0)
    goto BAIL;
  if(!r)
    flagbuild=1;

  if(flagvrb)
    oconfig->flags|=M_OBJCONFIG_VERBOSE;

rebuild_loop:
  if(flagbuild) { // build

    PRINT_SYS_MSG("building..\n");

    if(!oconfig->ccpath || !oconfig->cpath) {
      PRINT_SYS_ERR("CPATH or CCPATH not set - %s probably not found\n",rcfile);
      bret(-55);
    }

    if((r=scAddMarks(&head))<0)
      goto BAIL;

    if((r=scScan(&head,scpath,0))<0)
      goto BAIL;

    if(oconfig->numMetaTable[METATOK_BETA])
      oconfig->betaPass=1;

    if((r=scPatch(&head))<0)
      goto BAIL;

    for(;;) {
      chr *cmd=0;
      if((r=scBuildCompileCmd(&head,&cmd))<0)
        goto BAIL;
      if((r=scWriteCode(&head,cmd))>=0)
        r=scRunCompileCmd(&head,cmd);
      if(cmd)
        MemFree(cmd);
      if(r) // any return from compiling is considered an error
        goto BAIL;
      if(!oconfig->betaPass)
        break;
      oconfig->betaPass=0;
    } // for
  } // build

  rc=0;
  if(!flagdontrun) {
    PRINT_SYS_MSG("running..\n");
    if(!(oconfig->flags&M_OBJCONFIG_DEBUGGER)) {
      if((r=scRun(&head))<0)
        goto BAIL;
    } else {
      if((r=scDebug(&head))<0)
        goto BAIL;
    }
    rc=r;
    //PRINT_SYS_MSG("rc=%d\n",rc);

    // handle return code that indicates dependency check requires rebuild
    if(rc==MAINRC_REBUILD) {

      if(flagbuild) {
        // just built - error - prevent loop
        PRINT_SYS_ERR("rebuild error - use +nolibdep\n");
      } else {
        //flagdepcheck=0; // prevent loop
        flagbuild=1; // signal to build
        PRINT_SYS_MSG("rebuild signalled\n");
        goto rebuild_loop;
      }
    }
  } // !flagdontrun

  // build lib after running to allow unit-tests
  if(flagbuild && oconfig->libBuild) {
    if(!rc) { // successful run
      // build the lib
      chr *cmd=0,*ar=0;
      if((r=scBuildLibCompileCmd(&head,&cmd))>=0)
        r=scBuildLibArCmd(&head,&ar);
      if(r>=0) {
        if((r=scWriteLibCode(&head,cmd,ar))>=0)
          r=scCompileLib(&head,cmd,ar);
      }
      if(ar)
        MemFree(ar);
      if(cmd)
        MemFree(cmd);
      if(r) // any return from compiling is considered an error
        goto BAIL;
    } else {
      PRINT_SYS_MSG("library not built\n");
    }
  } // libBuild

  if(flagclean)
    scCleanTempFiles(&head);

  PRINT_SYS_VRB("mainrc:%d\n",rc);

  // return the value r
  r=rc;
BAIL:
  ObjListZap(&head);
  if((i=MemCount())) {
    PRINT_SYS_ERR("MemCount:%d\n",i);
    BRK();
  }
  return r;
} // main()

// EOF
