// sz.h
// Copyright (C) 2011-2018 Joe Linhoff, All Rights Reserved.
#ifndef STZ_H
#define STZ_H

#include "base.h"

#ifdef __cplusplus // C in C++ wrapper
PFM_API "C" {       //
#endif             //

///////////////////////////////////////////////////////////////////////////////
// SZ STRINGS

#define C_IS_EOL(c) ((c=='\n')||(c=='\r'))
#define C_IS_WHITESPACE(c) ((c==' ')||(c=='\t'))
#define C_IS_WHITE(c) ((c==' ')||(c=='\t')||(c=='\n')||(c=='\r'))
#define C_IS_ID(c) (((c>='A')&&(c<='Z')) \
  || ((c>='a')&&(c<='z')) || ((c>='0')&&(c<='9')) || (c=='_'))
#define C_IS_DIGIT(c) ((c>='0')&&(c<='9'))
#define C_SZ_SKIP (1)
#define C_IS_NUMMOD(c) \
  (((c>='0')&&(c<='9'))||(c=='.')||(c=='-')||(c=='e')||(c=='E'))
#define C_IS_UPPER(_c_) (((_c_)>='A')&&((_c_)<='Z'))

PFM_API cchr* stzskipwhitespace(cchr *s1,cchr *s1x);
PFM_API cchr* stzskipwhite(cchr *s1,cchr *s1x);
PFM_API cchr* stztillwhite(cchr *s1,cchr *s1x);
PFM_API cchr* stzskiparg(cchr *str,cchr*strx,cchr **s1p,cchr **s1xp);
PFM_API cchr* stztrimwhite(cchr *s1,cchr *s1x);
PFM_API cchr* stzskipeol(cchr *s1,cchr *s1x);
PFM_API cchr* stztilleol(cchr *s1,cchr *s1x);
PFM_API cchr* stzskipid(cchr *s1,cchr *s1x);
PFM_API cchr* stztillid(cchr *s1,cchr *s1x);
PFM_API cchr* stztillchr(cchr *s1,cchr *s1x,cchr *till,cchr *tillx);
PFM_API cchr *stzlastslash(cchr *s1,cchr *s1x);
PFM_API cchr* stzchr(cchr *s1,cchr *s1x,chr match);
PFM_API cchr* stzrchr(cchr *s1,cchr *s1x,chr match);
PFM_API int stzstart(cchr *start,cchr *startx,cchr *s2,cchr *s2x);
PFM_API int stzchrreplace(chr *str,chr*strx,chr from,chr to);
PFM_API int stztolower(chr *str,chr *strx);
PFM_API int stzcpy(chr *dst,int dstSize,cchr *s1,cchr *s1x);
PFM_API int stzcmp(cchr *s1,cchr *s1x,cchr *s2,cchr *s2x);
PFM_API int stzicmp(cchr *s1,cchr *s1x,cchr *s2,cchr *s2x);
PFM_API cchr* stzsub(cchr *sub,cchr *subx,cchr *s2,cchr *s2x);
PFM_API cchr* stzisub(cchr *sub,cchr *subx,cchr *s2,cchr *s2x);
PFM_API int stzcountchrs(cchr *str,cchr*strx,cchr*count,cchr*countx);
PFM_API int stzcountlines(cchr *s1,cchr *end);
PFM_API int stzcat(chr *dst,int n,cchr *s1,cchr *s1x,cchr *s2,cchr *s2x);
PFM_API int stzimap(cchr *table[],cchr *s1,cchr *s1x);
PFM_API int stzeos(cchr *s1,cchr *s1x);
PFM_API int stzlen(cchr *s1,cchr *s1x);
PFM_API int stzsize(cchr *s1,cchr *s1x);
PFM_API int stzfmt(chr *dst,int dstSize,cchr *fmt,...);
PFM_API int stzfmt_v(chr *dst,int dstSize,cchr *fmt,va_list args);
PFM_API cchr* stztobin(cchr *s1,cchr *s1x,uns8 base,chr dsttype,void *dstp);

PFM_API int stznume64(chr *dst,int dstSize,flt64 *nump);

#ifdef __cplusplus // C in C++ wrapper
} //
#endif //

#endif // STZ_H
