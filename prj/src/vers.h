#define MAINVERS_SZ "84"
//   84 01/03/25 - instantcc softlink for c++
//   83 02/29/24 - assignment in core in parens
//   82 01/19/24 - fix for szlib 0.01f
// 0.81 01/02/24 - SUDO ?=
// 0.80 06/04/23 - default c include stdint
// 0.79 11/12/22 - linker dependencies
// 0.78 08/05/22 - PRIu64 for time
// 0.77 08/05/22 - va_list args passed
// 0.76 03/27/22 - multiline metablocks, shellcc
