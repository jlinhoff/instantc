// base.h
// Copyright (C) 2018-2019 JoeCo, All Rights Reserved.
#ifndef BASE_H
#define BASE_H

#include <string.h>
#include <stdint.h>
#include <stdarg.h>

#if BUILD_MINGW
#include <malloc.h>
#else
#include <alloca.h>
#endif

#ifdef __cplusplus // C in C++
extern "C" {       //
#endif             //

///////////////////////////////////////////////////////////////////////////////

#if UINTPTR_MAX == UINT64_MAX
#define BUILD_64BIT 1
#else // !BUILD_64BIT
#define BUILD_64BIT 0
#endif // !BUILD_64BIT

// base types
typedef signed char int8;     // 8 bit signed int
typedef unsigned char uns8;   // 8 bit unsigned int
typedef uns8 bit;             // 1 or 0; 8 bit unsigned
typedef char chr;             // character for internal strings; 8 bit unsigned
typedef chr const cchr;       // const cchr
typedef chr const chrc;       // const cchr
typedef signed short int16;   // 16 bit signed int
typedef unsigned short uns16; // 16 bit unsigned int
typedef signed int int32;     // 32 bit signed int
typedef int32 off32;          // all offsets are from their adr base
typedef unsigned int uns32;   // 32 bit unsigned bit
typedef unsigned int uns;     // unsigned natural int
typedef float flt32;          // 32 bit float
typedef double flt64;         // 64 bit float
typedef void* ptr;            // pointer
typedef long long int64;      // 64 bit signed int
typedef unsigned long long uns64;  // 64 bit unsigned int
#if BUILD_64BIT
typedef int64 intptr; // pointer as int
#else                 // !BUILD_64BIT
typedef int32 intptr; // pointer as int
#endif                // !BUILD_64BIT

// define pointer type for 64 or 32 bit data
typedef struct {
  union {
    void* p; // treat as pointer
    chr* ps;
    flt32* pf;
    int32* pi;
    uns32* pu;
    uns32 u[2]; // 0:lo 1:hi -- force 64 bits on 32 bit machine
  }v;
} ptr64;

#define ret(_r_) \
  do {           \
    r = (_r_);   \
    goto BAIL;   \
  } while(0)
#define bret(_r_) \
  do {            \
    BRK();        \
    r = (_r_);    \
    goto BAIL;    \
  } while(0)
#define bbail() \
  do {          \
    BRK();      \
    goto BAIL;  \
  } while(0)
#define bbreak() \
  {              \
    BRK();       \
    break;       \
  }
#define bcontinue() \
  {                 \
    BRK();          \
    continue;       \
  }
#define bgoto(_lb_) \
  do {              \
    BRK();          \
    goto _lb_;      \
  } while(0)

#define COMPILE_ASSERT(e)  ((void)sizeof(char[1 - 2*!(e)]))

#define ALIGN(_n_, _m_) ((((uns)_n_) + (_m_)) & ~(_m_))
#define ALIGNMACH(_n_) ALIGN(_n_,3)
#define UNUSED(_x_) ((void)(_x_))

#define SZ(_str_) ((chr*)(_str_))

#define STRINGIFY(_a_) #_a_

// generate a compiler warning
#define WARNING(_n_)   \
  do {                 \
    int warning_##_n_; \
  } while(0)

// function types -- void int uns flt ptr
typedef void (*fnc_v)(void);
typedef int (*fnc_i)(void);
typedef void (*fnc_vp)(void*);
typedef void* (*fnc_pp)(void*);
typedef int (*fnc_ip)(void*);
typedef uns (*fnc_up)(void*);
typedef int (*fnc_iii)(int, int);
typedef int (*fnc_ipp)(void*, void*);
typedef int (*fnc_iipp)(int, void*, void*);
typedef int (*fnc_iippa)(int, void*, void*, ...);
typedef int (*fnc_ipa)(void*, ...);
typedef void (*fnc_vpi)(void*, int);
typedef int (*fnc_ipi)(void*, int);
typedef void (*fnc_vpia)(void*, int, ...);
typedef int (*fnc_ipia)(void*, int, ...);
typedef int (*fnc_ipip)(void*, int, void*);
typedef int (*fnc_iia)(int, ...);
typedef int (*fnc_iip)(int, void*);
typedef int (*fnc_iipa)(int, void*, ...);
typedef int (*fnc_ippp)(void*, void*, void*);
typedef int (*fnc_ippi)(void*, void*, int);
typedef int (*fnc_ippia)(void*, void*, int, ...);
typedef int (*fnc_ippa)(void*, void*, ...);

#define NUM(_a_) (sizeof(_a_) / sizeof(_a_[0])) ///< number of entries

#define PTR_ADD(_p_, _i_) (void*)(((uns8*)(_p_)) + (_i_))
#define PTR_SUB(_p_, _i_) (void*)(((uns8*)(_p_)) - (_i_))
#define PTR_DIFF(_p1_, _p2_) ((int)((uns8*)(_p1_) - (uns8*)(_p2_)))
#define PTR_OVERLAP(_p_, _lo_, _hi_) \
  ((((ptr)_p_) >= ((ptr)_lo_)) && (((ptr)_p_) < ((ptr)_hi_)))
#define PTR_OVERLAPX(_p_, _lo_, _hi_) \
  ((((ptr)_p_) >= ((ptr)_lo_)) && (((ptr)_p_) <= ((ptr)_hi_)))

// test for NaN, inf, ind
#define FLT_IS_ERR(_f_) (((_f_) != (_f_)) || (0 != (_f_) - (_f_)))

#define SET2(d,a,b) (d)[0]=(a),(d)[1]=(b)

// VARIABLE ARGUMENTS
#if BUILD_MSVC
typedef uns8* vargs;
#define VARG_SIZEOF(n) ((sizeof(n) + sizeof(int) - 1) & ~(sizeof(int) - 1))
#define varg_start(ap, v) (ap = (vargs)&v + VARG_SIZEOF(v))
#define varg_next(ap, t) (*(t*)((ap += VARG_SIZEOF(t)) - VARG_SIZEOF(t)))
#define varg_prev(argp, type) \
  (*(type*)((argp -= VARG_SIZEOF(type)) - VARG_SIZEOF(type)))
#define varg_next_vargs(_args_)              \
  {                                          \
    vargs* varp = varg_next(_args_, vargs*); \
    _args_ = *varp;                          \
  }

#define varg_end(x)
#define FLOAT_PASSED_AS double
#define FLOATS_PASSED_AS double
#else // !BUILD_MSVC
#include <stdarg.h>
#define vargs va_list
#define varg_start va_start
#define varg_end va_end
#define varg_next va_arg
#ifndef va_copy
#define va_copy __va_copy
#endif // ndef va_copy
// typedef struct { va_list a; } varg_rec;
#define varg_next_vargs(_args_)            \
  {                                        \
    vargs* varp = varg_next(args, vargs*); \
    va_copy(_args_, *varp);                \
  }
#define FLOAT_PASSED_AS double
#define FLOATS_PASSED_AS double
#endif // !BUILD_MSVC

#ifndef PFM_API
#if BUILD_PFM_LIB
#define PFM_API extern
#elif BUILD_PFM_EXPORTS
#define PFM_API __declspec(dllexport)
#else
#define PFM_API __declspec(dllimport)
#endif
#endif // ndef PFM_API

#define IROM0

PFM_API int pfmLogf(chr*, ...);

#if DEBUG
// note on BRK()
// - it is safest to use the pfmBrkf() b/c it goes to log window
// - interrupt versions make for better debugging if env supports them
// - raising interrupts in child threads interrupts parent thread
#if BUILD_MSVC
PFM_API int main_nobrk;
#define BRK()                              \
  (main_nobrk ? pfmLogf("* BRK() "__FILE__ \
                        ":%d\n",           \
                    __LINE__) :            \
                __debugbreak())
#elif BUILD_IOS
PFM_API int main_nobrk;
// PFM_API int pfmBrkf(chr *,...);
#define BRK() (main_nobrk ? 1 : raise(SIGTRAP), 1)
//#define BRK() (main_nobrk?1:pfmBrkf("* BRK() "__FILE__":%d\n",__LINE__),1)
#elif BUILD_MINGW
// in Settings > Debugger > Startup Commands: b pfmBrkf
PFM_API int main_nobrk;
PFM_API int pfmBrkf(chr*, ...);
#define BRK() \
  do{ if(!main_nobrk) { \
    pfmBrkf("* BRK() "__FILE__ ":%d\n", __LINE__); \
    __debugbreak(); \
  }}while(0)
#else // other
PFM_API int main_nobrk;
#define BRK()                              \
  (main_nobrk ? 1 :                        \
                pfmLogf("* BRK() "__FILE__ \
                        ":%d\n",           \
                    __LINE__))
#endif // other
#else  // !DEBUG
PFM_API int main_nobrk;
#define BRK() (1)
#endif // !DEBUG

#define PRINT_X \
  X(RAW) \
  X(COM) \
  X(SYS)

#define X(n) PRINT_##n ,
enum { PRINT_X PRINT_NUMOF};
#undef X

#define M_PRINT_ERR 0x00
#define M_PRINT_MSG 0x01
#define M_PRINT_VRB 0x02

PFM_API void MainPrintf(int pf,cchr *fmt,...);
PFM_API int MainLogOutput(chr *buf,uns len);

#define VA_ARGS(...) , ##__VA_ARGS__

#define PRINT_RAW_MSG(_fmt_,...) do{if(!(BaseG.printLevel[PRINT_RAW]&M_PRINT_MSG)) MainPrintf(PRINT_RAW,_fmt_ VA_ARGS(__VA_ARGS__));}while(0)
#define PRINT_RAW_VRB(_fmt_,...) do{if(!(BaseG.printLevel[PRINT_RAW]&M_PRINT_VRB)) MainPrintf(PRINT_RAW,_fmt_ VA_ARGS(__VA_ARGS__));}while(0)
#define PRINT_RAW_ERR(_fmt_,...) do{if(!(BaseG.printLevel[PRINT_RAW]&M_PRINT_ERR)) MainPrintf(PRINT_RAW,"*"_fmt_ VA_ARGS(__VA_ARGS__));}while(0)

#define PRINT_SYS_MSG(_fmt_,...) do{if(!(BaseG.printLevel[PRINT_SYS]&M_PRINT_MSG)) MainPrintf(PRINT_SYS,_fmt_ VA_ARGS(__VA_ARGS__));}while(0)
#define PRINT_SYS_VRB(_fmt_,...) do{if(!(BaseG.printLevel[PRINT_SYS]&M_PRINT_VRB)) MainPrintf(PRINT_SYS,_fmt_ VA_ARGS(__VA_ARGS__));}while(0)
#define PRINT_SYS_ERR(_fmt_,...) do{if(!(BaseG.printLevel[PRINT_SYS]&M_PRINT_ERR)) MainPrintf(PRINT_SYS,"*"_fmt_ VA_ARGS(__VA_ARGS__));}while(0)
#define IS_SYS_VRB() (!(BaseG.printLevel[PRINT_SYS]&M_PRINT_VRB))

#define PRINT_COM_MSG(_fmt_,...) do{if(!(BaseG.printLevel[PRINT_COM]&M_PRINT_MSG)) MainPrintf(PRINT_COM,_fmt_ VA_ARGS(__VA_ARGS__));}while(0)
#define PRINT_COM_VRB(_fmt_,...) do{if(!(BaseG.printLevel[PRINT_COM]&M_PRINT_VRB)) MainPrintf(PRINT_COM,_fmt_ VA_ARGS(__VA_ARGS__));}while(0)
#define PRINT_COM_ERR(_fmt_,...) do{if(!(BaseG.printLevel[PRINT_COM]&M_PRINT_ERR)) MainPrintf(PRINT_COM,"*"_fmt_ VA_ARGS(__VA_ARGS__));}while(0)

////////////////////////////////////////////////////////////////////////////////

#define MEMZ(_x_) memset((void*)&(_x_), 0, sizeof(_x_))

PFM_API int MemAlloc(void *outp,uns size);
PFM_API int MemAlloz(void *outp,uns size);
PFM_API int MemCount(void);
PFM_API void MemFree(void *p);

////////////////////////////////////////////////////////////////////////////////

typedef struct baselink {
  struct baselink *p; // prev
  struct baselink *n; // next
  uns8 t;        // BASETYPE_
  uns8 f;        // M_BASELINK_
} BaseLink;

//#define M_BASELINK_SID 0x3f

#define BL(_n_) ((BaseLink*)(_n_))
#define BLPREV(_n_) (BL(_n_)->p)
#define BLNEXT(_n_) (BL(_n_)->n)

PFM_API void BaseLinkMakeHead(BaseLink* h);
PFM_API void BaseLinkMakeNode(BaseLink* h, int t);
PFM_API void BaseLinkBefore(BaseLink* h, BaseLink* n);
PFM_API void BaseLinkAfter(BaseLink* h, BaseLink* n);
PFM_API void BaseLinkUnlink(BaseLink* n);

////////////////////////////////////////////////////////////////////////////////

#define TICK_FROM_MILLISECONDS(_ms_) (_ms_)
#define TICK_FROM_SECONDS(_sec_) ((_sec_)*1000)

//#define TIME_DAY (60 * 60 * 24)
//#define TIME_YEAR (TIME_DAY * 365)
//#define TIME_BASE (TIME_YEAR * (2010 - 1970))

PFM_API uns64 MainTimeNow();
PFM_API uns64 MainTimeToday();

#define M_BASEREADFILE_QUIET        0x01
#define M_BASEREADFILE_EOLGUARANTEE 0x10 // add EOL at end (if not already)
PFM_API int BaseReadFile(cchr *fpath,cchr *fpathx,chr **bufp,int flags);

#define M_BASEPATHALT_DIFF     0x01
#define M_BASEPATHALT_WINSLASH 0x04
#define M_BASEPATHALT_NOROOT   0x08
#define M_BASEPATHALT_SETROOT  0x10
PFM_API chr *BasePathAlt(chrc *path,chrc *pathx,int flags);

PFM_API int BaseHomeDir(chr *dst,int dstMax);

PFM_API uns64 MainTimeFromYMD(int year, int month, int day);
PFM_API uns64 MainTimeFromYMDHHMMSS(uns16 y,uns8 m,uns8 d,
  uns8 hh,uns8 mm,uns8 ss);
PFM_API uns64 MainTimeFromYJHHMMSS(uns16 y,uns16 j,uns8 hh,uns8 mm,uns8 ss);

PFM_API int MainTimeToYMDHHMMSS(uns64 tt,uns16 *yOut,uns8 *mOut,uns8 *dOut,
  uns8 *hhOut,uns8 *mmOut,uns8 *ssOut);
PFM_API int32 MainTimeZoneOffsetSeconds();

// MainTimeStr()
// fmt is string, unmapped characters are copied
// map: y=year, Y=year%100, m=month, d=day, H=hour,M=min,S=sec
PFM_API int MainTimeStr(chr *dst,int dstSize,cchr *fmt,uns64 t);

PFM_API int BaseScanYearMonthDay(cchr *str,cchr *strx,
  uns16 *yearp,uns8 *monthp,uns8 *dayp,uns8 mdy);

PFM_API int BaseScanHourMinSec(cchr *str,cchr *strx,
  uns8 *hourp,uns8 *minp,uns8 *secp);

PFM_API int MainSingleToDoubleQuotes(chr* str, chr* strx);

enum {BASERUNSCRIPT_QUIET,BASERUNSCRIPT_STDOUT,BASERUNSCRIPT_STDERR,BASERUNSCRIPT_STDOUTERR};
PFM_API int BaseRunScript(chr **bufp,cchr *scr,cchr *scrx,int flagEcho);
PFM_API int BaseLaunchScript(cchr *scr,cchr *scrx,chr*const* envarr);
 // envarr must be zero terminated
PFM_API int BaseFileDate(uns64 *secp,cchr *path,cchr *pathx);

PFM_API int BaseInit();
PFM_API void BaseFinal();

enum {
  ERR_MEM_ALLOC=-1900,

  ERR_OBJ_ALLOC=-2900,
  ERR_OBJ_RANGE,
  ERR_OBJ_NOTIMPLEMENTED,
  ERR_OBJ_ZERO,

  ERR_JSON_NOTHANDLED=-3999,
  ERR_JSON_NOTFOUND,
  ERR_JSON_RANGE,
};

typedef struct {
  chr* strLogfileNamePtr;
  uns32 tick;
  uns8 printLevel[PRINT_NUMOF]; // print flags
  int allocCount;
  int32 tzOffSeconds;
} BaseGlobals;

PFM_API BaseGlobals BaseG;

///////////////////////////////////////////////////////////////////////////////
#ifdef __cplusplus // C in C++
} //
#endif //

#endif // BASE_H
