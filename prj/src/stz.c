// sz.c
// Copyright (C) 2011-2018 JoeCo, All Rights Reserved.
#include <math.h> // for pow
#include "stz.h"

// JFL 18 Mar 18
int stzlen(cchr *s1,cchr *s1x)
{
  cchr *start;
  if(!s1) return 0;
  for(start=s1;(!s1x||(s1<s1x))&&*s1;s1++)
    /* do nothing */ ;
  return s1-start;
} // stzlen()

// JFL 18 Mar 18
int stzsize(cchr *s1,cchr *s1x)
{
  int r = stzlen(s1,s1x);
  if(r) r++;
  return r;
} // stzsize()

// JFL 17 Mar 18
cchr* stzchr(cchr *s1,cchr *s1x,chr match)
{
  chr c;
  if(!s1) return 0;
  for(;(c=*s1)&&(!s1x||(s1<s1x));s1++) {
    if(c == match)
      return s1;
  } // for
  return 0;
} // stzchr()

// JFL 17 Sep 19
cchr* stzrchr(cchr *s1,cchr *s1x,chr match)
{
  chr c;
  if(!s1) return 0;
  if(!s1x)
    s1x=s1+stzlen(s1,0);

  for(s1x--;(s1<=s1x) && (c=*s1x);s1x--) {
    if(c == match)
      return s1x;
  } // for
  return 0;
} // stzrchr()

// JFL 17 Mar 18
cchr* stztillwhite(cchr *s1,cchr *s1x)
{
  char c;
  for(;(!s1x||(s1<s1x))&&(c=*s1);s1++) {
    if(!C_IS_WHITE(c)) continue;
    break;
  } // for
  return s1; 
} // stztillwhite()

// JFL 28 Mar 22
cchr* stzskipwhitespace(cchr *s1,cchr *s1x)
{
  char c;
  for(;(!s1x||(s1<s1x))&&(c=*s1);s1++) {
    if(C_IS_WHITESPACE(c)) continue;
    break;
  } // for
  return s1; 
} // szskipwhitespace()

// JFL 17 Mar 18
cchr* stzskipwhite(cchr *s1,cchr *s1x)
{
  char c;
  for(;(!s1x||(s1<s1x))&&(c=*s1);s1++) {
    if(C_IS_WHITE(c)) continue;
    break;
  } // for
  return s1; 
} // szskipwhite()

// JFL 23 Jun 19
cchr* stztrimwhite(cchr *s1,cchr *s1x)
{
  char c;
  if(!s1) return 0;
  if(!s1x) s1x=s1+stzlen(s1,0);
  for(;s1<s1x;s1x--) {
    c=s1x[-1];
    if(C_IS_WHITE(c)) continue;
    break;
  } // for
  return s1x; 
} // stztrimwhite()

// JFL 17 Mar 18
cchr* stzskipid(cchr *s1,cchr *s1x)
{
  char c;
  for(;(!s1x||(s1<s1x))&&(c=*s1);s1++) {
    if(C_IS_ID(c)) continue;
    break;
  } // for
  return s1; 
} // stzskipid()

// JFL 17 Mar 18
cchr* stztillid(cchr *s1,cchr *s1x)
{
  char c;
  for(;(!s1x||(s1<s1x))&&(c=*s1);s1++) {
    if(!C_IS_ID(c)) continue;
    break;
  } // for
  return s1; 
} // stztillid()

// JFL 18 Mar 18
cchr* stzskipeol(cchr *s1,cchr *s1x)
{
  char c;
  for(;(!s1x||(s1<s1x))&&(c=*s1);s1++) {
    if(C_IS_EOL(c)) continue;
    break;
  } // for
  return s1; 
} // stzskipeol()

// JFL 18 Mar 18
cchr* stztilleol(cchr *s1,cchr *s1x)
{
  char c;
  for(;(!s1x||(s1<s1x))&&(c=*s1);s1++) {
    if(!C_IS_EOL(c)) continue;
    break;
  } // for
  return s1; 
} // stztilleol()

// JFL 04 Jun 19
cchr* stztillchr(cchr *s1,cchr *s1x,cchr *till,cchr *tillx)
{
  char c;
  cchr *match;
  if(!tillx) tillx=till+stzlen(till,0);
  for(;(!s1x||(s1<s1x))&&(c=*s1);s1++) {
    for(match=till;match<tillx;match++) {
      if(c == *match)
        goto done;
    } // for
  } // for
done:
  return s1; 
} // stztillchr()

// JFL 05 Nov 19
cchr* stzskiparg(cchr *str,cchr*strx,cchr **s1p,cchr **s1xp)
{
  cchr *s1;
  if(((*str=='\"')||(*str=='\''))
    && (s1=stzchr(str+1,strx,*str))) {
    if(s1p)
      *s1p=str+1;
    if(s1xp)
      *s1xp=s1;
    str=s1+1;
  } else {
    if(s1p)
      *s1p=str;
    str=stztillwhite(str,strx);
    if(s1xp)
      *s1xp=str;
  }
  return str;
} // stzskiparg()

// JFL 02 Nov 05
// JFL 01 Feb 07
// JFL 15 Sep 11; start startx
int stzstart(cchr *start,cchr *startx,cchr *s2,cchr *s2x)
{ // test if string s2 starts with s1
   chr s,t;

   if(!start || !s2)
      return -98;

   while((!s2x || (s2<s2x)) && (!startx || (start<startx)))
   {
      s=*start++;
      t=*s2++;
      if(!s) // source hit end, match
         return 0;
      if(s!=t) // mismatch
         return s-t;
   } // while

   if(!*start || (startx && (start>=startx)))
      return 0;

   return -99;
} // stzstart()

// JFL 28 May 19
int stzcountlines(cchr *s1,cchr *end)
{
  int n=1;
  if(!end)
    end=s1+stzlen(s1,0);
  for(;s1<end;s1++) {
    if((*s1=='\n')||(*s1=='\r')) {
      n++;
      if(((s1[1]=='\n')||(s1[1]=='\r'))&&(s1[0]!=s1[1]))
        s1++;
    }
  } // for
  return n;
} // stzcountlines()

// JFL 18 Mar 18
int stzcpy(chr *dst,int dstSize,cchr *s1,cchr *s1x)
{
  chr *dst0=dst;
  if(!s1||(dstSize<1)) return 0;
  dstSize--; // always leave room for term
  for(;dstSize;dstSize--) {
    if((s1x&&(s1>=s1x)) || !*s1)
      break;
    *dst++=*s1++;
  } // for
  *dst=0; // always term
  return dst-dst0;
} // stzcpy()

// JFL 17 Mar 18
int stzcmp(cchr *s1,cchr *s1x,cchr *s2,cchr *s2x)
{
  chr c1,c2;
  if(!s1 || !s2) return -100;
  for(;;s1++,s2++) {
    if(s1x && (s1>=s1x))
      c1 = 0;
    else
      c1 = *s1;

    if(s2x && (s2>=s2x))
      c2 = 0;
    else
      c2 = *s2;
    
    if(!c1 || !c2) {
      return c1 - c2;
    }
    
    if(c1 == c2)
      continue;
    return c1 - c2;
  } // for
  return -99;
} // stzcmp()

// JFL 17 Mar 18
int stzicmp(cchr *s1,cchr *s1x,cchr *s2,cchr *s2x)
{
  chr c1,c2;
  if(!s1 || !s2) return -100;
  for(;;s1++,s2++) {
    if(s1x && (s1>=s1x))
      c1 = 0;
    else
      c1 = *s1;

    if(s2x && (s2>=s2x))
      c2 = 0;
    else
      c2 = *s2;
    
    if(!c1 || !c2) {
      return c1 - c2;
    }
    
    if((c1>='A')&&(c1<='Z'))
      c1+='a'-'A';
    if((c2>='A')&&(c2<='Z'))
      c2+='a'-'A';
    if(c1 == c2)
      continue;
    return c1 - c2;
  } // for
  return -99;
} // stzicmp()

// JFL 20 Oct 05
// JFL 22 Jan 07; return NUL on failure
cchr* stzsub(cchr *sub,cchr *subx,cchr *s2,cchr *s2x)
{ // find first occurrence of sub in string s2
  cchr *ss,*tt;
  chr c,d;

   if(!s2)
      return s2;
lp:
   if(s2x&&(s2>=s2x))
      return 0; // fail
    if(!*s2)
      return 0; // fail

   for(ss=sub,tt=s2;;ss++,tt++)
   {
      if(subx&&(ss>=subx))
         return s2; // success
      if(!*ss)
         return s2; // success
      if(s2x&&(tt>=s2x))
         return 0; // fail
      if(!(c=*tt))
         return 0; // fail
      d=*ss;
      if(c!=d)
         break; // mismatch
   } // for
   s2++;
   goto lp;
} // stzsub()

// JFL 20 Oct 05
// JFL 22 Jan 07; return NUL on failure
// JFL 27 Jun 19; isub
cchr* stzisub(cchr *sub,cchr *subx,cchr *s2,cchr *s2x)
{ // find first occurrence of sub in string s2
  cchr *ss,*tt;
  chr c,d;

   if(!s2)
      return s2;
lp:
   if(s2x&&(s2>=s2x))
      return 0; // fail
    if(!*s2)
      return 0; // fail

   for(ss=sub,tt=s2;;ss++,tt++)
   {
      if(subx&&(ss>=subx))
         return s2; // success
      if(!*ss)
         return s2; // success
      if(s2x&&(tt>=s2x))
         return 0; // fail
      if(!(c=*tt))
         return 0; // fail
      d=*ss;
      if((c>'A') && (c<'Z'))
        c+='a'-'A';
      if((d>'A') && (d<'Z'))
        d+='a'-'A';      
      if(c!=d)
         break; // mismatch
   } // for
   s2++;
   goto lp;
} // stzisub()

// JFL 02 Jun 19
int stzchrreplace(chr *str,chr*strx,chr from,chr to)
{
  if(!str) return 0;
  if(!strx) strx=str+stzlen(str,strx);
  for(;str<strx;str++) {
    if(*str!=from) continue;
    *str=to;
  } // for
  return 0;
} // stzchrreplace()

// JFL 29 Oct 19
int stztolower(chr *str,chr *strx)
{
  chr c;
  if(!str) return 0;
  for(;;str++) {
    if(strx && (str>=strx))
      break;
    if(!(c=*str))
      break;
    if(!C_IS_UPPER(c))
      continue;
    *str=c-'A'+'a';
  } // for
  return 0;
} // stztolower()

// JFL 17 Mar 18
int stzimap(cchr **table,cchr *s1,cchr *s1x)
{
    int i;
    for(i=0;*table;i++,table++) {
      if(!stzicmp(*table,0,s1,s1x))
        return i;
    } // for
    return -1;
} // stzimap()

// JFL 18 Mar 18
int stzeos(cchr *s1,cchr *s1x)
{
  if(!s1 || (s1x&&(s1>=s1x)))
    return 1;
  return *s1 ? 0 : 1;
} // stzeos()

// JFL 22 Sep 19
int stzcountchrs(cchr *str,cchr*strx,cchr*count,cchr*countx)
{
  int r;
  int32 i,num=0;
  int32 countlen=stzlen(count,countx);
  cchr *s1;
  chr c;
  if(!strx) strx=str+stzlen(str,0);
  for(;str<strx;) {
    c=*str++;
    for(s1=count,i=countlen;i>0;i--) {
      if(c==*s1++) {
        num++;
        break;
      }
    } // for
  } // for
  r=num;
//BAIL:
  return r;
} // stzcountchrs()

// JFL 09 Aug 19
cchr *stzlastslash(cchr *s1,cchr *s1x)
{
  cchr *s2;
  if(!s1) return 0;
  while((s2=stzchr(s1+1,s1x,'/'))||(s2=stzchr(s1+1,s1x,'\\')))
    s1=s2;
  return ((*s1=='/') || (*s1=='\\')) ? s1 : 0;
} // stzlastslash()

// JFL 15 Aug 04
// JFL 01 Feb 07; sizeof(chr)
// JFL 17 Jun 07; return review
// JFL 18 Dec 10; two string version
int stzcat(chr *dst,int n,cchr *s1,cchr *s1x,cchr *s2,cchr *s2x)
{ // concatenate string to existing
   int org;

   if((n<0) || !s1) return 0;
   org=n;

   // skip over current string
   while(n)
   {
      if(!*dst) break;
      dst++;
      n--;
   } // while

   // concatenate first string
   if(s1)
   while(n && (!s1x || (s1<s1x)))
   {
      if(!*s1) break;
      *dst++=*s1++;
      n--;
   } // while

   // concatenate second string
   if(s2)
   while(n && (!s2x || (s2<s2x)))
   {
      if(!*s2) break;
      *dst++=*s2++;
      n--;
   } // while

    *dst=0;

   return org-n;
} // stzcat()

static double sztobinDivisors[] = {
1.0/1.0,
1.0/10.0, 
1.0/100.0, 
1.0/1000.0, 
1.0/10000.0, 
1.0/100000.0,
1.0/1000000.0,
1.0/10000000.0,
1.0/100000000.0,
1.0/1000000000.0,
1.0/10000000000.0,
1.0/100000000000.0,
1.0/1000000000000.0,
1.0/10000000000000.0,
1.0/100000000000000.0,
1.0/1000000000000000.0,
1.0/10000000000000000.0,
1.0/100000000000000000.0,
};

static uns64 sztobinPow[] = {
1,
10,
100,
1000,
10000,
100000,
1000000,
10000000,
100000000,
1000000000,
10000000000,
100000000000,
1000000000000,
10000000000000,
100000000000000,
1000000000000000,
10000000000000000,
100000000000000000,
};

#if 0
flt64 rman;
  
  if(dstSize<3)
    ret(0);

  int64 m = *((int64*)nump);
  int exp = (m>>52)&0x7ff;
  uns8 isneg = m<0;
  m &= 0x000fffffffffffff;

  if(!exp)
    exp++;
  else
    m|=((int64)1<<52);
  exp -= 1075; // bias of 1023 - 52

  rman = m;
  while(rman>1)
    rman*=0.5,exp++;

  //if(isneg)
  //  rman = -rman;

#endif
#include <stdio.h>
// JFL 09 Jul 19
int stznume64(chr *dst,int dstSize,flt64 *nump)
{ // return num chrs added
  int r;
  int bs=dstSize;
  int16 exp=0;
  chr *bb=dst;
  uns8 isneg=0;
  
  flt64 f64 = *((flt64*)nump);
  if(f64<0)
    isneg=1,f64=-f64;

  exp = log10(f64);
  flt64 man=f64/pow(10,exp);

  COMPILE_ASSERT(NUM(sztobinPow)>=14);
  int i=14;
  uns64 q1 = man * sztobinPow[i];
  uns64 q2;
  uns16 c;
  uns8 dotted=0;
  
  if(isneg)
    *bb++='-',bs--;

  for(;(i>0) && (bs>0);i--) {
    q2 = sztobinPow[i];
    c = q1 / q2;
    *bb++=c+'0',bs--;
    if(!dotted)
      dotted=1,*bb++='.',bs--;
    q1 -= c * sztobinPow[i];
    
    if(!q1) // if only zeros
      break;
  } // for

  if(exp) {
    r=stzfmt(bb,bs,"e%d",exp);
    bb+=r,bs-=r;
  }
  
  r=dstSize-bs;

//BAIL:
  return r;
} // stznume64()

// JFL 14 Apr 18
// JFL 06 Nov 18
// JFL 20 Jan 20
// JFL 04 Feb 20
int sznum(chr *dst,int dstSize,chr fmt,void *nump)
{ // return num chrs added
  int r;
  uns32 u32;
  uns64 u64;
  chr digits[32],aa;
  int8 d;
  uns8 neg=0,dlen,base,is64=0,isflt=0,fltdot=0,isu=0;

  if(fmt=='x')
    base = 16,aa = 'a'-10,isu=1;
  else if(fmt=='X')
    base = 16, aa = 'A'-10,isu=1;
  else if(fmt=='q')
    base = 10, aa = 'a'-10, is64=1;
  else if(fmt=='y')
    base = 16, aa = 'a'-10, isu=1, is64=1;
  else if((fmt=='f')||(fmt=='g')||(fmt=='e'))
    base = 10,aa='a'-10,isflt=1;
  else {
    base = 10, aa = 0;
    if(fmt=='u')
      isu=1;
  }
  if(dstSize<1) return 0;

  if(isflt) {
    int64 i64;
    is64=1;
    flt64 f1 = *((flt64*)nump);
    f1 *= 100;
    fltdot=2;
    if((i64=f1)<0)
      u64=-i64,neg=1;
    else
      u64=i64;
  } else if(is64) {
    int64 i64;
    if((i64=*((int64*)nump))<0)
      u64=-i64,neg=1;
    else
      u64=i64;
  } else {
    if(isu)
      u32=*((uns32*)nump);
    else {
      int32 i32;
      if((i32=*((int32*)nump))<0)
        u32=-i32,neg=1;
      else
        u32=i32;
    }
  }

  for(dlen=0;dlen<sizeof(digits);) {

    if(!is64) {
      if(!u32 && dlen)
        break;
      d = u32 % base;
      u32 /= base;
    } else {
      if(!u64 && dlen)
        break;
      d = u64 % base;
      u64 /= base;
    }

    if(d<=9)
      d += '0';
    else if(d<base)
      d += aa;
    else
      d = '?';
    digits[dlen++] = d;
  } // for
  
  r = dstSize;
  if(neg)
    *dst++ = '-',dstSize--;

  while((dstSize>0) && (dlen>fltdot)) 
    *dst++ = digits[--dlen], dstSize--;
  if(dstSize && fltdot) {
    *dst++ = '.',dstSize--;
  while((dstSize>0) && dlen)
    *dst++ = digits[--dlen], dstSize--;
  }

  if(dstSize>0)
    *dst=0;
  else
    dst[-1]=0;
  return r - dstSize;
} // sznum()

// JFL 09 Jun 19
static cchr* sztobin10(cchr *s1,cchr *s1x,int *dstp)
{
  chr c,isneg=0;
  int v=0;
  for(;(c=*s1) && (!s1x||(s1<s1x));s1++) {
    if((c>='0')&&(c<='9'))
      c-='0';
    else if(c=='+')
      continue;
    else if(c=='-') {
      isneg=1;
      continue;
    } else
      break;
    v*=10;
    v+=c;
  } // for
  *dstp=isneg?-v:v;
  return s1;
} // sztobin10()

// JFL 03 May 18
// JFL 06 Nov 18
// JFL 09 Jan 19; added exp for flt64
cchr* stztobin(cchr *s1,cchr *s1x,uns8 base,chr dsttype,void *dstp)
{
  int32 vali=0;
  int64 valq=0;
  cchr *ss,*sx=0,*sdot=0;
  chr c;
  chr cbase;
  uns8 is64;
  uns8 neg=0;
  int exp=0;

  is64 = (dsttype=='q')||(dsttype=='f')||(dsttype=='g');

  if(*s1=='-')
    neg=1,s1++;
  if(*s1=='+')
    s1++;

  if(((s1[0])=='0')&&(((s1[1])=='x')||((s1[1])=='X')))
  {
    s1+=2;
    base=16;
  }
  if(!base)
    base=10;
  cbase = base<=10 ? '0'+base : 'a' + base-10;

  for(ss=s1;(c=*ss) && (!s1x||(ss<s1x));ss++) {
    if((c>='A')&&(c<='Z'))
      c=c-'A'+'a'; // tolower

    if((c>='0')&&(c<='9'))
      c-='0';
    else if((c>='a')&&(c<='z')) {
      if(c<cbase)
        c=c-'a'+10;
      else if(c=='e') {
        sx=sztobin10(ss+1,s1x,&exp);
        break;
      } else {
        break;
      }
    } else if(c=='.') {
      sdot=ss;
      continue;
    } else
      break;
    if(!is64) {
      vali*=base;
      vali+=c;
    } else {
      valq*=base;
      valq+=c;
    }
  } // for
  if(!sx)
    sx=ss; // end used for dot-shifting

  switch(dsttype) {
  default:
  case 'i':
    if(neg)
      vali=-vali;
  case 'u':
    *((int32*)dstp)=vali;
    break;
  case 'q':
    if(neg)
      valq=-valq;
    *((int64*)dstp)=valq;
    break;
  case 'f':
  case 'g': {
    double dval;
    dval = neg  ? -((double)valq) : valq;
    if(sdot||exp) {
      int16 pow=-exp;
      if(sdot)
        pow += ss-sdot-1;
      while(pow<0)
        pow++,dval*=10;
      while(pow>=NUM(sztobinDivisors))
        pow--,dval*=0.1;
      if((pow>0)&&(pow<NUM(sztobinDivisors)))
        dval *= sztobinDivisors[pow];
  }
  if(dsttype!='g')
    *((flt32*)dstp)=dval;
  else
    *((flt64*)dstp)=dval;
  } break;
  } // switch

  return sx;
} // stztobin()

// JFL 10 Apr 18
int stzfmt_v(chr *dst,int dstSize,cchr *fmt,va_list args)
{ // minimal, safe string formatter
 // minimal, safe string formatter
  int r = 0;
  int32 i1;
  int64 q1;
  chr *dst0 = dst;
  cchr *s1,*s2;
  chr c;
  
  if(!fmt || !dst || (dstSize<=1))
    return 0;
  
  dstSize--; // reserve for size

  for(;dstSize>0;) {
    if(!(c=*fmt++))
      break;
    if(c!='%') {
      *dst++=c,dstSize--;
      continue;
    }
    if(!(c=*fmt++))
       break;
    switch(c) {
    case '%':
      *dst++=c;
      break;
    case 'c':
      if(!(c=va_arg(args,int)))
        break;
      if(c!=C_SZ_SKIP)
        *dst++=c,dstSize--;
      break;
    case 'S':
      s1=va_arg(args,cchr*);
      s2=va_arg(args,cchr*);
      goto cpy_str;
    case 's':
      s1=va_arg(args,cchr*);
      s2=0;
    cpy_str:
      if(!s1) break;
      while(dstSize>0) {
        if(s2 && (s1>=s2))
            break;
        if(!(*dst=*s1++))
            break;
        dst++,dstSize--;
      } // while
      break;
    case 'l':
      if((fmt[0]=='d')||(fmt[0]=='u')||(fmt[0]=='x')||(fmt[0]=='X')) {
        c=*fmt++;
        goto num_32;
      } else if((fmt[0]=='l') && (fmt[1]=='d')) {
        fmt+=2;
        c='q';
        goto num_64;
      } else if((fmt[0]=='l') && (fmt[1]=='x')) {
        fmt+=2;
        c='y';
        goto num_64;
      }
      // drop through  
    case 'p':
      if(dstSize>2)
        dst[0]='0',dst[1]='x',dst+=2,dstSize-=2;
      c='x';
    case 'd':
    case 'u':
    case 'x':
    case 'X':
    num_32:
      i1=va_arg(args,int);
      r=sznum(dst,dstSize,c,&i1);
      dst+=r,dstSize-=r;
      break;
    case 'f': // flt32 (floats passed as flt64)
    case 'g': // flt64
    case 'q':
    num_64:
      q1=va_arg(args,int64);
      r=sznum(dst,dstSize,c,&q1);
      dst+=r,dstSize-=r;
      break;
    case 'e': { // flt64
      flt64 f64=va_arg(args,flt64);
      r=stznume64(dst,dstSize,&f64);
      dst+=r,dstSize-=r;
    } break;
    } // switch
  } // for
  
  *dst = 0;
  r = PTR_DIFF(dst,dst0);
  return r;
} // stzfmt_v()

// JFL 10 Apr 18
int stzfmt(chr *dst,int dstSize,cchr *fmt,...)
{
  int r;
  va_list args;
  va_start(args,fmt);
  r = stzfmt_v(dst,dstSize,fmt,args);
  va_end(args);
  return r;
} // stzfmt()

// EOF
