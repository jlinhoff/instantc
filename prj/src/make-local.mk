# file: makefile - Joe Linhoff
APP=../../bin/instantc

.PHONY: all clean help
NODEPS := clean help

CC := gcc
LD := gcc
DEL := rm -f

CFLAGS := -I. -g -DBUILD_PFM_LIB
LDFLAGS := -g -lm
ARFLAGS := rc

UNAME:=$(shell uname -s)
ifneq ($(findstring MINGW,$(UNAME)),)
CFLAGS+=-DBUILD_MINGW
else ifneq ($(findstring Darwin,$(UNAME)),)
CFLAGS+=-DBUILD_DARWIN
else ifneq ($(findstring CYGWIN,$(UNAME)),)
CFLAGS+=-DBUILD_CYGWIN
else
CFLAGS+=-DBUILD_LINUX
endif

GCCINFO=../../gccinfo.txt

FUNFLAGS=$(shell $(CC) -x c -DFUN_GMTIME_S $(GCCINFO) -o gccinfo 2> /dev/null && ./gccinfo && $(DEL) gccinfo)
FUNFLAGS+=$(shell $(CC) -x c -DFUN_GMTIME_R $(GCCINFO) -o gccinfo 2> /dev/null && ./gccinfo && $(DEL) gccinfo)
FUNFLAGS+=$(shell $(CC) -x c -DFUN_IO_H $(GCCINFO) -o gccinfo 2> /dev/null && ./gccinfo && $(DEL) gccinfo)
FUNFLAGS+=$(shell $(CC) -x c -DFUN_FINDDATA_T $(GCCINFO) -o gccinfo 2> /dev/null && ./gccinfo && $(DEL) gccinfo)
FUNFLAGS+=$(shell $(CC) -x c -DFUN_DIRROOT $(GCCINFO) -o gccinfo 2> /dev/null && ./gccinfo && $(DEL) gccinfo)
CFLAGS+=$(FUNFLAGS)
# CFLAGS+=-DDIRROOT="C:/dev/msys64"

all: $(APP)

SRC := $(wildcard *.c) # list of source files

SUFFIXES += .d
DEPFILES := $(patsubst %.c,%.d,$(SRC))

ifeq (0, $(words $(findstring $(MAKECMDGOALS), $(NODEPS))))
-include $(DEPFILES)
endif

$(APP): $(SRC:%.c=%.o)
	@echo building $(APP)
	$(CC) $(SRC:%.c=%.o) $(LDFLAGS) -o $@

%.d: %.c
	$(CC) $(CFLAGS) -MM -MT '$(patsubst %.c,%.o,$<)' $< -MF $@

%.o: %.c %.d %.h
	$(CC) $(CFLAGS) -o $@ -c $<

clean:
	$(DEL) $(APP) *.o *.d $(APP).exe

dirroot:
	@echo DIRROOT=$(DIRROOT)

help:
	@echo make clean -- removes all built files
	@echo make -- build
	@echo WINDOWS:$(WINDOWS)
	@echo DARWIN:$(DARWIN)
	@echo CFLAGS:$(CFLAGS)

