// base.c
// Copyright (C) 2018-2019 JoeCo, All Rights Reserved.
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "stz.h"
#include "base.h"

////////////////////////////////////////////////////////////////////////////////

BaseGlobals BaseG;

int BaseInit()
{
  MEMZ(BaseG);
  BaseG.tzOffSeconds=MainTimeZoneOffsetSeconds();
  return 0;
} // BaseInit()

void BaseFinal()
{
  if(BaseG.allocCount) {
    pfmLogf("free all memory! (%d not freed)\n",BaseG.allocCount);
    BRK();
  }
} // BaseFinal()

////////////////////////////////////////////////////////////////////////////////

int MemAlloc(void *pp,uns size)
{
  void*p=malloc(size);
  if(!p) return -999;
  *((void**)pp)=p;
  BaseG.allocCount++;
  return 0;
} // MemAlloc()

int MemAlloz(void *pp,uns size)
{
  void*p=calloc(1,size);
  if(!p) return -999;
  *((void**)pp)=p;
  BaseG.allocCount++;
  return 0;
} // MemAlloz()

void MemFree(void *p)
{
  free(p);
  BaseG.allocCount--;
} // MemFree()

int MemCount()
{
  return BaseG.allocCount;
} // MemCount()

////////////////////////////////////////////////////////////////////////////////

// JFL 14 Nov 10
int pfmLogf(chr* fmt, ...)
{
  int r;
  chr buf[1024];
  vargs args;

  varg_start(args, fmt);

  r = stzfmt_v(buf, sizeof(buf), fmt, args);
  MainLogOutput(buf,r);

  varg_end(args);

  return r;
} // pfmLogf()

// JFL 14 Nov 10
int pfmBrkf(chr* fmt, ...)
{
  int r;
  chr buf[1024];
  vargs args;

  varg_start(args, fmt);

  r = stzfmt_v(buf, sizeof(buf), fmt, args);
  MainLogOutput(buf,r);

  varg_end(args);

  return r;
} // pfmBrkf()

////////////////////////////////////////////////////////////////////////////////

// JFL 23 Aug 06
// JFL 20 Mar 08; re-worked from DL
// JFL 28 Jul 17
void BaseLinkMakeHead(BaseLink* h)
{
  h->p = h->n = h;
  h->t = 0;
  h->f = 0;
} // BaseLinkMakeHead()

// JFL 23 Aug 06
// JFL 20 Mar 08; re-worked from DL
// JFL 28 Jul 17
void BaseLinkMakeNode(BaseLink* n, int t)
{
  n->n = n->p = n;
  n->f = 0;
  n->t = (uns8)t;
} // BaseLinkMakeNode()

// JFL 23 Aug 06
// JFL 20 Mar 08; re-worked from DL
// JFL 28 Jul 17
void BaseLinkBefore(BaseLink* h, BaseLink* n)
{
  n->p = h->p;
  n->p->n = n;
  n->n = h;
  h->p = n;
} // BaseLinkBefore()

// JFL 23 Aug 06
// JFL 20 Mar 08; re-worked from DL
// JFL 08 Mar 10; fixed link bug
void BaseLinkAfter(BaseLink *h,BaseLink *n)
{
   n->p=h;
   n->n=h->n;
   h->n->p=n;
   h->n=n;
} // BaseLinkAfter()

// JFL 28 Jul 17
void BaseLinkUnlink(BaseLink* n)
{
  n->p->n = n->n;
  n->n->p = n->p;
  n->n = n->p = n; // multiple unlinks OK
} // BaseLinkUnlink()

///////////////////////////////////////////////////////////////////////////////

// MINGW
// gmtime_r (on MSYS) MINGW1
// gmtime_s (on Windows) MINGW2
// not sure the best way to support the variants yet
#if FUN_GMTIME_S
#define SECONDS_FROM_TM(tmSrc) _mkgmtime(tmSrc)
#define TM_GMT_FROM_SECONDS(tmDst,secSrc) gmtime_s(tmDst,(time_t const*)secSrc)
#define TM_LOC_FROM_SECONDS(tmDst,secSrc) localtime_s(tmDst,(time_t const*)secSrc)
#else
#define SECONDS_FROM_TM(tmSrc) timegm(tmSrc)
#define TM_LOC_FROM_SECONDS(tmDst,secSrc) localtime_r((time_t const*)secSrc,tmDst)
#define TM_GMT_FROM_SECONDS(tmDst,secSrc) gmtime_r((time_t const*)secSrc,tmDst)
#endif

// JFL 16 Nov 13
uns64 MainTimeNow()
{
  time_t tt;
  struct tm d1;
  tt = time(0);
  TM_GMT_FROM_SECONDS(&d1,&tt);
  tt = SECONDS_FROM_TM(&d1);
  return tt;
} // MainTimeNow()

// JFL 16 Nov 13
uns64 MainTimeToday()
{
  time_t tt;
  struct tm d1;
  tt = time(0);
  TM_GMT_FROM_SECONDS(&d1,&tt);
  d1.tm_hour = 0;
  d1.tm_min = 0;
  d1.tm_sec = 0;
  tt = SECONDS_FROM_TM(&d1);
  return tt;
} // MainTimeToday()

// JFL 29 Jun 19
int32 MainTimeZoneOffsetSeconds()
{
  time_t tt = time(0);
  struct tm d1,d2;
  TM_GMT_FROM_SECONDS(&d1,&tt);
  TM_LOC_FROM_SECONDS(&d2,&tt);
  time_t sgmt = SECONDS_FROM_TM(&d1);
  time_t sloc = SECONDS_FROM_TM(&d2);
  return sgmt-sloc;
} // MainTimeZoneOffsetSeconds()

// JFL 16 Nov 13
// JFL 29 Jun 19; time-zone
uns64 MainTimeFromYMDHHMMSS(uns16 y,uns8 m,uns8 d,uns8 hh,uns8 mm,uns8 ss)
{ // y=year(2019==2019) m=month(1==jan) d=day(1st==1)
  time_t tt;
  struct tm d1,d2,d3;
  tt = time(0); // get current local time for any non-specifed field
  TM_GMT_FROM_SECONDS(&d1,&tt); // use GMT to avoid DST

  if(y>1900)
    d1.tm_year = y-1900;
  if(m)
    d1.tm_mon = m-1;
  else BRK();
  if(d)
    d1.tm_mday = d;
  else BRK();

  MEMZ(d2);
  d2.tm_year = d1.tm_year;
  d2.tm_mon = d1.tm_mon;
  d2.tm_mday = d1.tm_mday;
  d2.tm_hour = hh;
  d2.tm_min = mm;
  d2.tm_sec = ss;
  //d2.tm_isdst = -1;
  tt = SECONDS_FROM_TM(&d2);

#if DEBUG
{
  TM_GMT_FROM_SECONDS(&d3,&tt);

  if(y && (d3.tm_year+1900 != y)) BRK();
  if(d3.tm_mon+1 != m) BRK();
  if(d3.tm_mday != d) BRK();

  if(d3.tm_hour != hh) BRK();
  if(d3.tm_min != mm) BRK();
  if(d3.tm_sec != ss) BRK();
}
#endif // DEBUG

  return tt;
} // MainTimeFromYMDHHMMSS()

// JFL 08 Jul 19
uns64 MainTimeFromYJHHMMSS(uns16 y,uns16 j,uns8 hh,uns8 mm,uns8 ss)
{
  time_t tt;
  struct tm d1,d2;
  uns32 jj;

  if(y>1900) {
    d1.tm_year = y-1900;
  } else {
    tt = time(0); // get current local time for any non-specifed field
    TM_GMT_FROM_SECONDS(&d1,&tt); // use GMT to avoid DST
  }

  // find time at start of year
  MEMZ(d2);
  d2.tm_year = d1.tm_year;
  tt = SECONDS_FROM_TM(&d2);

  // add in days
  jj = j;
  jj *= 24*60*60; // days to seconds
  tt += jj;

  jj = hh*60*60;
  tt += jj;

  jj = mm*60;
  tt += jj;

  tt += ss;

  return tt;
} // MainTimeFromYJHHMMSS()

int MainTimeToYMDHHMMSS(uns64 t,uns16 *yOut,uns8 *mOut,uns8 *dOut,
  uns8 *hhOut,uns8 *mmOut,uns8 *ssOut)
{
  time_t tt = t;
  struct tm d1;
  TM_GMT_FROM_SECONDS(&d1,&tt);
  tt = SECONDS_FROM_TM(&d1);
  if(tt != t) BRK();
  if(yOut)
    *yOut=d1.tm_year+1900;
  if(mOut)
    *mOut=d1.tm_mon+1;
  if(dOut)
    *dOut=d1.tm_mday;
  if(hhOut)
    *hhOut=d1.tm_hour;
  if(mmOut)
    *mmOut=d1.tm_min;
  if(ssOut)
    *ssOut=d1.tm_sec;
  return 0;
} // MainTimeToYMDHHMMSS()

static cchr *mainTimeFmtDefault = "m/d/Y H:M:S";

// JFL 23 Jun 19
int MainTimeStr(chr *dst,int dstSize,cchr *fmt,uns64 t)
{
  int r,ds=dstSize;
  uns16 y;
  uns8 m,d,hh,mm,ss;
  chr c,chh,cmm,css;

  if(!fmt)
    fmt=mainTimeFmtDefault;

  t -= BaseG.tzOffSeconds;
  if((r=MainTimeToYMDHHMMSS(t,&y,&m,&d,&hh,&mm,&ss))<0)
    goto BAIL;

  chh = hh>9 ? 0 : '0';
  cmm = mm>9 ? 0 : '0';
  css = ss>9 ? 0 : '0';

  // y=year, Y=year%100 m=month, d=day, H=hour,M=min,S=sec
  for(;;) {
    if(ds<=1)
      break;
    if(!(c=*fmt++))
      break;
    switch(c) {
    default:
      *dst++=c,ds--;
      break;
    case 'Y':
      y %= 100;
    case 'y':
      r=stzfmt(dst,ds,"%d",y);
      dst+=r,ds-=r;
      break;
    case 'm':
      r=stzfmt(dst,ds,"%d",m);
      dst+=r,ds-=r;
      break;
    case 'd':
      r=stzfmt(dst,ds,"%d",d);
      dst+=r,ds-=r;
      break;
    case 'H':
      r=stzfmt(dst,ds,"%c%d",chh,hh);
      dst+=r,ds-=r;
      break;
    case 'M':
      r=stzfmt(dst,ds,"%c%d",cmm,mm);
      dst+=r,ds-=r;
      break;
    case 'S':
      r=stzfmt(dst,ds,"%c%d",css,ss);
      dst+=r,ds-=r;
      break;
    } // switch
  } // for

BAIL:
  return dstSize-ds;
} // MainTimeStr()

////////////////////////////////////////////////////////////////////////////////

// JFL 29 May 19
int BaseScanYearMonthDay(cchr *str,cchr *strx,uns16 *yearp,uns8 *monthp,uns8 *dayp,uns8 mdy)
{
  int r;
  int v[3];
  uns8 k;

  for(k=0;k<NUM(v);k++)
    v[k]=0;

  for(k=0;k<NUM(v);k++) {
    str=stztillid(str,strx);
    str=stztobin(str,strx,10,'i',v+k);
  } // for

  if(!mdy) {
    // year-month-day
    *yearp = v[0];
    *monthp = v[1];
    *dayp = v[2];
  } else {
    // month-day-year
    *monthp=v[0];
    *dayp=v[1];
    *yearp=v[2];
  }

  if(!*yearp || !*monthp)
    ret(-9);

  r=0;
BAIL:
  return r;
} // BaseScanYearMonthDay()

// JFL 29 May 19
int BaseScanHourMinSec(cchr *str,cchr *strx,uns8 *hourp,uns8 *minp,uns8 *secp)
{
  int r,v;
  cchr *s1;

  str=stztobin(str,strx,10,'i',&v);
  *hourp=v;
  str=stztillid(str,strx);
  str=stztobin(str,strx,10,'i',&v);
  *minp=v;
  str=stztillid(str,strx);
  if(!(s1=stzchr(str,strx,'.')))
    s1=strx;
  str=stztobin(str,s1,10,'i',&v);
  *secp=v;

  r=0;
  return r;
} // BaseScanHourMinSec()

////////////////////////////////////////////////////////////////////////////////

chr *BasePathAlt(chrc *path,chrc *pathx,int flags) {
  int r;
  uint32_t s;
  chr *buf=0,*bb;
  static chr root[512]={0};
  static int rootflags=0;

  if(!path) return 0;

return 0;

  if(flags&M_BASEPATHALT_SETROOT) {
    rootflags=flags;
    stzcpy(root,sizeof(root),path,pathx);
    goto BAIL;
  }

  flags|=rootflags;

  s=stzlen(path,pathx);
  s+=stzlen(root,0)+16;

  if((r=MemAlloc(&buf,s))<0)
    goto BAIL;
  bb=buf;

  if((*path=='/')&&!(flags&M_BASEPATHALT_NOROOT)) {
    r=stzcpy(bb,s,root,0);
    bb+=r,s-=r;
  }
  r=stzcpy(bb,s,path,pathx);
  bb+=r,s-=r;

  if(flags&M_BASEPATHALT_WINSLASH) {
    for(bb=buf;*bb;bb++) {
      if(*bb=='/') *bb='\\';
    } // for
  }

BAIL:
  return buf;
} // BasePathAlt()

// JFL 22 Feb 15
// JFL 31 May 19
// JFL 18 Sep 19
int BaseReadFile(cchr *fpath,cchr *fpathx,chr **bufp,int flags)
{
  int r, s, top;
  FILE* fh = 0;
  chr *pathbuf = 0;
  chr *buf = 0;

  if(fpathx) {
    s=stzsize(fpath,fpathx);
    if((r=MemAlloc(&pathbuf,s))<0)
      goto BAIL;
    stzcpy(pathbuf,s,fpath,fpathx);
    fpath=pathbuf,fpathx=0;
  }

  if(!(fh = fopen(fpath, "rb"))) {
//    if(pathbuf) {
//      MemFree(pathbuf);
//      pathbuf=0;
//    }
//
//    if((pathbuf=BasePathAlt(fpath,0,0))) {
//      if((fh = fopen(pathbuf, "rb")))
//        goto opend;
//    }

    if(!(flags&M_BASEREADFILE_QUIET))
      PRINT_SYS_ERR("BaseReadFile couldn't open '%s'\n",fpath);
    ret(-5);
  }
//opend:

  // get file size
  fseek(fh, 0, SEEK_END);
  s = top = ftell(fh);
  fseek(fh, 0, SEEK_SET);

  if(bufp) {
    // alloc 2 extra (one for EOL, one for EOS)
    if((r=MemAlloc(&buf,s+2))<0)
      goto BAIL;

    if((r = fread(buf, 1, s, fh)) != s)
      ret(-7);
    buf[s] = 0; // always term

    if(M_BASEREADFILE_EOLGUARANTEE) {
      if(!C_IS_EOL(buf[s-1])) {
        buf[s++]='\n';
        buf[s]=0;
      }
    } // add EOL

    *bufp=buf,buf=0;
  }

  r = s;
BAIL:
  if(pathbuf)
    MemFree(pathbuf);
  if(fh)
    fclose(fh);
  return r;
} // BaseReadFile()

// JFL 03 Jul 19
// JFL 02 Nov 19
// JFL 18 Nov 20; reworked with getc
int BaseRunScript(chr **bufp,cchr *scr,cchr *scrx,int flagEcho)
{
  int r,c;
  FILE *echofd[2];
  int32 allocsize,bs,expandsize;
  FILE *pp=0;
  chr *alloc=0,*bb;
  chr *scralloc=0;

  expandsize=512*4; // first alloc will be 2x this amount
  echofd[0]=echofd[1]=0;

  if(scrx) {
    bs=stzsize(scr,scrx);
    if((r=MemAlloc(&scralloc,bs))<0)
      goto BAIL;
    stzcpy(scralloc,bs,scr,scrx);
    scr=scralloc,scrx=0;
  }

  if(flagEcho==BASERUNSCRIPT_STDOUT)
    echofd[0]=stdout;
  else if(flagEcho==BASERUNSCRIPT_STDERR)
    echofd[1]=stderr;
  else if(flagEcho==BASERUNSCRIPT_STDOUTERR)
    echofd[0]=stdout,echofd[1]=stderr;

  // try to start the command
  if(!(pp=popen(scr,"r")))
    ret(-1);

  allocsize=0;
  bs=0;
  while(1) {
    c=getc(pp);
    if(c==EOF) // EOF sent when process ends
      c=0;
    else {
      if(echofd[0])
        putc(c,echofd[0]);
      if(echofd[1])
        putc(c,echofd[1]);
      if(c=='\n') {
        if(echofd[0])
          fflush(echofd[0]);
        if(echofd[1])
          fflush(echofd[1]);
      }
    }
    if(!bs)
    {
        int bs2;
        chr *alloc2;

        // create new, larger buffer
        if(expandsize<1024*1024*16) // double in size until 16M
          expandsize*=2;
        bs2=allocsize+expandsize;
        if((r=MemAlloc(&alloc2,bs2))<0)
          goto BAIL;
        r=allocsize-bs; // amount in current buffer
        if(alloc)
          memcpy(alloc2,alloc,r);

        // free old & swap
        bb = alloc2+r; // offset into new buf
        bs = bs2-r; // remaining in new buf
        allocsize = bs2;
        if(alloc)
          MemFree(alloc);
        alloc = alloc2;
    }
    *bb++=c,bs--;
    if(!c)
      break;
  } // while

  if(bufp)
    *bufp=alloc,alloc=0;

  r = 0;
BAIL:
  if(scralloc)
    MemFree(scralloc);
  if(alloc)
    MemFree(alloc);
  if(pp) {
    int x = pclose(pp);
    if(!r) // change ret code if no error so far
      r= (x > 255) ? x>>8 : x; // exit code
  }
  return r;
} // BaseRunScript()

// JFL 05 Nov 19
int BaseLaunchScript(cchr *scr,cchr *scrx,chr*const* envarr)
{ // experimental for gdb -tui
  chr *scralloc=0;
  chr *dst,*dstx;
  cchr *s1,*s2;
  int argc,r,s,nn;

  nn=stzcountchrs(scr,scrx," ",0)+1;
  cchr *argv[nn];

  s=stzsize(scr,scrx);
  if((r=MemAlloc(&scralloc,s))<0)
    goto BAIL;

  dst=scralloc;
  r=stzcpy(dst,s,scr,scrx);
  dstx=dst+r;

  argc=0;

  dst=(chr*)stzskiparg(dst,dstx,&s1,&s2);
  argv[argc++]=s1;
  *((chr*)s2)=0;
  dst++;

  for(;;) {
    dst=(chr*)stzskipwhite(dst,dstx);
    if(stzeos(dst,dstx))
      break;
    dst=(chr*)stzskiparg(dst,dstx,&s1,&s2);
    dst++;
    argv[argc++]=s1;
    *((chr*)s2)=0;
  } // for

  argv[argc++]=0;
  if(argc>nn) BRK();

  for(r=0;r<argc;r++) {
    PRINT_COM_VRB("execve [%d] '%s'\n",r,argv[r]);
  } // for

  execve(argv[0],(chr*const*)argv,(chr*const*)envarr);

  r=0;
BAIL:
  if(scralloc)
    MemFree(scralloc);
  return r;
} // BaseLaunchScript()

// JFL 17 Sep 19
int BaseFileDate(uns64 *secp,cchr *path,cchr *pathx)
{
  int r,s;
  struct stat st;
  chr *alloc=0;

  if(pathx) {
    s=stzsize(path,pathx);
    if((r=MemAlloc(&alloc,s))<0)
      goto BAIL;
    stzcpy(alloc,s,path,pathx);
    path=alloc;
  }

  if(stat(path,&st)<0)
    ret(-1);

  if(secp) {
    *secp = st.st_ctime;
    if(*secp < st.st_mtime)
      *secp = st.st_mtime;
  }

  r=0;
BAIL:
  if(alloc)
    MemFree(alloc);
  return r;
} // BaseFileDate()

////////////////////////////////////////////////////////////////////////////////

#if BUILD_MINGW

// JFL 11 Oct 19
int BaseHomeDir(chr *dst,int dstMax)
{
  int r;
  chr *homedir;

  if((homedir = getenv("HOME")))
    goto success;
  if((homedir = getenv("HOMEPATH")))
    goto success;
  if(!homedir)
    ret(-1);

success:
  r=stzcpy(dst,dstMax,homedir,0);

BAIL:
  return r;
} // BaseHomeDir()

#else // !BUILD_MINGW

#include <pwd.h>

// JFL 11 Oct 19
int BaseHomeDir(chr *dst,int dstMax)
{
  int r;
  chr *homedir;

  if (!(homedir = getenv("HOME"))) {
    homedir = getpwuid(getuid())->pw_dir;
  }

  if(!homedir)
    ret(-1);

  r=stzcpy(dst,dstMax,homedir,0);

BAIL:
  return r;
} // BaseHomeDir()

#endif // !BUILD_MINGW

// EOF

