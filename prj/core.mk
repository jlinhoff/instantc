##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=core
ConfigurationName      :=Debug
WorkspacePath          :=D:/ainstantc/prj
ProjectPath            :=D:/ainstantc/prj
IntermediateDirectory  :=./Debug
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=joe
Date                   :=15/11/2022
CodeLitePath           :=D:/Programs/CodeLite
LinkerName             :=D:/TDM-GCC-64/bin/g++.exe
SharedObjectLinkerName :=D:/TDM-GCC-64/bin/g++.exe -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=$(ProjectPath)/../lib/lib$(ProjectName).a
Preprocessors          :=$(PreprocessorSwitch)BUILD_MINGW $(PreprocessorSwitch)FUN_IO_H $(PreprocessorSwitch)FUN_FINDDATA_T $(PreprocessorSwitch)FUN_GMTIME_S $(PreprocessorSwitch)DEBUG=1 $(PreprocessorSwitch)BUILD_PFM_LIB=1 $(PreprocessorSwitch)_WIN32 
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :="core.txt"
PCHCompileFlags        :=
MakeDirCommand         :=makedir
RcCmpOptions           := 
RcCompilerName         :=D:/TDM-GCC-64/bin/windres.exe
LinkOptions            :=  
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch)$(ProjectPath)/../ 
IncludePCH             := 
RcIncludePath          := 
Libs                   := 
ArLibs                 :=  
LibPath                := $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := D:/TDM-GCC-64/bin/ar.exe rcu
CXX      := D:/TDM-GCC-64/bin/g++.exe
CC       := D:/TDM-GCC-64/bin/gcc.exe
CXXFLAGS :=  -g $(Preprocessors)
CFLAGS   :=  -g $(Preprocessors)
ASFLAGS  := 
AS       := D:/TDM-GCC-64/bin/as.exe


##
## User defined environment variables
##
CodeLiteDir:=D:\Programs\CodeLite
Objects0=$(IntermediateDirectory)/up_core_core.c$(ObjectSuffix) $(IntermediateDirectory)/up_core_sz.c$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: $(IntermediateDirectory) $(OutputFile)

$(OutputFile): $(Objects)
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(AR) $(ArchiveOutputSwitch)$(OutputFile) @$(ObjectsFileList)
	@$(MakeDirCommand) "D:\ainstantc\prj/.build-debug"
	@echo rebuilt > "D:\ainstantc\prj/.build-debug/core"

MakeIntermediateDirs:
	@$(MakeDirCommand) "./Debug"


./Debug:
	@$(MakeDirCommand) "./Debug"

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/up_core_core.c$(ObjectSuffix): ../core/core.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_core_core.c$(ObjectSuffix) -MF$(IntermediateDirectory)/up_core_core.c$(DependSuffix) -MM ../core/core.c
	$(CC) $(SourceSwitch) "D:/ainstantc/core/core.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_core_core.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_core_core.c$(PreprocessSuffix): ../core/core.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_core_core.c$(PreprocessSuffix) ../core/core.c

$(IntermediateDirectory)/up_core_sz.c$(ObjectSuffix): ../core/sz.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_core_sz.c$(ObjectSuffix) -MF$(IntermediateDirectory)/up_core_sz.c$(DependSuffix) -MM ../core/sz.c
	$(CC) $(SourceSwitch) "D:/ainstantc/core/sz.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_core_sz.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_core_sz.c$(PreprocessSuffix): ../core/sz.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_core_sz.c$(PreprocessSuffix) ../core/sz.c


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r ./Debug/


