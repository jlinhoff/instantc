# file: make-local.mk - Joe Linhoff

.PHONY: all clean help

DEL=rm -f

all:
	@echo run scripts individually

clean:
	$(DEL) *.c *.cpp *.cc *.exe *.log

help:
	@echo make clean -- removes all built files
	@echo make -- build

