#!/usr/bin/env instantc
$(lang c) $(lib core) // set language and link in the instantc core library
// CorePrintf() is a limited printf
int r=CorePrintf("InstantC example2"); // returns number of chars printed
CorePrintf("\n%r-\n",r);
// EOF
