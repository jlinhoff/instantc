#!/usr/local/bin/instantc
$(include <stdio.h>)
$(sclib core)

///////////////////////////////////////////////////////////////////////////////
$(function)

///////////////////////////////////////////////////////////////////////////////
$(main)

int r;
chr path[2048],*pathx;
r=szfmt(path,sizeof(path),"%s",argv[1]);
pathx=path+r;
CoreLink list;
CoreLinkMakeHead(&list);

uns64 t;
CoreTimeNow(&t);
chr buf[256];
CoreTimeStr(buf,sizeof(buf),0,t);
CorePrintf("time:%lld %s\n",t,buf);

CorePrintf("path %s\n",path);
r=CoreListFiles(&list,path,pathx,1);
CorePrintf("NUM:%d\n",r);

uns64 timebest;
CoreFileRec *filebest=0;
for(CoreLink *lnk=list.n;lnk->t;lnk=lnk->n) {
  if(lnk->t!=CORETYPE_FILEREC) continue;
  CoreFileRec *file=(CoreFileRec*)lnk; 
  uns64 time,size;
  CorePrintf("%s\n",file->name);
  CoreFileSizeTime(&size,&time,file->name,0);
  if(!filebest || (time>=timebest)) {
    //if(time==timebest) CorePrintf("-- matching times --\n");
    filebest=file,timebest=time;
  }
} // for

if(filebest) {
  CoreTimeStr(buf,sizeof(buf),0,timebest);
  CorePrintf("best: %s %s\n",filebest->name,buf);
} else
  CorePrintf("no files found\n");

CoreLinkFncAll(&list,COREFNC_ZAP);
// EOF
