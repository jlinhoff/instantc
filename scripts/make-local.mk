# file: make-local.mk - Joe Linhoff
# updated

.PHONY: all clean help

REBUILD:=+rebuild
RUN:=instantc 
DEL:=rm -f
RUNDIR:=./

all:
	@echo building libraries and tools RUN=$(RUN)
	@$(RUN) $(RUNDIR)szlib $(REBUILD)
	@$(RUN) $(RUNDIR)instantlib $(REBUILD)
	@$(RUN) $(RUNDIR)listlib $(REBUILD)
	@$(RUN) $(RUNDIR)shelllib $(REBUILD)
	@$(RUN) $(RUNDIR)estfile -q $(REBUILD)
	@$(RUN) $(RUNDIR)instantdoc -q $(REBUILD)
	@$(RUN) $(RUNDIR)getbase $(REBUILD)
	@echo

clean:
	$(DEL) *.c *.cpp *.cc *.exe *.log

help:
	@echo make clean -- removes all built files
	@echo make -- build

