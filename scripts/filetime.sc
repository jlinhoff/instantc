#!/usr/bin/env instantc
$(include <stdio.h>) $(lib core)

uns64 t;
CoreTimeNow(&t);
chr buf[256];
CoreTimeStr(buf,sizeof(buf),0,t);
printf("time:%lld %s\n",t,buf);

cchr *s1 = "hello.sc";
CoreFileSizeTime(0,&t,s1,0,0);
CoreTimeStr(buf,sizeof(buf),0,t);
printf("file:%s time:%lld %s\n",s1,t,buf);

chr *sh = "$(shell echo yo ma)";
printf("SHELL: '%s'\n",sh);

// EOF
